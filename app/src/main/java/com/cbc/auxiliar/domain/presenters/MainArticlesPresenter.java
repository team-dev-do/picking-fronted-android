package com.cbc.auxiliar.domain.presenters;

import com.cbc.auxiliar.presentation.viewcontrollers.base.BaseActivityView;
import com.cbc.auxiliar.presentation.viewmodels.CustomerViewModel;
import com.cbc.auxiliar.presentation.viewmodels.ArticleViewModel;

import java.util.List;

/**
 * -----------------------------------------------------------------------------------------------------------
 * |                                               CBC AUXILIAR                                              |
 * -----------------------------------------------------------------------------------------------------------
 *
 * FECHA           AUTOR               J.PROYECTO              PROYECTO                DESCRIPCION
 * -----------------------------------------------------------------------------------------------------------
 * 04/10/2020       Joel Cauti          Alexander Olivares      CBC Picking             Creacion Presenter
 */

public interface MainArticlesPresenter {

    void finalizarPedido(CustomerViewModel order);
    void setTerminar(String id);
    void logOut();

    interface View extends BaseActivityView{

        void updateRecycler(List<ArticleViewModel> products, String orderName);
        void showLoading();
        void showLoading(String message);
        void hideDialog();
        void showAlert(String message, int typeAlert);
        void showAlert(String message, int typeAlert, boolean actionBack);

        void showDialogDecision(Dialog dialog, String message, String labelAceptar, String labelRechazar, boolean isWarning);

        void showDialogDecision(Dialog dialog, String message, String labelAceptar, String labelRechazar);
        void showDialogDecision(Dialog dialog, String message);
        void goToPayroll();
        void showImage(String image);
        void closeSession();
    }

    interface Dialog{

        void aceptarDialog();
        void rechazarDialog();
    }
}
