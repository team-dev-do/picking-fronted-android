package com.cbc.auxiliar.domain.helpers;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import static com.cbc.auxiliar.presentation.viewutils.ConstantsUtils.REQUEST_PERMISSION_ALL;

/**
 * -----------------------------------------------------------------------------------------------------------
 * |                                               CBC AUXILIAR                                              |
 * -----------------------------------------------------------------------------------------------------------
 *
 * FECHA           AUTOR               J.PROYECTO              PROYECTO                DESCRIPCION
 * -----------------------------------------------------------------------------------------------------------
 * 04/10/2020       Joel Cauti          Alexander Olivares      CBC Picking             Creacion Helper
 */

public class PermissionHelper {

    public static boolean isGranted(Context context, String permission){
        if(ContextCompat.checkSelfPermission(context,permission) == PackageManager.PERMISSION_GRANTED){
            return true;
        }
        return false;
    }

    public static void request(Activity activity, String[] permissions) {
        ActivityCompat.requestPermissions(activity,permissions,REQUEST_PERMISSION_ALL);
    }


}
