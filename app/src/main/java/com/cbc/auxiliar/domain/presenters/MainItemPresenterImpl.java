package com.cbc.auxiliar.domain.presenters;

import android.content.Context;

import com.cbc.auxiliar.data.datamodels.responses.ResponseDeviceEnabledBean;
import com.cbc.auxiliar.data.datamodels.responses.ResponseFinishBean;
import com.cbc.auxiliar.data.datamodels.responses.ResponseInicioProducto;
import com.cbc.auxiliar.data.datamodels.responses.ResponseCustomersBean;
import com.cbc.auxiliar.data.datamodels.responses.ResponseLogoutBean;
import com.cbc.auxiliar.data.datamodels.responses.ResponsePayrollBean;
import com.cbc.auxiliar.data.datamodels.responses.ResponseTerminarProducto;
import com.cbc.auxiliar.data.datamodels.responses.ResponseUnlockBean;
import com.cbc.auxiliar.data.interactors.EnabledInteractor;
import com.cbc.auxiliar.data.interactors.EnabledInteractorImpl;
import com.cbc.auxiliar.data.interactors.OrdersInteractor;
import com.cbc.auxiliar.data.interactors.OrdersInteractorImpl;
import com.cbc.auxiliar.domain.helpers.PreferencesHelper;

import java.util.List;

/**
 * -----------------------------------------------------------------------------------------------------------
 * |                                               CBC AUXILIAR                                              |
 * -----------------------------------------------------------------------------------------------------------
 *
 * FECHA           AUTOR               J.PROYECTO              PROYECTO                DESCRIPCION
 * -----------------------------------------------------------------------------------------------------------
 * 04/10/2020       Joel Cauti          Alexander Olivares      CBC Picking             Creacion Implementacion Presenter
 */

public class MainItemPresenterImpl implements MainItemPresenter, OrdersInteractor.Callbacks, EnabledInteractor.Callbacks {

    private final Context context;
    private final View view;
    private final PreferencesHelper preferencesHelper;
    private final OrdersInteractor ordersInteractor;
    private final EnabledInteractor enabledInteractor;

    public MainItemPresenterImpl(View view) {
        this.context = (Context) view;
        this.view = view;
        this.preferencesHelper = new PreferencesHelper(context);
        this.ordersInteractor = new OrdersInteractorImpl(this);
        this.enabledInteractor = new EnabledInteractorImpl(this);
    }

    @Override
    public void setInicio(String id) {
        ordersInteractor.setInicio(preferencesHelper.getSessionVM().getImei(), id);
    }

    @Override
    public void logOut() {
        view.showLoading("Cerrando sesión.");
        enabledInteractor.logOut(preferencesHelper.getSessionVM().getImei());
    }

    //
    //
    // CALLBACKS ORDERS|ENABLED ERRORS
    //
    //

    @Override
    public void errorRequest(Integer errorCode, String method) {
        view.hideDialog();
    }

    @Override
    public void unauthorized(String method) {
        view.hideDialog();
    }

    //
    //
    // CALLBACKS ENABLED SUCCESS
    //
    //

    @Override
    public void successCheckImei(ResponseDeviceEnabledBean response) {
        // No se utiliza
    }

    @Override
    public void successEnabledDevice(ResponseDeviceEnabledBean response) {
        // No se utiliza
    }

    @Override
    public void successLogout(ResponseLogoutBean response) {
        preferencesHelper.removeSession();
        view.hideDialog();
        view.closeSession();
    }

    //
    //
    // CALLBACKS ORDERS SUCCESS
    //
    //

    @Override
    public void successOrders(List<ResponseCustomersBean> response) {
        // No se utiliza
    }

    @Override
    public void successFinishPedidos(ResponseFinishBean response) {
        // No se utiliza
    }

    @Override
    public void successSetInicio(ResponseInicioProducto response) {
        // No se utiliza
    }

    @Override
    public void successSetTerminar(ResponseTerminarProducto response) {
        // No se utiliza
    }

    @Override
    public void successTransportations(ResponsePayrollBean transportes) {
        // No se utiliza
    }

    @Override
    public void successCustomers(List<ResponseCustomersBean> customers) {
        // No se utiliza
    }

    @Override
    public void successUnlock(ResponseUnlockBean unlock) {
        // No se utiliza
    }
}
