package com.cbc.auxiliar.domain.presenters;

import com.cbc.auxiliar.presentation.viewcontrollers.base.BaseActivityView;

/**
 * -----------------------------------------------------------------------------------------------------------
 * |                                               CBC AUXILIAR                                              |
 * -----------------------------------------------------------------------------------------------------------
 *
 * FECHA           AUTOR               J.PROYECTO              PROYECTO                DESCRIPCION
 * -----------------------------------------------------------------------------------------------------------
 * 04/10/2020       Joel Cauti          Alexander Olivares      CBC Picking             Creacion Presenter
 */

public interface WelcomePresenter {

    void iniciarSesion();

    interface View extends BaseActivityView {

        void goToMain();
    }
}
