package com.cbc.auxiliar.domain.presenters;

import android.content.Context;

import com.cbc.auxiliar.data.datamodels.responses.ResponseDeviceEnabledBean;
import com.cbc.auxiliar.data.datamodels.responses.ResponseFinishBean;
import com.cbc.auxiliar.data.datamodels.responses.ResponseInicioProducto;
import com.cbc.auxiliar.data.datamodels.responses.ResponseLogoutBean;
import com.cbc.auxiliar.data.datamodels.responses.ResponseCustomersBean;
import com.cbc.auxiliar.data.datamodels.responses.ResponsePayrollBean;
import com.cbc.auxiliar.data.datamodels.responses.ResponseTerminarProducto;
import com.cbc.auxiliar.data.datamodels.responses.ResponseTransportationsBean;
import com.cbc.auxiliar.data.datamodels.responses.ResponseUnlockBean;
import com.cbc.auxiliar.data.interactors.EnabledInteractor;
import com.cbc.auxiliar.data.interactors.EnabledInteractorImpl;
import com.cbc.auxiliar.data.interactors.OrdersInteractor;
import com.cbc.auxiliar.data.interactors.OrdersInteractorImpl;
import com.cbc.auxiliar.domain.helpers.PreferencesHelper;
import com.cbc.auxiliar.presentation.viewmappers.CustomersViewModelMapper;
import com.cbc.auxiliar.presentation.viewmodels.CustomerViewModel;
import com.cbc.auxiliar.presentation.viewmodels.TransportationViewModel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * -----------------------------------------------------------------------------------------------------------
 * |                                               CBC AUXILIAR                                              |
 * -----------------------------------------------------------------------------------------------------------
 *
 * FECHA           AUTOR               J.PROYECTO              PROYECTO                DESCRIPCION
 * -----------------------------------------------------------------------------------------------------------
 * 04/10/2020       Joel Cauti          Alexander Olivares      CBC Picking             Creacion Implementacion Presenter
 */

public class MainCustomersPresenterImpl implements MainCustomersPresenter, OrdersInteractor.Callbacks, EnabledInteractor.Callbacks {

    private final View view;
    private final Context context;
    private final PreferencesHelper preferencesHelper;
    private final OrdersInteractor ordersInteractor;
    private final EnabledInteractor enabledInteractor;
    private List<CustomerViewModel> customers;
    private String idTransportation;
    private String idPartner;

    public MainCustomersPresenterImpl(View view, String idTransportation, String idPartner) {
        this.view = view;
        this.context = (Context) view;
        this.idTransportation = idTransportation;
        this.idPartner = idPartner;
        this.preferencesHelper = new PreferencesHelper(context);
        this.ordersInteractor = new OrdersInteractorImpl(this);
        this.enabledInteractor = new EnabledInteractorImpl(this);
    }

    @Override
    public void getCustomers() {
        // Obtener customers
        ordersInteractor.getCustomers(preferencesHelper.getSessionVM().getImei(), idTransportation, idPartner);
    }

    @Override
    public void logOut() {
        view.showLoading("Cerrando sesión.");
        enabledInteractor.logOut(preferencesHelper.getSessionVM().getImei());
    }

    //
    //
    // CALLBACKS ORDERS|ENABLED ERRORS
    //
    //

    @Override
    public void errorRequest(Integer errorCode, String method) {
        switch (method){
            case "logOut":
                preferencesHelper.removeSession();
                view.hideDialog();
                view.closeSession();
                break;
            case "getCustomers":
                view.hideRefresh();
                break;
            default:
                view.hideDialog();
                break;
        }
    }

    @Override
    public void unauthorized(String method) {
        switch (method){
            case "logOut":
                preferencesHelper.removeSession();
                view.hideDialog();
                view.closeSession();
                break;
            case "getCustomers":
                view.hideRefresh();
                break;
            default:
                view.hideDialog();
                break;
        }
    }

    //
    //
    // CALLBACKS ENABLED SUCCESS
    //
    //

    @Override
    public void successCheckImei(ResponseDeviceEnabledBean response) {
        // No se utiliza
    }

    @Override
    public void successEnabledDevice(ResponseDeviceEnabledBean response) {
        // No se utiliza
    }

    @Override
    public void successLogout(ResponseLogoutBean response) {
        preferencesHelper.removeSession();
        view.hideDialog();
        view.closeSession();
    }

    //
    //
    // CALLBACKS ORDERS SUCCESS
    //
    //

    @Override
    public void successOrders(List<ResponseCustomersBean> response) {
        // No se utiliza
    }

    @Override
    public void successFinishPedidos(ResponseFinishBean response) {
        // No se utiliza
    }

    @Override
    public void successSetInicio(ResponseInicioProducto response) {
        // No se utiliza
    }

    @Override
    public void successSetTerminar(ResponseTerminarProducto response) {
        // No se utiliza
    }

    @Override
    public void successTransportations(ResponsePayrollBean transportes) {
        // No se utiliza
    }

    @Override
    public void successCustomers(List<ResponseCustomersBean> customers) {
        final List<CustomerViewModel> oldCustomers = preferencesHelper.getCustomersSelected(idTransportation, idPartner);
        final List<CustomerViewModel> oldCustomersA = new ArrayList<>();
        final List<CustomerViewModel> oldCustomersB = new ArrayList<>();
        final List<CustomerViewModel> oldCustomersC = new ArrayList<>();
        final List<CustomerViewModel> newCustomers = new ArrayList<>();
        final List<CustomerViewModel> newCustomersA = new ArrayList<>();
        final List<CustomerViewModel> newCustomersB = new ArrayList<>();
        final List<CustomerViewModel> newCustomersC = new ArrayList<>();
        newCustomers.addAll(CustomersViewModelMapper.toViewModels(customers));
        final List<CustomerViewModel> resCustomers = new ArrayList<>();

        // Validacion si hay pedidos en cache
        if(oldCustomers.size() == 0){
            resCustomers.addAll(newCustomers);
            preferencesHelper.updateCustomers(idTransportation, idPartner, resCustomers);
            view.hideRefresh();
            view.updateCustomers();
        }else{
            // Separacion de pedidos conjunto OLD
            for (CustomerViewModel oldCustomer: oldCustomers){
                boolean flag = false;
                for (CustomerViewModel newCustomer: newCustomers){
                    if(oldCustomer.getId().equals(newCustomer.getId())) {
                        if(!oldCustomer.getProgressStatus().equals(TransportationViewModel.ProgressStatus.PENDIENTE.getProgressStatus())){
                            oldCustomersB.add(oldCustomer);
                        }else{
                            oldCustomersC.add(oldCustomer);
                        }
                        flag = true;
                    }
                }
                if(!flag){
                    oldCustomersA.add(oldCustomer);
                }
            }
            // Separacion de pedidos conjunto NEW
            for (CustomerViewModel newCustomer: newCustomers){
                boolean flag = false;
                for (CustomerViewModel oldCustomer: oldCustomers){
                    if(newCustomer.getId().equals(oldCustomer.getId())) {
                        if(!oldCustomer.getProgressStatus().equals(TransportationViewModel.ProgressStatus.PENDIENTE.getProgressStatus())){
                            newCustomersB.add(newCustomer);
                        }else{
                            newCustomersC.add(newCustomer);
                        }
                        flag = true;
                    }
                }
                if(!flag){
                    newCustomersA.add(newCustomer);
                }
            }
            // Agregamos a la respuesta los grupos que no estan en conflicto
            resCustomers.addAll(oldCustomersA);
            resCustomers.addAll(newCustomersA);
            // Agregamos a la respuesta los nuevos pedidos que reemplazan a los antiguos que no estan en progreso
            if(newCustomersC.size() > 0){
                resCustomers.addAll(newCustomersC);
            }
            // Obtenemos cadena de mensaje
            String numPedidos = "";
            for(int i = 0; i < oldCustomersB.size(); i++){
                numPedidos += oldCustomersB.get(i).getId();
                if (i < oldCustomersB.size()-1){
                    numPedidos += " - ";
                }
            }
            // Si hay pedidos en conflicto se pide al usuario decidir
            if(oldCustomersB.size() > 0){
                view.hideRefresh();
                view.showDialogDecision(new Dialog() {
                    @Override
                    public void aceptarDialog() {
                        resCustomers.addAll(newCustomersB);
                        Collections.sort(resCustomers, new MainCustomersPresenterImpl.SortCustomersbyId());
                        preferencesHelper.updateCustomers(idTransportation, idPartner, resCustomers);
                        preferencesHelper.updateStatusProgress();
                        view.updateCustomers();
                        view.hideDialog();
                    }
                    @Override
                    public void rechazarDialog() {
                        resCustomers.addAll(oldCustomersB);
                        Collections.sort(resCustomers, new MainCustomersPresenterImpl.SortCustomersbyId());
                        preferencesHelper.updateCustomers(idTransportation, idPartner, resCustomers);
                        preferencesHelper.updateStatusProgress();
                        view.updateCustomers();
                        view.hideDialog();
                    }
                },"Se actualizarán los siguientes pedidos:<br><br><b>" + numPedidos + "</b><br><br><b>¿Deseas actualizarlo?</b><br><br>(Esta acción borrará el progreso de estos pedidos).","Si","No");
            }else{
                Collections.sort(resCustomers, new MainCustomersPresenterImpl.SortCustomersbyId());
                preferencesHelper.updateCustomers(idTransportation, idPartner, resCustomers);
                preferencesHelper.updateStatusProgress();
                view.hideRefresh();
                view.updateCustomers();
            }
        }
    }

    @Override
    public void successUnlock(ResponseUnlockBean unlock) {
        // No se utiliza
    }

    //
    //
    // ORDENAMIENTO
    //
    //

    public static class SortCustomersbyId implements Comparator<CustomerViewModel>
    {
        public int compare(CustomerViewModel a, CustomerViewModel b)
        {
            return Integer.parseInt(a.getId()) - Integer.parseInt(b.getId());
        }
    }
}
