package com.cbc.auxiliar.domain.presenters;

import com.cbc.auxiliar.presentation.viewcontrollers.base.BaseActivityView;

/**
 * -----------------------------------------------------------------------------------------------------------
 * |                                               CBC AUXILIAR                                              |
 * -----------------------------------------------------------------------------------------------------------
 *
 * FECHA           AUTOR               J.PROYECTO              PROYECTO                DESCRIPCION
 * -----------------------------------------------------------------------------------------------------------
 * 04/10/2020       Joel Cauti          Alexander Olivares      CBC Picking             Creacion Presenter
 */

public interface MainCustomersPresenter {

    void getCustomers();
    void logOut();

    interface View extends BaseActivityView {

        void showLoading();
        void showLoading(String message);
        void hideDialog();
        void showAlert(String message);
        void showDialogDecision(Dialog dialog, String message);
        void showDialogDecision(Dialog dialog, String message, String labelAceptar, String labelRechazar);
        void updateCustomers();
        void closeSession();
        void hideRefresh();
    }

    interface Dialog{

        void aceptarDialog();
        void rechazarDialog();
    }
}
