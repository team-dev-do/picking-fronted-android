package com.cbc.auxiliar.domain.presenters;

import android.content.Context;

import com.cbc.auxiliar.data.datamodels.request.PedidosFinishBean;
import com.cbc.auxiliar.data.datamodels.request.ProductFinishBean;
import com.cbc.auxiliar.data.datamodels.responses.ResponseDeviceEnabledBean;
import com.cbc.auxiliar.data.datamodels.responses.ResponseFinishBean;
import com.cbc.auxiliar.data.datamodels.responses.ResponseInicioProducto;
import com.cbc.auxiliar.data.datamodels.responses.ResponseCustomersBean;
import com.cbc.auxiliar.data.datamodels.responses.ResponseLogoutBean;
import com.cbc.auxiliar.data.datamodels.responses.ResponsePayrollBean;
import com.cbc.auxiliar.data.datamodels.responses.ResponseTerminarProducto;
import com.cbc.auxiliar.data.datamodels.responses.ResponseUnlockBean;
import com.cbc.auxiliar.data.interactors.EnabledInteractor;
import com.cbc.auxiliar.data.interactors.EnabledInteractorImpl;
import com.cbc.auxiliar.data.interactors.OrdersInteractor;
import com.cbc.auxiliar.data.interactors.OrdersInteractorImpl;
import com.cbc.auxiliar.domain.helpers.PreferencesHelper;
import com.cbc.auxiliar.presentation.viewmodels.CustomerViewModel;
import com.cbc.auxiliar.presentation.viewmodels.ArticleViewModel;

import java.util.ArrayList;
import java.util.List;

import static com.cbc.auxiliar.presentation.viewmodels.CustomerViewModel.ProgressStatus.TERMINADO;

/**
 * -----------------------------------------------------------------------------------------------------------
 * |                                               CBC AUXILIAR                                              |
 * -----------------------------------------------------------------------------------------------------------
 *
 * FECHA           AUTOR               J.PROYECTO              PROYECTO                DESCRIPCION
 * -----------------------------------------------------------------------------------------------------------
 * 04/10/2020       Joel Cauti          Alexander Olivares      CBC Picking             Creacion Implementacion Presenter
 */

public class MainArticlesPresenterImpl implements MainArticlesPresenter, OrdersInteractor.Callbacks, EnabledInteractor.Callbacks {

    private final View view;
    private final Context context;
    private final OrdersInteractor ordersInteractor;
    private final EnabledInteractor enabledInteractor;
    private final PreferencesHelper preferencesHelper;
    private CustomerViewModel customer;
    private String idTransportation;
    private String idPartner;

    public MainArticlesPresenterImpl(View view, String idTransportation, String idPartner) {
        this.view = view;
        this.context = (Context) view;
        this.idTransportation = idTransportation;
        this.idPartner = idPartner;
        this.ordersInteractor = new OrdersInteractorImpl(this);
        this.enabledInteractor = new EnabledInteractorImpl(this);
        this.preferencesHelper = new PreferencesHelper(this.context);
    }

    @Override
    public void finalizarPedido(CustomerViewModel order) {
        view.showLoading("Guardando planilla.");
        this.customer = order;
        PedidosFinishBean pedidos = new PedidosFinishBean();
        pedidos.setIdOrder(Integer.parseInt(order.getId()));
        List<ProductFinishBean> productos = new ArrayList<>();
        for(ArticleViewModel product: order.getArticles()){
            ProductFinishBean producto = new ProductFinishBean();
            producto.setId(Integer.parseInt(product.getId()));
            producto.setCount(product.getAmountFound());
            producto.setMessage(product.getMessage());
            productos.add(producto);
        }
        pedidos.setProducts(productos);
        ordersInteractor.finishOrders(preferencesHelper.getSessionVM().getImei(),pedidos);
    }

    @Override
    public void setTerminar(String id) {
        ordersInteractor.setTerminar(preferencesHelper.getSessionVM().getImei(), id);
    }

    @Override
    public void logOut() {
        view.showLoading("Cerrando sesión.");
        enabledInteractor.logOut(preferencesHelper.getSessionVM().getImei());
    }

    //
    //
    // CALLBACKS ORDERS|ENABLED ERRORS
    //
    //

    @Override
    public void errorRequest(Integer errorCode, String method) {
        view.hideDialog();
    }

    @Override
    public void unauthorized(String method) {
        view.hideDialog();
    }

    //
    //
    // CALLBACKS ENABLED SUCCESS
    //
    //

    @Override
    public void successCheckImei(ResponseDeviceEnabledBean response) {
        // No se utiliza
    }

    @Override
    public void successEnabledDevice(ResponseDeviceEnabledBean response) {
        // No se utiliza
    }

    @Override
    public void successLogout(ResponseLogoutBean response) {
        preferencesHelper.removeSession();
        view.hideDialog();
        view.closeSession();
    }

    //
    //
    // CALLBACKS ORDERS SUCCESS
    //
    //

    @Override
    public void successOrders(List<ResponseCustomersBean> response) {
        view.hideDialog();
    }

    @Override
    public void successFinishPedidos(ResponseFinishBean response) {
        List<CustomerViewModel> customers = preferencesHelper.getCustomersSelected(idTransportation, idPartner);
        for(int k = 0; k < customers.size(); k++){
            if(customers.get(k).getId().equals(this.customer.getId())){
                customers.get(k).setProgressStatus(TERMINADO.getProgressStatus());
            }
        }
        preferencesHelper.updateCustomers(idTransportation, idPartner, customers);
        preferencesHelper.updateStatusProgress();
        view.showAlert("Terminaste los pedidos para " + this.customer.getName(), 3, true);
    }

    @Override
    public void successSetInicio(ResponseInicioProducto response) {
        // No se utiliza
    }

    @Override
    public void successSetTerminar(ResponseTerminarProducto response) {
        // No se utiliza
    }

    @Override
    public void successTransportations(ResponsePayrollBean transportes) {
        // No se utiliza
    }

    @Override
    public void successCustomers(List<ResponseCustomersBean> customers) {
        // No se utiliza
    }

    @Override
    public void successUnlock(ResponseUnlockBean unlock) {
        // No se utiliza
    }
}
