package com.cbc.auxiliar.domain.presenters;

import android.content.Context;

import com.cbc.auxiliar.data.datamodels.responses.ResponseCustomersBean;
import com.cbc.auxiliar.data.datamodels.responses.ResponseDeviceEnabledBean;
import com.cbc.auxiliar.data.datamodels.responses.ResponseFinishBean;
import com.cbc.auxiliar.data.datamodels.responses.ResponseInicioProducto;
import com.cbc.auxiliar.data.datamodels.responses.ResponseLogoutBean;
import com.cbc.auxiliar.data.datamodels.responses.ResponsePayrollBean;
import com.cbc.auxiliar.data.datamodels.responses.ResponseTerminarProducto;
import com.cbc.auxiliar.data.datamodels.responses.ResponseUnlockBean;
import com.cbc.auxiliar.data.interactors.EnabledInteractor;
import com.cbc.auxiliar.data.interactors.EnabledInteractorImpl;
import com.cbc.auxiliar.data.interactors.OrdersInteractor;
import com.cbc.auxiliar.data.interactors.OrdersInteractorImpl;
import com.cbc.auxiliar.domain.helpers.PreferencesHelper;
import com.cbc.auxiliar.presentation.viewmappers.CustomersViewModelMapper;
import com.cbc.auxiliar.presentation.viewmodels.CustomerViewModel;
import com.cbc.auxiliar.presentation.viewmodels.PartnerViewModel;
import com.cbc.auxiliar.presentation.viewmodels.TransportationViewModel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import static com.cbc.auxiliar.presentation.viewmodels.PartnerViewModel.ProgressStatus.ASIGNADO;
import static com.cbc.auxiliar.presentation.viewmodels.PartnerViewModel.ProgressStatus.PENDIENTE;
import static com.cbc.auxiliar.presentation.viewmodels.PartnerViewModel.ProgressStatus.PROCESO;

/**
 * -----------------------------------------------------------------------------------------------------------
 * |                                               CBC AUXILIAR                                              |
 * -----------------------------------------------------------------------------------------------------------
 *
 * FECHA           AUTOR               J.PROYECTO              PROYECTO                DESCRIPCION
 * -----------------------------------------------------------------------------------------------------------
 * 09/12/2020       Joel Cauti          Alexander Olivares      CBC Picking             Creacion Implementacion Presenter
 */

public class MainPartnersPresenterImpl implements MainPartnersPresenter, OrdersInteractor.Callbacks, EnabledInteractor.Callbacks {

    private final View view;
    private final Context context;
    private final PreferencesHelper preferencesHelper;
    private final OrdersInteractor ordersInteractor;
    private final EnabledInteractor enabledInteractor;
    private String idTransportation;
    private String idPartner;
    private String namePartner;

    public MainPartnersPresenterImpl(View view) {
        this.view = view;
        this.context = (Context) view;
        this.preferencesHelper = new PreferencesHelper(context);
        this.ordersInteractor = new OrdersInteractorImpl(this);
        this.enabledInteractor = new EnabledInteractorImpl(this);
    }

    @Override
    public void getPartners() {
        view.updatePartners();
    }

    @Override
    public void logOut() {
        view.showLoading("Cerrando sesión.");
        enabledInteractor.logOut(preferencesHelper.getSessionVM().getImei());
    }

    @Override
    public void getCustomers(String transportation, String partner) {
        view.showLoading("Verificando disponibilidad.");
        this.idTransportation = transportation;
        this.idPartner = partner;
        ordersInteractor.getCustomers(preferencesHelper.getSessionVM().getImei(), transportation, partner);
    }

    @Override
    public void unlockFamily(final String idTransportation, final String id) {
        this.idTransportation = idTransportation;
        this.idPartner = id;
        view.showDialogDecision(new Dialog() {
            @Override
            public void aceptarDialog() {
                ordersInteractor.unlockCustomers(preferencesHelper.getSessionVM().getImei(), idTransportation, id);
                view.showLoading("Liberando familia.");
            }

            @Override
            public void rechazarDialog() {
                view.hideDialog();
            }
        }, "<b>¿Deseas liberar la familia seleccionada?</b><br><br>Esta accion borrara los clientes asignado y su progreso, para el cliente seleccionado.", "Si, liberar","No, cancelar");
    }

    //
    //
    // CALLBACKS ENABLED ERRORS
    //
    //

    @Override
    public void errorRequest(Integer errorCode, String method) {
        switch (method){
            case "logOut":
                preferencesHelper.removeSession();
                view.hideDialog();
                view.closeSession();
                break;
            case "unlockCustomers":
                List<TransportationViewModel> transportes = preferencesHelper.getSessionVM().getTransportes();
                for(TransportationViewModel transporte : transportes){
                    if(transporte.getId().equals(idTransportation)){
                        for (PartnerViewModel partner : transporte.getPartners()){
                            if(partner.getId().equals(idPartner)){
                                partner.setCustomers(new ArrayList<CustomerViewModel>());
                                partner.setCustomersComplete(0);
                                partner.setProgressStatus(PENDIENTE.getProgressStatus());
                                partner.setSelected(false);
                            }
                        }
                    }
                }
                preferencesHelper.updateTransportations(transportes);
                preferencesHelper.updateStatusProgress();
                view.updatePartners();
                view.hideDialog();
                break;
            default:
                view.hideDialog();
                break;
        }
    }

    @Override
    public void unauthorized(String method) {
        switch (method){
            case "logOut":
                preferencesHelper.removeSession();
                view.hideDialog();
                view.closeSession();
                break;
            case "getCustomers":
                // La familia ya fue asignada a otro usuario
                Integer partnersComplete = 0;
                List<TransportationViewModel> transportes = new ArrayList<>();
                transportes.addAll(preferencesHelper.getSessionVM().getTransportes());
                for (TransportationViewModel transporte : transportes){
                    if(transporte.getId().equals(idTransportation)){
                        for (PartnerViewModel familia : transporte.getPartners()){
                            if(familia.getId().equals(idPartner)){
                                familia.setProgressStatus(ASIGNADO.getProgressStatus());
                            }
                            if(!familia.getProgressStatus().equals(PENDIENTE.getProgressStatus())){
                                partnersComplete++;
                            }
                        }
                        transporte.setPartnersComplete(partnersComplete);
                    }
                }
                preferencesHelper.updateTransportations(transportes);
                view.hideDialog();
                view.updatePartners();
                view.showAlert("La familia seleccionada se encuentra asignada a otro usuario.");
                break;
            default:
                view.hideDialog();
                break;
        }
    }

    //
    //
    // CALLBACKS ORDERS SUCCESS
    //
    //

    @Override
    public void successOrders(List<ResponseCustomersBean> response) {
        // No se utiliza
    }

    @Override
    public void successFinishPedidos(ResponseFinishBean response) {
        // No se utiliza
    }

    @Override
    public void successSetInicio(ResponseInicioProducto response) {
        // No se utiliza
    }

    @Override
    public void successSetTerminar(ResponseTerminarProducto response) {
        // No se utiliza
    }

    @Override
    public void successTransportations(ResponsePayrollBean transportes) {
        // No se utiliza
    }

    @Override
    public void successCustomers(List<ResponseCustomersBean> customers) {
        // Validar si es nuevo o ya existe en almacenamiento local
        boolean isLocal = false;
        Integer partnersComplete = 0;
        List<TransportationViewModel> transportes = new ArrayList<>();
        transportes.addAll(preferencesHelper.getSessionVM().getTransportes());
        List<CustomerViewModel> clientes = new ArrayList<>();
        // Validacion si los datos estan en local
        for (TransportationViewModel transporte : transportes){
            if(transporte.getId().equals(idTransportation)){
                for (PartnerViewModel familia : transporte.getPartners()){
                    if(familia.getId().equals(idPartner)){
                        this.namePartner = familia.getName();
                        clientes = familia.getCustomers();
                        if(clientes.size() > 0){
                            isLocal = true;
                        }
                    }
                }
            }
        }
        // Si no esta en local se procede a almacenar en local
        if(!isLocal){
            for (TransportationViewModel transporte : transportes) {
                if (transporte.getId().equals(idTransportation)) {
                    for (PartnerViewModel familia : transporte.getPartners()) {
                        if (familia.getId().equals(idPartner)) {
                            familia.setCustomers(CustomersViewModelMapper.toViewModels(customers));
                            familia.setProgressStatus(PROCESO.getProgressStatus());
                            transporte.setProgressStatus(TransportationViewModel.ProgressStatus.PROCESO.getProgressStatus());
                        }
                        if(!familia.getProgressStatus().equals(PENDIENTE.getProgressStatus())){
                            partnersComplete++;
                        }
                    }
                    transporte.setPartnersComplete(partnersComplete);
                }
            }
            preferencesHelper.updateTransportations(transportes);
            view.updatePartners();
            view.hideDialog();
            view.gotoCustomers(idTransportation, idPartner, namePartner);
        // En caso contrarion verificamos si hay progreso en el pedido
        }else{
            final List<CustomerViewModel> oldCustomers = clientes;
            final List<CustomerViewModel> oldCustomersA = new ArrayList<>();
            final List<CustomerViewModel> oldCustomersB = new ArrayList<>();
            final List<CustomerViewModel> oldCustomersC = new ArrayList<>();
            final List<CustomerViewModel> newCustomers = new ArrayList<>();
            final List<CustomerViewModel> newCustomersA = new ArrayList<>();
            final List<CustomerViewModel> newCustomersB = new ArrayList<>();
            final List<CustomerViewModel> newCustomersC = new ArrayList<>();
            newCustomers.addAll(CustomersViewModelMapper.toViewModels(customers));
            final List<CustomerViewModel> resCustomers = new ArrayList<>();

            // Separacion de pedidos conjunto OLD
            for (CustomerViewModel oldCustomer: oldCustomers){
                boolean flag = false;
                for (CustomerViewModel newCustomer: newCustomers){
                    if(oldCustomer.getId().equals(newCustomer.getId())) {
                        if(!oldCustomer.getProgressStatus().equals(TransportationViewModel.ProgressStatus.PENDIENTE.getProgressStatus())){
                            oldCustomersB.add(oldCustomer);
                        }else{
                            oldCustomersC.add(oldCustomer);
                        }
                        flag = true;
                    }
                }
                if(!flag){
                    oldCustomersA.add(oldCustomer);
                }
            }
            // Separacion de pedidos conjunto NEW
            for (CustomerViewModel newCustomer: newCustomers){
                boolean flag = false;
                for (CustomerViewModel oldCustomer: oldCustomers){
                    if(newCustomer.getId().equals(oldCustomer.getId())) {
                        if(!oldCustomer.getProgressStatus().equals(TransportationViewModel.ProgressStatus.PENDIENTE.getProgressStatus())){
                            newCustomersB.add(newCustomer);
                        }else{
                            newCustomersC.add(newCustomer);
                        }
                        flag = true;
                    }
                }
                if(!flag){
                    newCustomersA.add(newCustomer);
                }
            }
            // Agregamos a la respuesta los grupos que no estan en conflicto
            resCustomers.addAll(oldCustomersA);
            resCustomers.addAll(newCustomersA);
            // Agregamos a la respuesta los nuevos pedidos que reemplazan a los antiguos que no estan en progreso
            if(newCustomersC.size() > 0){
                resCustomers.addAll(newCustomersC);
            }
            // Obtenemos cadena de mensaje
            String numPedidos = "";
            for(int i = 0; i < oldCustomersB.size(); i++){
                numPedidos += oldCustomersB.get(i).getId();
                if (i < oldCustomersB.size()-1){
                    numPedidos += " - ";
                }
            }
            // Si hay pedidos en conflicto se pide al usuario decidir
            if(oldCustomersB.size() > 0){
                view.hideDialog();
                view.showDialogDecision(new MainPartnersPresenter.Dialog() {
                    @Override
                    public void aceptarDialog() {
                        resCustomers.addAll(newCustomersB);
                        Collections.sort(resCustomers, new SortCustomersbyId());
                        preferencesHelper.updateCustomers(idTransportation, idPartner, resCustomers);
                        preferencesHelper.updateStatusProgress();
                        view.updatePartners();
                        view.hideDialog();
                        view.gotoCustomers(idTransportation, idPartner, namePartner);
                    }
                    @Override
                    public void rechazarDialog() {
                        resCustomers.addAll(oldCustomersB);
                        Collections.sort(resCustomers, new SortCustomersbyId());
                        preferencesHelper.updateCustomers(idTransportation, idPartner, resCustomers);
                        preferencesHelper.updateStatusProgress();
                        view.updatePartners();
                        view.hideDialog();
                        view.gotoCustomers(idTransportation, idPartner, namePartner);
                    }
                },"Se actualizarán los siguientes pedidos:<br><br><b>" + numPedidos + "</b><br><br><b>¿Deseas actualizarlo?</b><br><br>(Esta acción borrará el progreso de estos pedidos).","Si","No");
            }else{
                Collections.sort(resCustomers, new SortCustomersbyId());
                preferencesHelper.updateCustomers(idTransportation, idPartner, resCustomers);
                preferencesHelper.updateStatusProgress();
                view.updatePartners();
                view.hideDialog();
                view.gotoCustomers(idTransportation, idPartner, namePartner);
            }
        }

    }

    @Override
    public void successUnlock(ResponseUnlockBean unlock) {
        List<TransportationViewModel> transportes = preferencesHelper.getSessionVM().getTransportes();
        for(TransportationViewModel transporte : transportes){
            if(transporte.getId().equals(idTransportation)){
                for (PartnerViewModel partner : transporte.getPartners()){
                    if(partner.getId().equals(idPartner)){
                        partner.setCustomers(new ArrayList<CustomerViewModel>());
                        partner.setCustomersComplete(0);
                        partner.setProgressStatus(PENDIENTE.getProgressStatus());
                        partner.setSelected(false);
                    }
                }
            }
        }
        preferencesHelper.updateTransportations(transportes);
        preferencesHelper.updateStatusProgress();
        view.updatePartners();
        view.hideDialog();
    }

    //
    //
    // CALLBACKS ENABLED SUCCESS
    //
    //

    @Override
    public void successCheckImei(ResponseDeviceEnabledBean response) {
        // No se utiliza
    }

    @Override
    public void successEnabledDevice(ResponseDeviceEnabledBean response) {
        // No se utiliza
    }

    @Override
    public void successLogout(ResponseLogoutBean response) {
        preferencesHelper.removeSession();
        view.hideDialog();
        view.closeSession();
    }

    //
    //
    // ORDENAMIENTO
    //
    //

    public static class SortCustomersbyId implements Comparator<CustomerViewModel>
    {
        public int compare(CustomerViewModel a, CustomerViewModel b)
        {
            return Integer.parseInt(a.getId()) - Integer.parseInt(b.getId());
        }
    }
}
