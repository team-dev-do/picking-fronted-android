package com.cbc.auxiliar.domain.presenters;

import android.content.Context;

import com.cbc.auxiliar.domain.helpers.PreferencesHelper;
import com.cbc.auxiliar.presentation.viewmodels.SessionViewModel;

import static com.cbc.auxiliar.domain.helpers.DeviceHelper.getVersion;

/**
 * -----------------------------------------------------------------------------------------------------------
 * |                                               CBC AUXILIAR                                              |
 * -----------------------------------------------------------------------------------------------------------
 *
 * FECHA           AUTOR               J.PROYECTO              PROYECTO                DESCRIPCION
 * -----------------------------------------------------------------------------------------------------------
 * 04/10/2020       Joel Cauti          Alexander Olivares      CBC Picking             Creacion Implementacion Presenter
 */

public class WelcomePresenterImpl implements WelcomePresenter {

    private final View view;
    private final Context context;
    private final PreferencesHelper preferencesHelper;

    public WelcomePresenterImpl(View view) {
        this.view = view;
        this.context = (Context) view;
        this.preferencesHelper = new PreferencesHelper(context);
    }

    @Override
    public void iniciarSesion() {
        SessionViewModel svm = preferencesHelper.getSessionVM();
        svm.setStatusSession(true);
        svm.setVersion(getVersion());
        preferencesHelper.setSessionVM(svm);
        view.goToMain();
    }
}
