package com.cbc.auxiliar.domain.presenters;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.os.Build;

import com.cbc.auxiliar.data.datamodels.responses.ResponseDeviceEnabledBean;
import com.cbc.auxiliar.data.datamodels.responses.ResponseLogoutBean;
import com.cbc.auxiliar.data.interactors.EnabledInteractor;
import com.cbc.auxiliar.data.interactors.EnabledInteractorImpl;
import com.cbc.auxiliar.domain.helpers.DeviceHelper;
import com.cbc.auxiliar.domain.helpers.PermissionHelper;
import com.cbc.auxiliar.domain.helpers.PreferencesHelper;
import com.cbc.auxiliar.presentation.viewmodels.SessionViewModel;

/**
 * -----------------------------------------------------------------------------------------------------------
 * |                                               CBC AUXILIAR                                              |
 * -----------------------------------------------------------------------------------------------------------
 *
 * FECHA           AUTOR               J.PROYECTO              PROYECTO                DESCRIPCION
 * -----------------------------------------------------------------------------------------------------------
 * 04/10/2020       Joel Cauti          Alexander Olivares      CBC Picking             Creacion Implementacion Presenter
 */

public class EnabledPresenterImpl implements EnabledPresenter, EnabledInteractor.Callbacks {

    private String imei = "";

    private final View view;
    private final Context context;
    private final PreferencesHelper preferencesHelper;
    private final EnabledInteractor enabledInteractor;

    public EnabledPresenterImpl(View view) {
        this.view = view;
        this.context = (Context) view;
        this.preferencesHelper = new PreferencesHelper(context);
        this.enabledInteractor = new EnabledInteractorImpl(this);
    }

    @Override
    public void checkImei() {
        if(Build.VERSION.SDK_INT < Build.VERSION_CODES.Q){
            imei = "IMEI-"+DeviceHelper.getImei(this.context);
        }else{
            imei = "ID-"+DeviceHelper.getId(this.context);
        }
        view.showLoading();
        enabledInteractor.checkImei(imei);
    }

    @Override
    public void getPermissions() {
        if(!PermissionHelper.isGranted(context, Manifest.permission.READ_PHONE_STATE)
                || !PermissionHelper.isGranted(context, Manifest.permission.ACCESS_FINE_LOCATION)
                || !PermissionHelper.isGranted(context, Manifest.permission.ACCESS_COARSE_LOCATION)
                || !PermissionHelper.isGranted(context, Manifest.permission.READ_EXTERNAL_STORAGE)
                || !PermissionHelper.isGranted(context, Manifest.permission.WRITE_EXTERNAL_STORAGE)){
            PermissionHelper.request((Activity) view, new String[]{
                    Manifest.permission.READ_PHONE_STATE,
                    Manifest.permission.ACCESS_FINE_LOCATION,
                    Manifest.permission.ACCESS_COARSE_LOCATION,
                    Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE});
        }else{
            view.setPermissionsGranted(true);
        }
    }

    @Override
    public void enabledDevice(String code) {
        if(Build.VERSION.SDK_INT < Build.VERSION_CODES.Q){
            imei = "IMEI-"+DeviceHelper.getImei(this.context);
        }else{
            imei = "ID-"+DeviceHelper.getId(this.context);
        }
        view.showLoading("Activando dispositivo...");
        enabledInteractor.enabledDevice(code,imei);
    }

    //
    //
    // CALLBACKS ENABLED ERRORS
    //
    //

    @Override
    public void errorRequest(Integer errorCode, String method) {
        view.hideLoading();
        view.showRegisterDevice();
    }

    @Override
    public void unauthorized(String method) {
        view.hideLoading();
        view.showRegisterDevice();
    }

    //
    //
    // CALLBACKS ENABLED SUCCESS
    //
    //

    @Override
    public void successCheckImei(ResponseDeviceEnabledBean response) {
        SessionViewModel svm = new SessionViewModel();
        svm.setImei(imei);
        svm.setCode(response.getCode());
        svm.setId(String.valueOf(response.getId()));
        preferencesHelper.setSessionVM(svm);
        view.hideLoading();
        view.goToWelcome();
    }

    @Override
    public void successEnabledDevice(ResponseDeviceEnabledBean response) {
        SessionViewModel svm = new SessionViewModel();
        svm.setImei(imei);
        svm.setCode(response.getCode());
        svm.setId(String.valueOf(response.getId()));
        preferencesHelper.setSessionVM(svm);
        view.hideLoading();
        view.goToWelcome();
    }

    @Override
    public void successLogout(ResponseLogoutBean response) {
        // No se utiliza
    }
}
