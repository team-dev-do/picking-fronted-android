package com.cbc.auxiliar.domain.presenters;

import com.cbc.auxiliar.presentation.viewcontrollers.base.BaseActivityView;

/**
 * -----------------------------------------------------------------------------------------------------------
 * |                                               CBC AUXILIAR                                              |
 * -----------------------------------------------------------------------------------------------------------
 *
 * FECHA           AUTOR               J.PROYECTO              PROYECTO                DESCRIPCION
 * -----------------------------------------------------------------------------------------------------------
 * 12/12/2020       Joel Cauti          Alexander Olivares      CBC Picking             Creacion Presenter
 */

public interface MainTransportationsPresenter {

    void getTransportations();
    void logOut();

    interface View extends BaseActivityView {

        void hideDialog();
        void showDialogDecision(Dialog dialog, String message, String labelAceptar, String labelRechazar);
        void showDialogDecision(Dialog dialog, String message);
        void showNotTransportations();
        void updateTransportations();
        void showAlert(String message, SimpleDialog dialog);
        void showLoading(String message);
        void closeSession();
        void hideRefresh();
        void showAlert(String message);
    }

    interface Dialog {

        void aceptarDialog();
        void rechazarDialog();
    }

    interface SimpleDialog {

        void aceptarDialog();
    }
}
