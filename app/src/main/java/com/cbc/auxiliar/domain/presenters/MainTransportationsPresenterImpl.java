package com.cbc.auxiliar.domain.presenters;

import android.content.Context;

import com.cbc.auxiliar.data.datamodels.responses.ResponseDeviceEnabledBean;
import com.cbc.auxiliar.data.datamodels.responses.ResponseFinishBean;
import com.cbc.auxiliar.data.datamodels.responses.ResponseInicioProducto;
import com.cbc.auxiliar.data.datamodels.responses.ResponseLogoutBean;
import com.cbc.auxiliar.data.datamodels.responses.ResponseCustomersBean;
import com.cbc.auxiliar.data.datamodels.responses.ResponsePayrollBean;
import com.cbc.auxiliar.data.datamodels.responses.ResponseTerminarProducto;
import com.cbc.auxiliar.data.datamodels.responses.ResponseUnlockBean;
import com.cbc.auxiliar.data.interactors.EnabledInteractor;
import com.cbc.auxiliar.data.interactors.EnabledInteractorImpl;
import com.cbc.auxiliar.data.interactors.OrdersInteractor;
import com.cbc.auxiliar.data.interactors.OrdersInteractorImpl;
import com.cbc.auxiliar.domain.helpers.PreferencesHelper;
import com.cbc.auxiliar.presentation.viewmappers.TransportationsViewModelMapper;
import com.cbc.auxiliar.presentation.viewmodels.SessionViewModel;
import com.cbc.auxiliar.presentation.viewmodels.TransportationViewModel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import static com.cbc.auxiliar.presentation.viewmodels.TransportationViewModel.ProgressStatus.PENDIENTE;

/**
 * -----------------------------------------------------------------------------------------------------------
 * |                                               CBC AUXILIAR                                              |
 * -----------------------------------------------------------------------------------------------------------
 *
 * FECHA           AUTOR               J.PROYECTO              PROYECTO                DESCRIPCION
 * -----------------------------------------------------------------------------------------------------------
 * 09/12/2020       Joel Cauti          Alexander Olivares      CBC Picking             Creacion Implementacion Presenter
 */

public class MainTransportationsPresenterImpl implements MainTransportationsPresenter, OrdersInteractor.Callbacks, EnabledInteractor.Callbacks{

    private final View view;
    private final Context context;
    private final PreferencesHelper preferencesHelper;
    private final OrdersInteractor ordersInteractor;
    private final EnabledInteractor enabledInteractor;

    public MainTransportationsPresenterImpl(View view) {
        this.view = view;
        this.context = (Context) view;
        this.preferencesHelper = new PreferencesHelper(context);
        this.ordersInteractor = new OrdersInteractorImpl(this);
        this.enabledInteractor = new EnabledInteractorImpl(this);
    }

    @Override
    public void getTransportations() {
        ordersInteractor.getTransportations(preferencesHelper.getSessionVM().getImei());
    }

    @Override
    public void logOut() {
        view.showLoading("Cerrando sesión.");
        enabledInteractor.logOut(preferencesHelper.getSessionVM().getImei());
    }

    //
    //
    // CALLBACKS ORDERS|ENABLED ERRORS
    //
    //

    @Override
    public void errorRequest(Integer errorCode, String method) {
        switch (method){
            case "logOut":
                preferencesHelper.removeSession();
                view.hideDialog();
                view.closeSession();
                break;
            default:
                view.showNotTransportations();
                break;
        }
    }

    @Override
    public void unauthorized(String method) {
        switch (method){
            case "logOut":
                preferencesHelper.removeSession();
                view.hideDialog();
                view.closeSession();
                break;
            default:
                view.showNotTransportations();
                break;
        }
    }

    //
    //
    // CALLBACKS ENABLED SUCCESS
    //
    //

    @Override
    public void successCheckImei(ResponseDeviceEnabledBean response) {
        // No se utiliza
    }

    @Override
    public void successEnabledDevice(ResponseDeviceEnabledBean response) {
        // No se utiliza
    }

    @Override
    public void successLogout(ResponseLogoutBean response) {
        preferencesHelper.removeSession();
        view.hideDialog();
        view.closeSession();
    }

    //
    //
    // CALLBACKS ORDERS SUCCESS
    //
    //

    @Override
    public void successOrders(List<ResponseCustomersBean> response) {
        // No se utiliza
    }

    @Override
    public void successFinishPedidos(ResponseFinishBean response) {
        // No se utiliza
    }

    @Override
    public void successSetInicio(ResponseInicioProducto response) {
        // No se utiliza
    }

    @Override
    public void successSetTerminar(ResponseTerminarProducto response) {
        // No se utiliza
    }

    @Override
    public void successTransportations(final ResponsePayrollBean response) {
        final SessionViewModel sesion = preferencesHelper.getSessionVM();
        if(sesion.getVersionPayroll() != null){
            if(!sesion.getVersionPayroll().equals(String.valueOf(response.getIdOrderFile()))){
                view.showAlert("Una nueva plantilla a sido cargada, se actualizaran todos los datos temporales.", new SimpleDialog() {
                    @Override
                    public void aceptarDialog() {
                        view.hideDialog();
                        logOut();
                    }
                });
            }
        }
        sesion.setVersionPayroll(String.valueOf(response.getIdOrderFile()));
        preferencesHelper.setSessionVM(sesion);
        // Obtenemos los conjuntos y sus partes involucradas
        final List<TransportationViewModel> oldTransportations = preferencesHelper.getSessionVM().getTransportes();
        final List<TransportationViewModel> oldTransportationsA = new ArrayList<>();
        final List<TransportationViewModel> oldTransportationsB = new ArrayList<>();
        final List<TransportationViewModel> oldTransportationsC = new ArrayList<>();
        final List<TransportationViewModel> newTransportations = new ArrayList<>();
        newTransportations.addAll(TransportationsViewModelMapper.toViewModels(response.getTrucks()));
        final List<TransportationViewModel> newTransportationsA = new ArrayList<>();
        final List<TransportationViewModel> newTransportationsB = new ArrayList<>();
        final List<TransportationViewModel> newTransportationsC = new ArrayList<>();
        final List<TransportationViewModel> resultTransportations = new ArrayList<>();
        // Validacion si hay pedidos en cache
        if(preferencesHelper.getSessionVM().getTransportes().size() == 0){
            resultTransportations.addAll(newTransportations);
            preferencesHelper.updateTransportations(resultTransportations);
            preferencesHelper.updateStatusProgress();
            view.hideRefresh();
            view.updateTransportations();
        }else{
            // Separacion de pedidos conjunto OLD
            for (TransportationViewModel oldTransportation: oldTransportations){
                boolean flag = false;
                for (TransportationViewModel newTransportation: newTransportations){
                    if(oldTransportation.getId().equals(newTransportation.getId())) {
                        if(!oldTransportation.getProgressStatus().equals(PENDIENTE.getProgressStatus())){
                            oldTransportationsB.add(oldTransportation);
                        }else{
                            oldTransportationsC.add(oldTransportation);
                        }
                        flag = true;
                    }
                }
                if(!flag){
                    oldTransportationsA.add(oldTransportation);
                }
            }
            // Separacion de pedidos conjunto NEW
            for (TransportationViewModel newTransportation: newTransportations){
                boolean flag = false;
                for (TransportationViewModel oldTransportation: oldTransportations){
                    if(newTransportation.getId().equals(oldTransportation.getId())) {
                        if(!oldTransportation.getProgressStatus().equals(PENDIENTE.getProgressStatus())){
                            newTransportationsB.add(newTransportation);
                        }else{
                            newTransportationsC.add(newTransportation);
                        }
                        flag = true;
                    }
                }
                if(!flag){
                    newTransportationsA.add(newTransportation);
                }
            }
            // Agregamos a la respuesta los grupos que no estan en conflicto
            resultTransportations.addAll(oldTransportationsA);
            resultTransportations.addAll(newTransportationsA);
            // Agregamos a la respuesta los nuevos pedidos que reemplazan a los antiguos que no estan en progreso
            if(newTransportationsC.size() > 0){
                resultTransportations.addAll(newTransportationsC);
            }
            // Obtenemos cadena de mensaje
            String numPedidos = "";
            for(int i = 0; i < oldTransportationsB.size(); i++){
                numPedidos += oldTransportationsB.get(i).getId();
                if (i < oldTransportationsB.size() - 1){
                    numPedidos += " - ";
                }
            }
            // Si hay transportes en conflicto se pide al usuario decidir
            if(newTransportationsB.size() > 0){
                view.hideRefresh();
                view.showDialogDecision(new MainTransportationsPresenter.Dialog() {
                    @Override
                    public void aceptarDialog() {
                        resultTransportations.addAll(newTransportationsB);
                        Collections.sort(resultTransportations, new SortTransportationsbyId());
                        preferencesHelper.updateTransportations(resultTransportations);
                        preferencesHelper.updateStatusProgress();
                        view.updateTransportations();
                        view.hideDialog();
                    }
                    @Override
                    public void rechazarDialog() {
                        resultTransportations.addAll(oldTransportationsB);
                        Collections.sort(resultTransportations, new SortTransportationsbyId());
                        preferencesHelper.updateTransportations(resultTransportations);
                        preferencesHelper.updateStatusProgress();
                        view.updateTransportations();
                        view.hideDialog();
                    }
                }, "Se intenta cargar los siguientes transportes:<br><br><b>" + numPedidos + "</b><br><br><b>¿Deseas actualizarlo?</b><br>(Esta acción borrará el progreso en tu dispositivo de los pedidos que no hayan sido finalizados).","Si","No");
            }else{
                Collections.sort(resultTransportations, new SortTransportationsbyId());
                preferencesHelper.updateTransportations(resultTransportations);
                preferencesHelper.updateStatusProgress();
                view.hideRefresh();
                view.updateTransportations();
            }
        }
    }

    @Override
    public void successCustomers(List<ResponseCustomersBean> customers) {
        // No se utiliza
    }

    @Override
    public void successUnlock(ResponseUnlockBean unlock) {
        // No se utiliza
    }

    //
    //
    // ORDENAMIENTO
    //
    //

    public static class SortTransportationsbyId implements Comparator<TransportationViewModel>
    {
        public int compare(TransportationViewModel a, TransportationViewModel b)
        {
            return Integer.parseInt(a.getId()) - Integer.parseInt(b.getId());
        }
    }
}
