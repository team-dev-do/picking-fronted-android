package com.cbc.auxiliar.domain.helpers;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;

import com.cbc.auxiliar.presentation.viewmodels.PartnerViewModel;
import com.cbc.auxiliar.presentation.viewmodels.TransportationViewModel;
import com.google.gson.Gson;
import com.cbc.auxiliar.presentation.viewmodels.CustomerViewModel;
import com.cbc.auxiliar.presentation.viewmodels.SessionViewModel;

import java.util.ArrayList;
import java.util.List;

/**
 * -----------------------------------------------------------------------------------------------------------
 * |                                               CBC AUXILIAR                                              |
 * -----------------------------------------------------------------------------------------------------------
 *
 * FECHA           AUTOR               J.PROYECTO              PROYECTO                DESCRIPCION
 * -----------------------------------------------------------------------------------------------------------
 * 04/10/2020       Joel Cauti          Alexander Olivares      CBC Picking             Creacion Helper
 */

public class PreferencesHelper {

    private final SharedPreferences sharedPreferences;
    private final SharedPreferences.Editor editor;

    private final String DOMAIN_PREFERENCES = "CBC_AUXILIAR";
    private final String PREFERENCES_SESSION = DOMAIN_PREFERENCES+".SESSION";

    @SuppressLint("CommitPrefEdits")
    public PreferencesHelper(Context context) {
        this.sharedPreferences = context.getSharedPreferences("",Context.MODE_PRIVATE);
        this.editor = sharedPreferences.edit();
    }

    public String getSession() {
        return sharedPreferences.getString(PREFERENCES_SESSION,null);
    }

    public void setSession(String sessionData) {
        this.editor.putString(PREFERENCES_SESSION,sessionData);
        this.editor.apply();
    }

    public SessionViewModel getSessionVM(){
        String session = sharedPreferences.getString(PREFERENCES_SESSION,null);
        if(session != null){
            return new Gson().fromJson(session,SessionViewModel.class);
        }else{
            return null;
        }
    }

    public void setSessionVM(SessionViewModel sessionViewModel){
        setSession(new Gson().toJson(sessionViewModel));
    }

    public void removeSession(){
        this.editor.remove(PREFERENCES_SESSION);
        this.editor.apply();
    }

    //
    //
    // METODOS AUXILIARES
    //
    //

    public void updateTransportations(List<TransportationViewModel> transportations) {
        SessionViewModel sesion = getSessionVM();
        sesion.setTransportes(transportations);
        setSessionVM(sesion);
    }

    public List<CustomerViewModel> getCustomersSelected(String idTransportation, String idPartner) {
        List<CustomerViewModel> customers = new ArrayList<>();
        for(TransportationViewModel transporte : getSessionVM().getTransportes()){
            if(transporte.getId().equals(idTransportation)){
                for(PartnerViewModel partner : transporte.getPartners()){
                    if(partner.getId().equals(idPartner)){
                        customers = partner.getCustomers();
                    }
                }
            }
        }
        return customers;
    }

    public void updateCustomers(String idTransportation, String idPartner, List<CustomerViewModel> customers) {
        List<TransportationViewModel> transportes = getSessionVM().getTransportes();
        for(TransportationViewModel transporte : transportes){
            if(transporte.getId().equals(idTransportation)){
                for(PartnerViewModel partner : transporte.getPartners()){
                    if(partner.getId().equals(idPartner)){
                        partner.setCustomers(customers);
                    }
                }
            }
        }
        updateTransportations(transportes);
    }

    public void updateStatusProgress() {
        List<TransportationViewModel> transportations = getSessionVM().getTransportes();
        for(TransportationViewModel transportation : transportations){
            int partnerFinish = 0;
            int partnerProcess = 0;
            for(PartnerViewModel partner : transportation.getPartners()){
                int customerFinish = 0;
                int customerProcess = 0;
                for(CustomerViewModel customer : partner.getCustomers()){
                    // Actualizamos customers terminados
                    if(customer.getProgressStatus().equals(CustomerViewModel.ProgressStatus.TERMINADO.getProgressStatus())){
                        customerFinish++;
                    }else if(!customer.getProgressStatus().equals(CustomerViewModel.ProgressStatus.PENDIENTE.getProgressStatus())){
                        customerProcess++;
                    }
                }
                if(customerFinish == partner.getCustomers().size() && customerFinish > 0){
                    partner.setProgressStatus(PartnerViewModel.ProgressStatus.TERMINADO.getProgressStatus());
                    partner.setCustomersComplete(customerFinish);
                }else if((customerFinish + customerProcess) > 0){
                    partner.setProgressStatus(PartnerViewModel.ProgressStatus.PROCESO.getProgressStatus());
                    partner.setCustomersComplete(customerFinish + customerProcess);
                }
                // Actualizamos partners terminados
                if(partner.getProgressStatus().equals(PartnerViewModel.ProgressStatus.TERMINADO.getProgressStatus())){
                    partnerFinish++;
                }else if(!partner.getProgressStatus().equals(PartnerViewModel.ProgressStatus.PENDIENTE.getProgressStatus())){
                    partnerProcess++;
                }
            }
            if(partnerFinish == transportation.getPartners().size() && partnerFinish > 0){
                transportation.setProgressStatus(TransportationViewModel.ProgressStatus.TERMINADO.getProgressStatus());
                transportation.setPartnersComplete(partnerFinish);
            }else if((partnerFinish + partnerProcess) > 0){
                transportation.setProgressStatus(TransportationViewModel.ProgressStatus.PROCESO.getProgressStatus());
                transportation.setPartnersComplete(partnerFinish + partnerProcess);
            }
        }
        updateTransportations(transportations);
    }
}
