package com.cbc.auxiliar.domain.presenters;

import com.cbc.auxiliar.presentation.viewcontrollers.base.BaseActivityView;

/**
 * -----------------------------------------------------------------------------------------------------------
 * |                                               CBC AUXILIAR                                              |
 * -----------------------------------------------------------------------------------------------------------
 *
 * FECHA           AUTOR               J.PROYECTO              PROYECTO                DESCRIPCION
 * -----------------------------------------------------------------------------------------------------------
 * 04/10/2020       Joel Cauti          Alexander Olivares      CBC Picking             Creacion Presenter
 */

public interface MainItemPresenter {

    void setInicio(String id);
    void logOut();

    interface View extends BaseActivityView{
        void hideDialog();
        void showDialogDecision(Dialog dialog, String message, String labelAceptar, String labelRechazar);
        void showDialogDecision(Dialog dialog, String message);
        void showLoading(String message);
        void closeSession();
    }

     interface Dialog {
        void aceptarDialog();
        void rechazarDialog();
    }
}
