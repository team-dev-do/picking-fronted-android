package com.cbc.auxiliar.domain.presenters;

import com.cbc.auxiliar.presentation.viewcontrollers.base.BaseActivityView;

/**
 * -----------------------------------------------------------------------------------------------------------
 * |                                               CBC AUXILIAR                                              |
 * -----------------------------------------------------------------------------------------------------------
 *
 * FECHA           AUTOR               J.PROYECTO              PROYECTO                DESCRIPCION
 * -----------------------------------------------------------------------------------------------------------
 * 12/12/2020       Joel Cauti          Alexander Olivares      CBC Picking             Creacion Presenter
 */

public interface MainPartnersPresenter {

    void getPartners();
    void logOut();
    void getCustomers(String transportation, String partner);
    void unlockFamily(String idTransportation, String id);

    interface View extends BaseActivityView{

        void gotoCustomers(String transportation, String partner, String namePartner);
        void hideDialog();
        void showDialogDecision(Dialog dialog, String message, String labelAceptar, String labelRechazar);
        void showDialogDecision(Dialog dialog, String message);
        void showAlert(String message);
        void closeSession();
        void showLoading(String message);
        void updatePartners();
        void getCustomers(String idTransportation, String id);
        void liberarFamilia(String idTransportation, String id);
    }

    interface Dialog {

        void aceptarDialog();
        void rechazarDialog();
    }
}
