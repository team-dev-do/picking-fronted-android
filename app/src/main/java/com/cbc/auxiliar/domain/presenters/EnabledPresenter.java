package com.cbc.auxiliar.domain.presenters;

import com.cbc.auxiliar.presentation.viewcontrollers.base.BaseActivityView;

/**
 * -----------------------------------------------------------------------------------------------------------
 * |                                               CBC AUXILIAR                                              |
 * -----------------------------------------------------------------------------------------------------------
 *
 * FECHA           AUTOR               J.PROYECTO              PROYECTO                DESCRIPCION
 * -----------------------------------------------------------------------------------------------------------
 * 04/10/2020       Joel Cauti          Alexander Olivares      CBC Picking             Creacion Presenter
 */

public interface EnabledPresenter {

    void checkImei();
    void getPermissions();
    void enabledDevice(String code);

    interface View extends BaseActivityView {

        void setPermissionsGranted(boolean b);
        void showLoading();
        void showLoading(String message);
        void hideLoading();
        void goToWelcome();
        void showRegisterDevice();
    }
}
