package com.cbc.auxiliar.domain.helpers;

/**
 * -----------------------------------------------------------------------------------------------------------
 * |                                               CBC AUXILIAR                                              |
 * -----------------------------------------------------------------------------------------------------------
 *
 * FECHA           AUTOR               J.PROYECTO              PROYECTO                DESCRIPCION
 * -----------------------------------------------------------------------------------------------------------
 * 04/10/2020       Joel Cauti          Alexander Olivares      CBC Picking             Creacion Helper
 */

public class KeyCodesHelper {

    public static String convertirKeycode(int keyCode) {
        String caracter = "";
        switch (keyCode){
            case 7: caracter = "0"; break;
            case 8: caracter = "1"; break;
            case 9: caracter = "2"; break;
            case 10: caracter = "3"; break;
            case 11: caracter = "4"; break;
            case 12: caracter = "5"; break;
            case 13: caracter = "6"; break;
            case 14: caracter = "7"; break;
            case 15: caracter = "8"; break;
            case 16: caracter = "9"; break;
            case 29: caracter = "a"; break;
            case 30: caracter = "b"; break;
            case 31: caracter = "c"; break;
            case 32: caracter = "d"; break;
            case 33: caracter = "e"; break;
            case 34: caracter = "f"; break;
            case 35: caracter = "g"; break;
            case 36: caracter = "h"; break;
            case 37: caracter = "i"; break;
            case 38: caracter = "j"; break;
            case 39: caracter = "k"; break;
            case 40: caracter = "l"; break;
            case 41: caracter = "m"; break;
            case 42: caracter = "n"; break;
            case 43: caracter = "o"; break;
            case 44: caracter = "p"; break;
            case 45: caracter = "q"; break;
            case 46: caracter = "r"; break;
            case 47: caracter = "s"; break;
            case 48: caracter = "t"; break;
            case 49: caracter = "u"; break;
            case 50: caracter = "v"; break;
            case 51: caracter = "w"; break;
            case 52: caracter = "x"; break;
            case 53: caracter = "y"; break;
            case 54: caracter = "z"; break;
        }
        return caracter;
    }
}
