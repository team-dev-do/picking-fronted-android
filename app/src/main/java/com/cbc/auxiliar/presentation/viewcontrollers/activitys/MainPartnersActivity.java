package com.cbc.auxiliar.presentation.viewcontrollers.activitys;

import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Html;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.cbc.auxiliar.R;
import com.cbc.auxiliar.domain.helpers.PreferencesHelper;
import com.cbc.auxiliar.domain.presenters.MainPartnersPresenter;
import com.cbc.auxiliar.domain.presenters.MainPartnersPresenterImpl;
import com.cbc.auxiliar.presentation.viewadapters.RecyclerPartnersAdapter;

import static com.cbc.auxiliar.presentation.viewutils.ConstantsUtils.EXTRA_CUSTOMER_IDPARTNER;
import static com.cbc.auxiliar.presentation.viewutils.ConstantsUtils.EXTRA_CUSTOMER_IDTRANSPORTATION;
import static com.cbc.auxiliar.presentation.viewutils.ConstantsUtils.EXTRA_CUSTOMER_NAMEPARTNER;
import static com.cbc.auxiliar.presentation.viewutils.ConstantsUtils.RESULT_ACTIVITY_CUSTOMER;

/**
 * -----------------------------------------------------------------------------------------------------------
 * |                                               CBC AUXILIAR                                              |
 * -----------------------------------------------------------------------------------------------------------
 *
 * FECHA           AUTOR               J.PROYECTO              PROYECTO                DESCRIPCION
 * -----------------------------------------------------------------------------------------------------------
 * 07/12/2020       Joel Cauti          Alexander Olivares      CBC Picking             Creacion Activity
 */

public class    MainPartnersActivity extends AppCompatActivity implements MainPartnersPresenter.View {

    private RelativeLayout rlPartners;
    private RecyclerView rvPartners;
    private Toolbar toolbar;

    private RecyclerView.LayoutManager layoutManager;
    private AlertDialog loadingDialog;
    private AlertDialog.Builder builderDialog;

    private MainPartnersPresenter presenter;
    private PreferencesHelper preferencesHelper;
    private RecyclerPartnersAdapter adapter;

    //
    //
    // BASE
    //
    //

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_partners);
        getExtras();
        getSession();
        initPresenter();
        bindUI();
        initUI();
        initEvents();
        presenter.getPartners();
    }

    @Override
    public void getExtras() {

    }

    @Override
    public void getSession() {
        preferencesHelper = new PreferencesHelper(MainPartnersActivity.this);
    }

    @Override
    public void initPresenter() {
        presenter = new MainPartnersPresenterImpl(MainPartnersActivity.this);
    }

    @Override
    public void bindUI() {
        toolbar = findViewById(R.id.toolbar_partner);
        rvPartners = findViewById(R.id.rv_partners);
        rlPartners = findViewById(R.id.rl_partners);
    }

    @Override
    public void initUI() {
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_baseline_keyboard_arrow_left_24);
        layoutManager = new LinearLayoutManager(MainPartnersActivity.this);
        rvPartners.setLayoutManager(layoutManager);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(rvPartners.getContext(), DividerItemDecoration.VERTICAL);
        rvPartners.addItemDecoration(dividerItemDecoration);
    }

    @Override
    public void initEvents() {
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    //
    //
    // REDIRECCIONAMIENTOS
    //
    //

    private void goToEnabled() {
        Intent enabled = new Intent(MainPartnersActivity.this, EnabledActivity.class);
        enabled.setAction(Intent.ACTION_MAIN);
        startActivity(enabled);
        finish();
    }

    private void goToDevices() {
        Intent devices = new Intent(MainPartnersActivity.this, ConfigDevicesActivity.class);
        startActivity(devices);
    }

    @Override
    public void gotoCustomers(String transportation, String partner, String namePartner) {
        Intent customer = new Intent(MainPartnersActivity.this, MainCustomersActivity.class);
        customer.putExtra(EXTRA_CUSTOMER_IDTRANSPORTATION, transportation);
        customer.putExtra(EXTRA_CUSTOMER_IDPARTNER, partner);
        customer.putExtra(EXTRA_CUSTOMER_NAMEPARTNER, namePartner);
        startActivityForResult(customer, RESULT_ACTIVITY_CUSTOMER);
    }

    //
    //
    // RESULT ACTIVITY
    //
    //

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == RESULT_ACTIVITY_CUSTOMER){
            updatePartners();
        }
    }


    //
    //
    // DIALOGOS
    //
    //

    @Override
    public void hideDialog() {
        if(loadingDialog != null){
            loadingDialog.dismiss();
        }
    }

    @Override
    public void showDialogDecision(final MainPartnersPresenter.Dialog dialog, String message, String labelAceptar, String labelRechazar) {
        hideDialog();
        builderDialog = new AlertDialog.Builder(MainPartnersActivity.this);
        View viewLoading = getLayoutInflater().inflate(R.layout.dialog_decision,null,false);
        TextView tvMessage = viewLoading.findViewById(R.id.tv_message);
        Button aceptar = viewLoading.findViewById(R.id.btn_aceptar);
        Button rechazar = viewLoading.findViewById(R.id.btn_rechazar);
        aceptar.setText(labelAceptar);
        rechazar.setText(labelRechazar);
        tvMessage.setText(Html.fromHtml(message));
        aceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.aceptarDialog();
            }
        });
        rechazar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.rechazarDialog();
            }
        });
        builderDialog.setView(viewLoading);
        loadingDialog = builderDialog.create();
        loadingDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        loadingDialog.setCancelable(false);
        loadingDialog.show();
    }

    @Override
    public void showDialogDecision(final MainPartnersPresenter.Dialog dialog, String message) {
        hideDialog();
        builderDialog = new AlertDialog.Builder(MainPartnersActivity.this);
        View viewLoading = getLayoutInflater().inflate(R.layout.dialog_decision,null,false);
        TextView tvMessage = viewLoading.findViewById(R.id.tv_message);
        Button aceptar = viewLoading.findViewById(R.id.btn_aceptar);
        Button rechazar = viewLoading.findViewById(R.id.btn_rechazar);
        tvMessage.setText(Html.fromHtml(message));
        aceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.aceptarDialog();
            }
        });
        rechazar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.rechazarDialog();
            }
        });
        builderDialog.setView(viewLoading);
        loadingDialog = builderDialog.create();
        loadingDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        loadingDialog.setCancelable(false);
        loadingDialog.show();
    }

    @Override
    public void showLoading(String message) {
        hideDialog();
        builderDialog = new AlertDialog.Builder(MainPartnersActivity.this);
        View viewLoading = getLayoutInflater().inflate(R.layout.dialog_loading,null,false);
        TextView tvMessage = viewLoading.findViewById(R.id.tv_message);
        tvMessage.setText(message);
        builderDialog.setView(viewLoading);
        loadingDialog = builderDialog.create();
        loadingDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        loadingDialog.setCancelable(false);
        loadingDialog.show();
    }

    @Override
    public void showAlert(String message) {
        hideDialog();
        builderDialog = new AlertDialog.Builder(MainPartnersActivity.this);
        View viewLoading = getLayoutInflater().inflate(R.layout.dialog_alert,null,false);
        TextView tvMessage = viewLoading.findViewById(R.id.tv_message);
        Button aceptar = viewLoading.findViewById(R.id.btn_aceptar);
        tvMessage.setText(Html.fromHtml(message));
        aceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideDialog();
            }
        });
        builderDialog.setView(viewLoading);
        loadingDialog = builderDialog.create();
        loadingDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        loadingDialog.setCancelable(false);
        loadingDialog.show();
    }

    //
    //
    // METODOS AUXILIARES
    //
    //

    @Override
    public void closeSession() {
        goToEnabled();
    }

    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        int keycode = event.getKeyCode();
        if(keycode == KeyEvent.KEYCODE_BACK){
            return super.dispatchKeyEvent(event);
        }
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.devices:
                goToDevices();
                return true;
            case R.id.close:
                showDialogDecision(new MainPartnersPresenter.Dialog() {
                    @Override
                    public void aceptarDialog() {
                        presenter.logOut();
                    }
                    @Override
                    public void rechazarDialog() {
                        hideDialog();
                    }
                },"<b> " + getString(R.string.label_cerrar_sesion) + " </b><br><br> " + getString(R.string.text_cerrar_sesion),"Sí","No");
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    @Override
    public void updatePartners() {
        adapter = new RecyclerPartnersAdapter(MainPartnersActivity.this);
        rvPartners.setAdapter(adapter);
    }

    @Override
    public void getCustomers(String idTransportation, String id) {
        presenter.getCustomers(idTransportation, id);
    }

    @Override
    public void liberarFamilia(String idTransportation, String id) {
        presenter.unlockFamily(idTransportation, id);
    }
}
