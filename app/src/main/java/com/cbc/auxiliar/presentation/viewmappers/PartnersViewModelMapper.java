package com.cbc.auxiliar.presentation.viewmappers;

import com.cbc.auxiliar.data.datamodels.responses.ResponsePartnersBean;
import com.cbc.auxiliar.presentation.viewmodels.CustomerViewModel;
import com.cbc.auxiliar.presentation.viewmodels.PartnerViewModel;

import java.util.ArrayList;
import java.util.List;

import static com.cbc.auxiliar.presentation.viewmodels.PartnerViewModel.ProgressStatus.PENDIENTE;
import static com.cbc.auxiliar.presentation.viewmodels.PartnerViewModel.ProgressStatus.TERMINADO;

/**
 * -----------------------------------------------------------------------------------------------------------
 * |                                               CBC AUXILIAR                                              |
 * -----------------------------------------------------------------------------------------------------------
 *
 * FECHA           AUTOR               J.PROYECTO              PROYECTO                DESCRIPCION
 * ------------------------------------------------------------------------------------------------------------
 * 09/12/2020       Joel Cauti          Alexander Olivares      CBC Picking             Creacion Mapper
 */

public class PartnersViewModelMapper {

    public static PartnerViewModel toViewModel(ResponsePartnersBean partnerBean){

        PartnerViewModel partner = null;
        if(partnerBean != null) {
            partner = new PartnerViewModel();

            partner.setId(String.valueOf(partnerBean.getId()));
            partner.setName(partnerBean.getName());
            if(partnerBean.getCompleted() == 0){
                partner.setProgressStatus(PENDIENTE.getProgressStatus());
            }else if(partnerBean.getCompleted() == 1){
                partner.setProgressStatus(TERMINADO.getProgressStatus());
            }

            partner.setTypeView(0);
            partner.setSelected(false);
            partner.setCustomersComplete(0);

            List<CustomerViewModel> customers = new ArrayList<>();
            partner.setCustomers(customers);
        }
        return partner;
    }

    public static List<PartnerViewModel> toViewModels(List<ResponsePartnersBean> partnersBean){

        List<PartnerViewModel> partners = new ArrayList<>();
        if(partnersBean != null) {
            for (ResponsePartnersBean partnerBean : partnersBean) {
                PartnerViewModel partner = toViewModel(partnerBean);
                if(partner != null){
                    partners.add(partner);
                }
            }
        }
        return partners;
    }
}
