package com.cbc.auxiliar.presentation.viewcontrollers.activitys;

import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Html;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.cbc.auxiliar.R;
import com.cbc.auxiliar.domain.helpers.PreferencesHelper;
import com.cbc.auxiliar.domain.presenters.MainTransportationsPresenter;
import com.cbc.auxiliar.domain.presenters.MainTransportationsPresenterImpl;
import com.cbc.auxiliar.presentation.viewadapters.RecyclerTransportationsAdapter;

import static com.cbc.auxiliar.presentation.viewutils.ConstantsUtils.RESULT_ACTIVITY_PARTNER;

/**
 * -----------------------------------------------------------------------------------------------------------
 * |                                               CBC AUXILIAR                                              |
 * -----------------------------------------------------------------------------------------------------------
 *
 * FECHA           AUTOR               J.PROYECTO              PROYECTO                DESCRIPCION
 * -----------------------------------------------------------------------------------------------------------
 * 07/12/2020       Joel Cauti          Alexander Olivares      CBC Picking             Creacion Activity
 */

public class MainTransportationsActivity extends AppCompatActivity implements MainTransportationsPresenter.View {

    private RelativeLayout rlNotOrders, rlOrders;
    private RecyclerView rvTransportations;
    private SwipeRefreshLayout srRefresh;
    private Toolbar toolbar;

    private RecyclerView.LayoutManager layoutManager;
    private AlertDialog loadingDialog;
    private AlertDialog.Builder builderDialog;

    private MainTransportationsPresenter presenter;
    private PreferencesHelper preferencesHelper;
    private RecyclerTransportationsAdapter adapter;

    //
    //
    // BASE
    //
    //


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_transportations);
        getExtras();
        getSession();
        initPresenter();
        bindUI();
        initUI();
        initEvents();
        srRefresh.setRefreshing(true);
        presenter.getTransportations();
    }

    @Override
    public void getExtras() {

    }

    @Override
    public void getSession() {
        preferencesHelper = new PreferencesHelper(MainTransportationsActivity.this);
    }

    @Override
    public void initPresenter() {
        presenter = new MainTransportationsPresenterImpl(MainTransportationsActivity.this);
    }

    @Override
    public void bindUI() {
        toolbar = findViewById(R.id.toolbar_tansportation);
        rlNotOrders = findViewById(R.id.rl_noOrders);
        rlOrders = findViewById(R.id.rl_orders);
        rvTransportations = findViewById(R.id.rv_transportations);
        srRefresh = findViewById(R.id.sr_refresh);
    }

    @Override
    public void initUI() {
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_baseline_event_note_24);
        layoutManager = new LinearLayoutManager(MainTransportationsActivity.this);
        rvTransportations.setLayoutManager(layoutManager);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(rvTransportations.getContext(), DividerItemDecoration.VERTICAL);
        rvTransportations.addItemDecoration(dividerItemDecoration);
        srRefresh.setColorSchemeColors(getColor(R.color.colorPrimary));
        updateTransportations();
    }

    @Override
    public void initEvents() {
        srRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                presenter.getTransportations();
            }
        });
    }

    //
    //
    // REDIRECCIONAMIENTOS
    //
    //

    private void goToEnabled() {
        Intent enabled = new Intent(MainTransportationsActivity.this, EnabledActivity.class);
        enabled.setAction(Intent.ACTION_MAIN);
        startActivity(enabled);
        finish();
    }

    private void goToDevices() {
        Intent devices = new Intent(MainTransportationsActivity.this, ConfigDevicesActivity.class);
        startActivity(devices);
    }

    //
    //
    // RESULT ACTIVITY
    //
    //

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == RESULT_ACTIVITY_PARTNER){
            updateTransportations();
        }
    }

    //
    //
    // DIALOGOS
    //
    //

    @Override
    public void hideDialog() {
        if(loadingDialog != null){
            loadingDialog.dismiss();
        }
    }

    @Override
    public void showDialogDecision(final MainTransportationsPresenter.Dialog dialog, String message, String labelAceptar, String labelRechazar) {
        hideDialog();
        builderDialog = new AlertDialog.Builder(MainTransportationsActivity.this);
        View viewLoading = getLayoutInflater().inflate(R.layout.dialog_decision,null,false);
        TextView tvMessage = viewLoading.findViewById(R.id.tv_message);
        Button aceptar = viewLoading.findViewById(R.id.btn_aceptar);
        Button rechazar = viewLoading.findViewById(R.id.btn_rechazar);
        aceptar.setText(labelAceptar);
        rechazar.setText(labelRechazar);
        tvMessage.setText(Html.fromHtml(message));
        aceptar.setOnClickListener(v -> dialog.aceptarDialog());
        rechazar.setOnClickListener(v -> dialog.rechazarDialog());
        builderDialog.setView(viewLoading);
        loadingDialog = builderDialog.create();
        loadingDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        loadingDialog.setCancelable(false);
        loadingDialog.show();
    }

    @Override
    public void showDialogDecision(final MainTransportationsPresenter.Dialog dialog, String message) {
        hideDialog();
        builderDialog = new AlertDialog.Builder(MainTransportationsActivity.this);
        View viewLoading = getLayoutInflater().inflate(R.layout.dialog_decision,null,false);
        TextView tvMessage = viewLoading.findViewById(R.id.tv_message);
        Button aceptar = viewLoading.findViewById(R.id.btn_aceptar);
        Button rechazar = viewLoading.findViewById(R.id.btn_rechazar);
        tvMessage.setText(Html.fromHtml(message));
        aceptar.setOnClickListener(v -> dialog.aceptarDialog());
        rechazar.setOnClickListener(v -> dialog.rechazarDialog());
        builderDialog.setView(viewLoading);
        loadingDialog = builderDialog.create();
        loadingDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        loadingDialog.setCancelable(false);
        loadingDialog.show();
    }

    @Override
    public void showAlert(String message) {
        hideDialog();
        builderDialog = new AlertDialog.Builder(MainTransportationsActivity.this);
        View viewLoading = getLayoutInflater().inflate(R.layout.dialog_alert,null,false);
        TextView tvMessage = viewLoading.findViewById(R.id.tv_message);
        Button aceptar = viewLoading.findViewById(R.id.btn_aceptar);
        tvMessage.setText(Html.fromHtml(message));
        aceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideDialog();
            }
        });
        builderDialog.setView(viewLoading);
        loadingDialog = builderDialog.create();
        loadingDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        loadingDialog.setCancelable(false);
        loadingDialog.show();
    }

    @Override
    public void showAlert(String message, final MainTransportationsPresenter.SimpleDialog dialog) {
        hideDialog();
        builderDialog = new AlertDialog.Builder(MainTransportationsActivity.this);
        View viewLoading = getLayoutInflater().inflate(R.layout.dialog_alert,null,false);
        TextView tvMessage = viewLoading.findViewById(R.id.tv_message);
        Button aceptar = viewLoading.findViewById(R.id.btn_aceptar);
        tvMessage.setText(Html.fromHtml(message));
        aceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.aceptarDialog();
            }
        });
        builderDialog.setView(viewLoading);
        loadingDialog = builderDialog.create();
        loadingDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        loadingDialog.setCancelable(false);
        loadingDialog.show();
    }

    @Override
    public void showLoading(String message) {
        hideDialog();
        builderDialog = new AlertDialog.Builder(MainTransportationsActivity.this);
        View viewLoading = getLayoutInflater().inflate(R.layout.dialog_loading,null,false);
        TextView tvMessage = viewLoading.findViewById(R.id.tv_message);
        tvMessage.setText(message);
        builderDialog.setView(viewLoading);
        loadingDialog = builderDialog.create();
        loadingDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        loadingDialog.setCancelable(false);
        loadingDialog.show();
    }

    //
    //
    // METODOS AUXILIARES
    //
    //

    @Override
    public void showNotTransportations() {
        srRefresh.setRefreshing(false);
        rlNotOrders.setVisibility(View.VISIBLE);
        rlOrders.setVisibility(View.GONE);
    }

    @Override
    public void updateTransportations() {
        if(preferencesHelper.getSessionVM().getTransportes().size() > 0){
            rlNotOrders.setVisibility(View.GONE);
            rlOrders.setVisibility(View.VISIBLE);
            adapter = new RecyclerTransportationsAdapter(MainTransportationsActivity.this);
            rvTransportations.setAdapter(adapter);
        }else{
            rlNotOrders.setVisibility(View.VISIBLE);
            rlOrders.setVisibility(View.GONE);
        }
    }

    @Override
    public void closeSession() {
        goToEnabled();
    }

    @Override
    public void hideRefresh() {
        srRefresh.setRefreshing(false);
    }

    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        int keycode = event.getKeyCode();
        if(keycode == KeyEvent.KEYCODE_BACK){
            return super.dispatchKeyEvent(event);
        }
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.devices:
                goToDevices();
                return true;
            case R.id.close:
                showDialogDecision(new MainTransportationsPresenter.Dialog() {
                    @Override
                    public void aceptarDialog() {
                        presenter.logOut();
                    }
                    @Override
                    public void rechazarDialog() {
                        hideDialog();
                    }
                },"<b> " + getString(R.string.label_cerrar_sesion) + " </b><br><br> " + getString(R.string.text_cerrar_sesion),"Sí","No");
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }
}
