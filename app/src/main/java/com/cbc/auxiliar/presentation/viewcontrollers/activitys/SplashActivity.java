package com.cbc.auxiliar.presentation.viewcontrollers.activitys;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.cbc.auxiliar.R;

/**
 * -----------------------------------------------------------------------------------------------------------
 * |                                               CBC AUXILIAR                                              |
 * -----------------------------------------------------------------------------------------------------------
 *
 * FECHA           AUTOR               J.PROYECTO              PROYECTO                DESCRIPCION
 * -----------------------------------------------------------------------------------------------------------
 * 04/10/2020       Joel Cauti          Alexander Olivares      CBC Picking             Creacion Activity
 */

public class SplashActivity extends AppCompatActivity{

    private ImageView logoCbc;
    private TextView firma;
    private ImageView logoDo;

    //
    //
    // BASE
    //
    //

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        logoCbc = findViewById(R.id.logo_cbc);
        firma = findViewById(R.id.texto);
        logoDo = findViewById(R.id.logo_do);
        logoCbc.startAnimation(AnimationUtils.loadAnimation(SplashActivity.this,R.anim.animacion1));
        firma.startAnimation(AnimationUtils.loadAnimation(SplashActivity.this,R.anim.animacion2));
        logoDo.startAnimation(AnimationUtils.loadAnimation(SplashActivity.this,R.anim.animacion3));
        startHandlerPresenter();
    }

    //
    //
    // TIMER
    //
    //

    private void startHandlerPresenter() {
        Handler handler = new Handler();
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(SplashActivity.this,EnabledActivity.class);
                intent.setAction(Intent.ACTION_MAIN);
                startActivity(intent);
                finish();
            }
        };
        handler.postDelayed(runnable,2700);
    }
}
