package com.cbc.auxiliar.presentation.viewcontrollers.activitys;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Html;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.cbc.auxiliar.R;
import com.cbc.auxiliar.domain.helpers.PreferencesHelper;
import com.cbc.auxiliar.domain.presenters.MainCustomersPresenter;
import com.cbc.auxiliar.domain.presenters.MainCustomersPresenterImpl;
import com.cbc.auxiliar.presentation.viewadapters.RecyclerCustomersAdapter;

import static com.cbc.auxiliar.presentation.viewutils.ConstantsUtils.EXTRA_CUSTOMER_IDPARTNER;
import static com.cbc.auxiliar.presentation.viewutils.ConstantsUtils.EXTRA_CUSTOMER_IDTRANSPORTATION;
import static com.cbc.auxiliar.presentation.viewutils.ConstantsUtils.EXTRA_CUSTOMER_NAMEPARTNER;
import static com.cbc.auxiliar.presentation.viewutils.ConstantsUtils.RESULT_ACTIVITY_ARTICLE;

/**
 * -----------------------------------------------------------------------------------------------------------
 * |                                               CBC AUXILIAR                                              |
 * -----------------------------------------------------------------------------------------------------------
 *
 * FECHA           AUTOR               J.PROYECTO              PROYECTO                DESCRIPCION
 * -----------------------------------------------------------------------------------------------------------
 * 04/10/2020       Joel Cauti          Alexander Olivares      CBC Picking             Creacion Activity
 */

public class MainCustomersActivity extends AppCompatActivity implements MainCustomersPresenter.View {

    private RelativeLayout rlcustomers;
    private RecyclerView rvCustomers;
    private SwipeRefreshLayout srRefresh;
    private Toolbar toolbar;

    private RecyclerView.LayoutManager layoutManager;
    private AlertDialog loadingDialog;
    private AlertDialog.Builder builderDialog;

    private MainCustomersPresenter presenter;
    private PreferencesHelper preferencesHelper;
    private RecyclerCustomersAdapter adapter;

    private String idTransportation     = "";
    private String idPartner            = "";
    private String namePartner          = "";

    //
    //
    // BASE
    //
    //

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_customers);
        getExtras();
        getSession();
        initPresenter();
        bindUI();
        initUI();
        initEvents();
        updateCustomers();
    }

    @Override
    public void getExtras() {
        Bundle extras = getIntent().getExtras();
        if(extras != null){
            idTransportation = extras.getString(EXTRA_CUSTOMER_IDTRANSPORTATION);
            idPartner = extras.getString(EXTRA_CUSTOMER_IDPARTNER);
            namePartner = extras.getString(EXTRA_CUSTOMER_NAMEPARTNER);
        }
    }

    @Override
    public void getSession() {
        preferencesHelper = new PreferencesHelper(MainCustomersActivity.this);
    }

    @Override
    public void initPresenter() {
        presenter = new MainCustomersPresenterImpl(MainCustomersActivity.this, idTransportation, idPartner);
    }

    @Override
    public void bindUI() {
        toolbar = findViewById(R.id.toolbar_customers);
        rlcustomers = findViewById(R.id.rl_customers);
        rvCustomers = findViewById(R.id.rv_customers);
        srRefresh = findViewById(R.id.sr_refresh);
    }

    @Override
    public void initUI() {
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_baseline_keyboard_arrow_left_24);
        toolbar.setTitle("Familias > " + namePartner);
        layoutManager = new LinearLayoutManager(MainCustomersActivity.this);
        rvCustomers.setLayoutManager(layoutManager);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(rvCustomers.getContext(), DividerItemDecoration.VERTICAL);
        rvCustomers.addItemDecoration(dividerItemDecoration);
        srRefresh.setColorSchemeColors(getColor(R.color.colorPrimary));
        updateCustomers();
    }

    @Override
    public void initEvents() {
        srRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                presenter.getCustomers();
            }
        });
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    //
    //
    // REDIRECCIONAMIENTOS
    //
    //

    private void goToEnabled() {
        Intent enabled = new Intent(MainCustomersActivity.this, EnabledActivity.class);
        enabled.setAction(Intent.ACTION_MAIN);
        startActivity(enabled);
        finish();
    }

    private void goToDevices() {
        Intent devices = new Intent(MainCustomersActivity.this, ConfigDevicesActivity.class);
        startActivity(devices);
    }

    //
    //
    // RESPUESTA DE ACTIVITYS
    //
    //

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == RESULT_ACTIVITY_ARTICLE) {
            adapter = new RecyclerCustomersAdapter(MainCustomersActivity.this, idTransportation, idPartner, namePartner);
            rvCustomers.setAdapter(adapter);
        }
    }

    //
    //
    // DIALOGOS
    //
    //

    @Override
    public void showLoading() {
        hideDialog();
        builderDialog = new AlertDialog.Builder(MainCustomersActivity.this);
        View viewLoading = getLayoutInflater().inflate(R.layout.dialog_loading,null,false);
        builderDialog.setView(viewLoading);
        loadingDialog = builderDialog.create();
        loadingDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        loadingDialog.setCancelable(false);
        loadingDialog.show();
    }

    @Override
    public void showLoading(String message) {
        hideDialog();
        builderDialog = new AlertDialog.Builder(MainCustomersActivity.this);
        View viewLoading = getLayoutInflater().inflate(R.layout.dialog_loading,null,false);
        TextView tvMessage = viewLoading.findViewById(R.id.tv_message);
        tvMessage.setText(Html.fromHtml(message));
        builderDialog.setView(viewLoading);
        loadingDialog = builderDialog.create();
        loadingDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        loadingDialog.setCancelable(false);
        loadingDialog.show();
    }

    @Override
    public void hideDialog() {
        if(loadingDialog != null){
            loadingDialog.dismiss();
        }
    }

    @Override
    public void showAlert(String message) {
        hideDialog();
        builderDialog = new AlertDialog.Builder(MainCustomersActivity.this);
        View viewLoading = getLayoutInflater().inflate(R.layout.dialog_alert,null,false);
        TextView tvMessage = viewLoading.findViewById(R.id.tv_message);
        Button aceptar = viewLoading.findViewById(R.id.btn_aceptar);
        tvMessage.setText(Html.fromHtml(message));
        aceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideDialog();
            }
        });
        builderDialog.setView(viewLoading);
        loadingDialog = builderDialog.create();
        loadingDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        loadingDialog.setCancelable(false);
        loadingDialog.show();
    }

    @Override
    public void showDialogDecision(final MainCustomersPresenter.Dialog dialog, String message, String labelAceptar, String labelRechazar) {
        hideDialog();
        builderDialog = new AlertDialog.Builder(MainCustomersActivity.this);
        View viewLoading = getLayoutInflater().inflate(R.layout.dialog_decision,null,false);
        TextView tvMessage = viewLoading.findViewById(R.id.tv_message);
        Button aceptar = viewLoading.findViewById(R.id.btn_aceptar);
        Button rechazar = viewLoading.findViewById(R.id.btn_rechazar);
        aceptar.setText(labelAceptar);
        rechazar.setText(labelRechazar);
        tvMessage.setText(Html.fromHtml(message));
        aceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.aceptarDialog();
            }
        });
        rechazar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.rechazarDialog();
            }
        });
        builderDialog.setView(viewLoading);
        loadingDialog = builderDialog.create();
        loadingDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        loadingDialog.setCancelable(false);
        loadingDialog.show();
    }

    @Override
    public void showDialogDecision(final MainCustomersPresenter.Dialog dialog, String message) {
        hideDialog();
        builderDialog = new AlertDialog.Builder(MainCustomersActivity.this);
        View viewLoading = getLayoutInflater().inflate(R.layout.dialog_decision,null,false);
        TextView tvMessage = viewLoading.findViewById(R.id.tv_message);
        Button aceptar = viewLoading.findViewById(R.id.btn_aceptar);
        Button rechazar = viewLoading.findViewById(R.id.btn_rechazar);
        tvMessage.setText(Html.fromHtml(message));
        aceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.aceptarDialog();
            }
        });
        rechazar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.rechazarDialog();
            }
        });
        builderDialog.setView(viewLoading);
        loadingDialog = builderDialog.create();
        loadingDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        loadingDialog.setCancelable(false);
        loadingDialog.show();
    }

    //
    //
    // METODOS AUXILIARES
    //
    //

    @Override
    public void updateCustomers() {
        adapter = new RecyclerCustomersAdapter(MainCustomersActivity.this, idTransportation, idPartner, namePartner);
        rvCustomers.setAdapter(adapter);
    }

    @Override
    public void closeSession() {
        goToEnabled();
    }

    @Override
    public void hideRefresh() {
        srRefresh.setRefreshing(false);
    }

    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        int keycode = event.getKeyCode();
        if(keycode == KeyEvent.KEYCODE_BACK){
            return super.dispatchKeyEvent(event);
        }
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.devices:
                goToDevices();
                return true;
            case R.id.close:
                showDialogDecision(new MainCustomersPresenter.Dialog() {
                    @Override
                    public void aceptarDialog() {
                        presenter.logOut();
                    }
                    @Override
                    public void rechazarDialog() {
                        hideDialog();
                    }
                },"<b> " + getString(R.string.label_cerrar_sesion) + " </b><br><br> " + getString(R.string.text_cerrar_sesion),"Sí","No");
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }
}