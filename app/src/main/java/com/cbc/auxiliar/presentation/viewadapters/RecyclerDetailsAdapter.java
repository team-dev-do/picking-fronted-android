package com.cbc.auxiliar.presentation.viewadapters;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.cbc.auxiliar.R;
import com.cbc.auxiliar.domain.helpers.PreferencesHelper;
import com.cbc.auxiliar.domain.presenters.MainArticlesPresenter;
import com.cbc.auxiliar.presentation.viewcontrollers.activitys.MainItemActivity;
import com.cbc.auxiliar.presentation.viewmodels.ArticleViewModel;

import java.util.List;

import static com.cbc.auxiliar.presentation.viewutils.ConstantsUtils.RESULT_ACTIVITY_ITEM;

/**
 * -----------------------------------------------------------------------------------------------------------
 * |                                               CBC AUXILIAR                                              |
 * -----------------------------------------------------------------------------------------------------------
 *
 * FECHA           AUTOR               J.PROYECTO              PROYECTO                DESCRIPCION
 * -----------------------------------------------------------------------------------------------------------
 * 04/10/2020       Joel Cauti          Alexander Olivares      CBC Picking             Creacion Adapter
 */

public class RecyclerDetailsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    private final Context context;
    private final MainArticlesPresenter.View viewActivity;
    private final Activity activity;
    private final PreferencesHelper preferencesHelper;

    private List<ArticleViewModel> products;
    private String title;
    private LayoutInflater inflater;

    public RecyclerDetailsAdapter(List<ArticleViewModel> products, String title, Context context) {
        this.products = products;
        this.title = title;
        this.context = context;
        this.activity = (Activity) context;
        this.preferencesHelper = new PreferencesHelper(context);
        this.viewActivity = (MainArticlesPresenter.View) context;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v;
        v = inflater.inflate(R.layout.item_article, null, false);
        return new ViewHolderOrder(v);
    }

    @SuppressLint({"UseCompatLoadingForDrawables", "SetTextI18n"})
    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if(holder instanceof RecyclerDetailsAdapter.ViewHolderOrder){
            RecyclerDetailsAdapter.ViewHolderOrder viewHolderOrder = (RecyclerDetailsAdapter.ViewHolderOrder) holder;
            final ArticleViewModel product = products.get(position);
            if(product.getStatusProgress() == 0){
                viewHolderOrder.imIcon.setImageDrawable(context.getDrawable(R.drawable.ic_baseline_check_24));
            }else if(product.getStatusProgress() == 1){
                viewHolderOrder.imIcon.setImageDrawable(context.getDrawable(R.drawable.ic_baseline_check_alert));
            }else if(product.getStatusProgress() == 2){
                viewHolderOrder.imIcon.setImageDrawable(context.getDrawable(R.drawable.ic_baseline_check_24_green));
            }
            viewHolderOrder.tvNombre.setText(product.getDescription());
            viewHolderOrder.tvDescripcion.setText("Pallet: "+product.getPalet() + " | Tipo: " + (product.getUm().equals("CJ")? "Caja" : "Unidad"));
            viewHolderOrder.tvBa.setText(product.getBa());
            viewHolderOrder.tvCantidad.setText("CANTIDAD: " + product.getAmountFound() + "/" + product.getAmountRequested());
            if(product.getStatusProgress() == 2){
                viewHolderOrder.rlItem.setBackgroundColor(context.getColor(R.color.CBC_backgroundSelected));
            }else{
                viewHolderOrder.rlItem.setBackgroundColor(context.getColor(R.color.CBC_transparent));
            }
            if(preferencesHelper.getSessionVM().getDevice().isEnabled()) {
                viewHolderOrder.rlItem.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (product.getStatusProgress() < 2) {
                            Intent item = new Intent(activity, MainItemActivity.class);
                            item.putExtra("EXTRA_TITLE_PLANILLA", title);
                            item.putExtra("EXTRA_PRODUCT", new Gson().toJson(product));
                            activity.startActivityForResult(item, RESULT_ACTIVITY_ITEM);
                        } else {
                            viewActivity.showAlert("Este producto ha sido completado, continua con otros productos", 0);
                        }
                    }
                });
            }
            viewHolderOrder.tvImagen.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(product.getImage().equals("")){
                        viewActivity.showAlert("Este producto no cuenta con una imagen para ver.",0);
                    }else{
                        viewActivity.showImage(product.getImage());
                    }
                }
            });
        }
    }

    @Override
    public int getItemViewType(int position) {
        return products.get(position).getTypeView();
    }

    @Override
    public int getItemCount() {
        return products.size();
    }

    //
    //
    // VIEW HOLDER
    //
    //

    public static class ViewHolderOrder extends RecyclerView.ViewHolder{

        private final ImageView imIcon;
        private final RelativeLayout rlItem;
        private final TextView tvNombre;
        private final TextView tvDescripcion;
        private final TextView tvBa;
        private final TextView tvCantidad;
        private final TextView tvImagen;

        public ViewHolderOrder(@NonNull View itemView) {
            super(itemView);
            imIcon = itemView.findViewById(R.id.iv_icon);
            rlItem = itemView.findViewById(R.id.rl_item);
            tvNombre = itemView.findViewById(R.id.tv_nombre);
            tvDescripcion = itemView.findViewById(R.id.tv_descripcion);
            tvBa = itemView.findViewById(R.id.tv_ba);
            tvCantidad = itemView.findViewById(R.id.tv_cantidad);
            tvImagen = itemView.findViewById(R.id.tv_ver_imagen);
        }
    }
}
