package com.cbc.auxiliar.presentation.viewcontrollers.base;

/**
 * -----------------------------------------------------------------------------------------------------------
 * |                                               CBC AUXILIAR                                              |
 * -----------------------------------------------------------------------------------------------------------
 *
 * FECHA           AUTOR               J.PROYECTO              PROYECTO                DESCRIPCION
 * -----------------------------------------------------------------------------------------------------------
 * 04/10/2020       Joel Cauti          Alexander Olivares      CBC Picking             Creacion Base View Activity
 */

public interface BaseActivityView {

    void getExtras();
    void getSession();
    void initPresenter();
    void bindUI();
    void initUI();
    void initEvents();

}
