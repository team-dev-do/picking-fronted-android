package com.cbc.auxiliar.presentation.viewmodels;

import java.util.List;

/**
 * -----------------------------------------------------------------------------------------------------------
 * |                                               CBC AUXILIAR                                              |
 * -----------------------------------------------------------------------------------------------------------
 *
 * FECHA           AUTOR               J.PROYECTO              PROYECTO                DESCRIPCION
 * ------------------------------------------------------------------------------------------------------------
 * 09/12/2020       Joel Cauti          Alexander Olivares      CBC Picking             Creacion View Model
 */

public class TransportationViewModel {

    private String id;
    private String name;
    private List<PartnerViewModel> partners;

    private Integer typeView;
    private boolean isSelected;
    private Integer partnersComplete;
    private String progressStatus;

    public TransportationViewModel() {
    }

    public TransportationViewModel(String id, String name, List<PartnerViewModel> partners, Integer typeView, boolean isSelected, Integer partnersComplete, String progressStatus) {
        this.id = id;
        this.name = name;
        this.partners = partners;
        this.typeView = typeView;
        this.isSelected = isSelected;
        this.partnersComplete = partnersComplete;
        this.progressStatus = progressStatus;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<PartnerViewModel> getPartners() {
        return partners;
    }

    public void setPartners(List<PartnerViewModel> partners) {
        this.partners = partners;
    }

    public Integer getTypeView() {
        return typeView;
    }

    public void setTypeView(Integer typeView) {
        this.typeView = typeView;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public Integer getPartnersComplete() {
        return partnersComplete;
    }

    public void setPartnersComplete(Integer partnersComplete) {
        this.partnersComplete = partnersComplete;
    }

    public String getProgressStatus() {
        return progressStatus;
    }

    public void setProgressStatus(String progressStatus) {
        this.progressStatus = progressStatus;
    }

    public enum ProgressStatus{
        PENDIENTE("Por hacer"),
        PROCESO("En proceso"),
        TERMINADO("Completado");

        private String progressStatus;

        ProgressStatus(String progressStatus) {
            this.progressStatus = progressStatus;
        }

        public String getProgressStatus() {
            return progressStatus;
        }
    }
}
