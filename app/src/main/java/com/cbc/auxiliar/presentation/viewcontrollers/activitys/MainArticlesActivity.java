package com.cbc.auxiliar.presentation.viewcontrollers.activitys;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Html;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.cbc.auxiliar.R;
import com.cbc.auxiliar.domain.helpers.PreferencesHelper;
import com.cbc.auxiliar.domain.presenters.MainArticlesPresenter;
import com.cbc.auxiliar.domain.presenters.MainArticlesPresenterImpl;
import com.cbc.auxiliar.presentation.viewadapters.RecyclerDetailsAdapter;
import com.cbc.auxiliar.presentation.viewmodels.CustomerViewModel;
import com.cbc.auxiliar.presentation.viewmodels.ArticleViewModel;
import com.squareup.picasso.Picasso;

import java.util.List;

import static com.cbc.auxiliar.domain.helpers.KeyCodesHelper.convertirKeycode;
import static com.cbc.auxiliar.domain.helpers.LogHelper.printLog;
import static com.cbc.auxiliar.presentation.viewmodels.CustomerViewModel.ProgressStatus.PENDIENTE;
import static com.cbc.auxiliar.presentation.viewmodels.CustomerViewModel.ProgressStatus.PROCESO;
import static com.cbc.auxiliar.presentation.viewutils.ConstantsUtils.EXTRA_CUSTOMER_IDPARTNER;
import static com.cbc.auxiliar.presentation.viewutils.ConstantsUtils.EXTRA_CUSTOMER_IDTRANSPORTATION;
import static com.cbc.auxiliar.presentation.viewutils.ConstantsUtils.EXTRA_CUSTOMER_NAMEPARTNER;
import static com.cbc.auxiliar.presentation.viewutils.ConstantsUtils.RESULT_ACTIVITY_ITEM;
import static com.cbc.auxiliar.presentation.viewutils.ConstantsUtils.RESULT_ACTIVITY_DEVICES;

/**
 * -----------------------------------------------------------------------------------------------------------
 * |                                               CBC AUXILIAR                                              |
 * -----------------------------------------------------------------------------------------------------------
 *
 * FECHA           AUTOR               J.PROYECTO              PROYECTO                DESCRIPCION
 * -----------------------------------------------------------------------------------------------------------
 * 04/10/2020       Joel Cauti          Alexander Olivares      CBC Picking             Creacion Activity
 */

public class MainArticlesActivity extends AppCompatActivity implements MainArticlesPresenter.View {

    private RecyclerView rvDetail;
    private Button btnFinalizar, btnScan;
    private Toolbar toolbar;

    private RecyclerView.LayoutManager layoutManager;
    private PreferencesHelper preferencesHelper;
    private CustomerViewModel customer;
    private RecyclerDetailsAdapter adapter;
    private MainArticlesPresenter presenter;
    private AlertDialog loadingDialog;
    private AlertDialog.Builder builderDialog;
    private String barCode = "";

    private String idTransportation;
    private String idPartner;
    private String namePartner;

    //
    //
    // BASE
    //
    //

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_articles);
        getExtras();
        getSession();
        initPresenter();
        bindUI();
        initUI();
        initEvents();
    }

    @Override
    public void getExtras() {
        Bundle extras = getIntent().getExtras();
        if(extras != null){
            idTransportation = extras.getString(EXTRA_CUSTOMER_IDTRANSPORTATION);
            idPartner = extras.getString(EXTRA_CUSTOMER_IDPARTNER);
            namePartner = extras.getString(EXTRA_CUSTOMER_NAMEPARTNER);
        }
    }

    @Override
    public void getSession() {
        preferencesHelper = new PreferencesHelper(MainArticlesActivity.this);
        List<CustomerViewModel> customers = preferencesHelper.getCustomersSelected(idTransportation, idPartner);
        for(CustomerViewModel itemOrder: customers){
            if(itemOrder.isSelected()){
                customer = itemOrder;
            }
        }
    }

    @Override
    public void initPresenter() {
        presenter = new MainArticlesPresenterImpl(MainArticlesActivity.this, idTransportation, idPartner);
    }

    @Override
    public void bindUI() {
        toolbar = findViewById(R.id.toolbar_articles);
        rvDetail = findViewById(R.id.rv_detail);
        btnFinalizar = findViewById(R.id.bt_finalizar);
        btnScan = findViewById(R.id.bt_scan);
    }

    @SuppressLint("UseCompatLoadingForDrawables")
    @Override
    public void initUI() {
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_baseline_keyboard_arrow_left_24);
        toolbar.setTitle("Clientes > " + customer.getName());
        layoutManager = new LinearLayoutManager(MainArticlesActivity.this);
        rvDetail.setLayoutManager(layoutManager);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(rvDetail.getContext(), DividerItemDecoration.VERTICAL);
        rvDetail.addItemDecoration(dividerItemDecoration);
        if(customer.getProgressStatus().equals(PENDIENTE.getProgressStatus())){
            btnFinalizar.setEnabled(false);
            btnFinalizar.setBackground(getDrawable(R.drawable.shape_btn_main_normal_disabled));
        }else{
            btnFinalizar.setEnabled(true);
            btnFinalizar.setBackground(getDrawable(R.drawable.shape_btn_main_normal));
        }
        updateRecycler(customer.getArticles(), customer.getName());
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public void initEvents() {
        btnFinalizar.setOnClickListener(v -> {
            int flagIncompletos = 0;
            boolean isWarning = false;
            String message, laceptar, lrechazar;
            for(ArticleViewModel producto : customer.getArticles()){
                if(producto.getStatusProgress() < 2){
                    flagIncompletos++;
                }
            }
            if(flagIncompletos > 0){
                message = customer.getName() + "<br><b>incompleto</b>";
                laceptar = "Finalizar planilla";
                lrechazar = "Revisar nuevamente";
                isWarning = true;
            }else{
                message = "¿Estas seguro de finalizar la planilla?";
                laceptar = "Finalizar planilla";
                lrechazar = "Revisar nuevamente";
                isWarning = false;
            }
            showDialogDecision(new MainArticlesPresenter.Dialog() {
                @Override
                public void aceptarDialog() {
                    presenter.finalizarPedido(customer);
                }
                @Override
                public void rechazarDialog() {
                    hideDialog();
                }
            } ,message, laceptar, lrechazar, isWarning);
        });
        toolbar.setNavigationOnClickListener(v -> onBackPressed());
    }

    //
    //
    // RESPUESTAS DE ACTIVITYS
    //
    //

    @SuppressLint("UseCompatLoadingForDrawables")
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == RESULT_ACTIVITY_ITEM){
            if(resultCode == RESULT_OK){
                Bundle extras = data.getExtras();
                if(extras != null){
                    ArticleViewModel product = new Gson().fromJson(extras.getString("EXTRA_PRODUCT_RESULT"), ArticleViewModel.class);
                    presenter.setTerminar(product.getId());
                    int index1 = -1;
                    for(int i = 0; i < customer.getArticles().size(); i++){
                        if(customer.getArticles().get(i).getId().equals(product.getId())){
                            index1 = i;
                        }
                    }
                    if (index1 > -1) {
                        customer.getArticles().remove(index1);
                        customer.getArticles().add(index1,product);
                    }
                    List<CustomerViewModel> orders = preferencesHelper.getCustomersSelected(idTransportation, idPartner);
                    int index2 = -1;
                    for(int j = 0; j < orders.size(); j++){
                        if(orders.get(j).getId().equals(customer.getId())){
                            index2 = j;
                        }
                    }

                    int flagStatus = 0;
                    for(int h = 0; h < customer.getArticles().size(); h++){
                        if(customer.getArticles().get(h).getStatusProgress() == 1 || customer.getArticles().get(h).getStatusProgress() == 2){
                            flagStatus++;
                        }
                    }
                    customer.setArticlesComplete(flagStatus);
                    if(flagStatus == 0){
                        btnFinalizar.setEnabled(false);
                        btnFinalizar.setBackground(getDrawable(R.drawable.shape_btn_main_normal_disabled));
                        customer.setProgressStatus(PENDIENTE.getProgressStatus());
                    }else if(flagStatus > 0 && (customer.getProgressStatus().equals(PENDIENTE.getProgressStatus()) || customer.getProgressStatus().equals(PROCESO.getProgressStatus()))) {
                        btnFinalizar.setEnabled(true);
                        btnFinalizar.setBackground(getDrawable(R.drawable.shape_btn_main_normal));
                        customer.setProgressStatus(PROCESO.getProgressStatus());
                    }
                    if(index2 > -1){
                        orders.remove(index2);
                        orders.add(index2, customer);
                    }
                    preferencesHelper.updateCustomers(idTransportation, idPartner, orders);
                    updateRecycler(customer.getArticles(), customer.getName());
                }
            }
            updateRecycler(customer.getArticles(), customer.getName());
        }else if(requestCode == RESULT_ACTIVITY_DEVICES){
            //updateRecycler(customer.getArticles(), customer.getName());
        }
    }

    //
    //
    // DIALOGOS
    //
    //

    @Override
    public void showLoading() {
        hideDialog();
        builderDialog = new AlertDialog.Builder(MainArticlesActivity.this);
        View viewLoading = getLayoutInflater().inflate(R.layout.dialog_loading,null,false);
        builderDialog.setView(viewLoading);
        loadingDialog = builderDialog.create();
        loadingDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        loadingDialog.setCancelable(false);
        loadingDialog.show();
    }

    @Override
    public void showLoading(String message) {
        hideDialog();
        builderDialog = new AlertDialog.Builder(MainArticlesActivity.this);
        View viewLoading = getLayoutInflater().inflate(R.layout.dialog_loading,null,false);
        TextView tvMessage = viewLoading.findViewById(R.id.tv_message);
        tvMessage.setText(Html.fromHtml(message));
        builderDialog.setView(viewLoading);
        loadingDialog = builderDialog.create();
        loadingDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        loadingDialog.setCancelable(false);
        loadingDialog.show();
    }

    @Override
    public void hideDialog() {
        if(loadingDialog != null){
            loadingDialog.dismiss();
        }
    }

    @Override
    public void showAlert(String message, int typeAlert) {
        hideDialog();
        builderDialog = new AlertDialog.Builder(MainArticlesActivity.this);
        View viewLoading = getLayoutInflater().inflate(R.layout.dialog_alert,null,false);
        TextView tvMessage = viewLoading.findViewById(R.id.tv_message);
        Button aceptar = viewLoading.findViewById(R.id.btn_aceptar);
        ImageView iconAlert = viewLoading.findViewById(R.id.icon_alert);
        ImageView iconError = viewLoading.findViewById(R.id.icon_error);
        ImageView iconSuccess = viewLoading.findViewById(R.id.icon_success);
        iconError.setVisibility(View.GONE);
        iconAlert.setVisibility(View.GONE);
        iconSuccess.setVisibility(View.GONE);
        if(typeAlert == 1){
            iconError.setVisibility(View.GONE);
            iconAlert.setVisibility(View.VISIBLE);
            iconSuccess.setVisibility(View.GONE);
        }else if(typeAlert == 2){
            iconError.setVisibility(View.VISIBLE);
            iconAlert.setVisibility(View.GONE);
            iconSuccess.setVisibility(View.GONE);
        }else if(typeAlert == 3){
            iconError.setVisibility(View.GONE);
            iconAlert.setVisibility(View.GONE);
            iconSuccess.setVisibility(View.VISIBLE);
        }
        tvMessage.setText(message);
        aceptar.setOnClickListener(v -> hideDialog());
        builderDialog.setView(viewLoading);
        loadingDialog = builderDialog.create();
        loadingDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        loadingDialog.setCancelable(false);
        loadingDialog.show();
    }

    @Override
    public void showAlert(String message, int typeAlert, final boolean actionBack) {
        hideDialog();
        builderDialog = new AlertDialog.Builder(MainArticlesActivity.this);
        View viewLoading = getLayoutInflater().inflate(R.layout.dialog_alert,null,false);
        TextView tvMessage = viewLoading.findViewById(R.id.tv_message);
        Button aceptar = viewLoading.findViewById(R.id.btn_aceptar);
        ImageView iconAlert = viewLoading.findViewById(R.id.icon_alert);
        ImageView iconError = viewLoading.findViewById(R.id.icon_error);
        ImageView iconSuccess = viewLoading.findViewById(R.id.icon_success);
        iconError.setVisibility(View.GONE);
        iconAlert.setVisibility(View.GONE);
        iconSuccess.setVisibility(View.GONE);
        if(typeAlert == 1){
            iconError.setVisibility(View.GONE);
            iconAlert.setVisibility(View.VISIBLE);
            iconSuccess.setVisibility(View.GONE);
        }else if(typeAlert == 2){
            iconError.setVisibility(View.VISIBLE);
            iconAlert.setVisibility(View.GONE);
            iconSuccess.setVisibility(View.GONE);
        }else if(typeAlert == 3){
            iconError.setVisibility(View.GONE);
            iconAlert.setVisibility(View.GONE);
            iconSuccess.setVisibility(View.VISIBLE);
        }
        tvMessage.setText(message);
        aceptar.setOnClickListener(v -> {
            hideDialog();
            if(actionBack){
                goToPayroll();
            }
        });
        builderDialog.setView(viewLoading);
        loadingDialog = builderDialog.create();
        loadingDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        loadingDialog.setCancelable(false);
        loadingDialog.show();
    }

    @Override
    public void showDialogDecision(final MainArticlesPresenter.Dialog dialog, String message, String labelAceptar, String labelRechazar, boolean isWarning) {
        hideDialog();
        builderDialog = new AlertDialog.Builder(MainArticlesActivity.this);
        View viewLoading = getLayoutInflater().inflate(R.layout.dialog_decision,null,false);
        TextView tvMessage = viewLoading.findViewById(R.id.tv_message);
        ImageView icon = viewLoading.findViewById(R.id.icon_warning);
        Button aceptar = viewLoading.findViewById(R.id.btn_aceptar);
        Button rechazar = viewLoading.findViewById(R.id.btn_rechazar);
        aceptar.setText(labelAceptar);
        rechazar.setText(labelRechazar);
        tvMessage.setText(Html.fromHtml(message));
        icon.setVisibility(isWarning?View.VISIBLE:View.GONE);
        aceptar.setOnClickListener(v -> dialog.aceptarDialog());
        rechazar.setOnClickListener(v -> dialog.rechazarDialog());
        builderDialog.setView(viewLoading);
        loadingDialog = builderDialog.create();
        loadingDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        loadingDialog.setCancelable(false);
        loadingDialog.show();
    }

    @Override
    public void showDialogDecision(final MainArticlesPresenter.Dialog dialog, String message, String labelAceptar, String labelRechazar) {
        hideDialog();
        builderDialog = new AlertDialog.Builder(MainArticlesActivity.this);
        View viewLoading = getLayoutInflater().inflate(R.layout.dialog_decision,null,false);
        TextView tvMessage = viewLoading.findViewById(R.id.tv_message);
        Button aceptar = viewLoading.findViewById(R.id.btn_aceptar);
        Button rechazar = viewLoading.findViewById(R.id.btn_rechazar);
        aceptar.setText(labelAceptar);
        rechazar.setText(labelRechazar);
        tvMessage.setText(Html.fromHtml(message));
        aceptar.setOnClickListener(v -> dialog.aceptarDialog());
        rechazar.setOnClickListener(v -> dialog.rechazarDialog());
        builderDialog.setView(viewLoading);
        loadingDialog = builderDialog.create();
        loadingDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        loadingDialog.setCancelable(false);
        loadingDialog.show();
    }

    @Override
    public void showDialogDecision(final MainArticlesPresenter.Dialog dialog, String message) {
        hideDialog();
        builderDialog = new AlertDialog.Builder(MainArticlesActivity.this);
        View viewLoading = getLayoutInflater().inflate(R.layout.dialog_decision,null,false);
        TextView tvMessage = viewLoading.findViewById(R.id.tv_message);
        Button aceptar = viewLoading.findViewById(R.id.btn_aceptar);
        Button rechazar = viewLoading.findViewById(R.id.btn_rechazar);
        tvMessage.setText(message);
        aceptar.setOnClickListener(v -> dialog.aceptarDialog());
        rechazar.setOnClickListener(v -> dialog.rechazarDialog());
        builderDialog.setView(viewLoading);
        loadingDialog = builderDialog.create();
        loadingDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        loadingDialog.setCancelable(false);
        loadingDialog.show();
    }

    @Override
    public void showImage(String image) {
        hideDialog();
        builderDialog = new AlertDialog.Builder(MainArticlesActivity.this);
        View viewLoading = getLayoutInflater().inflate(R.layout.dialog_imagen,null,false);
        ImageView imageView = viewLoading.findViewById(R.id.card_image);
        Picasso.get().load(image).into(imageView);
        builderDialog.setView(viewLoading);
        loadingDialog = builderDialog.create();
        loadingDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        loadingDialog.setCancelable(true);
        loadingDialog.show();
    }

    //
    //
    // REDIRECCIONAMIENTOS
    //
    //

    @Override
    public void goToPayroll() {
        setResult(RESULT_OK, new Intent());
        finish();
    }

    private void goToDevices() {
        Intent devices = new Intent(MainArticlesActivity.this, ConfigDevicesActivity.class);
        startActivityForResult(devices, RESULT_ACTIVITY_DEVICES);
    }

    private void goToEnabled() {
        Intent enabled = new Intent(MainArticlesActivity.this, EnabledActivity.class);
        enabled.setAction(Intent.ACTION_MAIN);
        startActivity(enabled);
        finish();
    }

    //
    //
    // METODOS AUXILIARES
    //
    //

    @Override
    public void closeSession() {
        goToEnabled();
    }

    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        int keyCode = event.getKeyCode();
        if(event.getAction() == KeyEvent.ACTION_UP){
            if((keyCode <= 54 && keyCode >= 29) || keyCode <= 16 && keyCode >= 7){
                barCode = barCode + convertirKeycode(keyCode);
            }
            if(keyCode == KeyEvent.KEYCODE_TAB || keyCode == KeyEvent.KEYCODE_ENTER){
                lanzarCodigo(barCode);
                barCode = "";
            }
        }
        if(keyCode == KeyEvent.KEYCODE_BACK){
            return super.dispatchKeyEvent(event);
        }
        return true;
    }

    private void lanzarCodigo(String barCode) {
        ArticleViewModel producto = null;
        if(!barCode.equals("")){
            int status = 0;
            for(int i = 0; i < customer.getArticles().size(); i++){
                if(customer.getArticles().get(i).getEan().equals(barCode)){
                    producto = customer.getArticles().get(i);
                    status = customer.getArticles().get(i).getStatusProgress();
                }
            }
            if(producto != null){
                if(status < 2){
                    hideDialog();
                    Intent item = new Intent(MainArticlesActivity.this, MainItemActivity.class);
                    item.putExtra("EXTRA_TITLE_PLANILLA", customer.getName());
                    item.putExtra("EXTRA_PRODUCT", new Gson().toJson(producto));
                    startActivityForResult(item, RESULT_ACTIVITY_ITEM);
                }else{
                    showAlert(producto.getDescription() + ", producto completado",1);
                }
            }else{
                showAlert("Producto incorrecto",2);
            }
        }
    }

    @Override
    public void updateRecycler(List<ArticleViewModel> products, String orderName) {
        adapter = new RecyclerDetailsAdapter(products ,orderName , MainArticlesActivity.this);
        rvDetail.setAdapter(adapter);
    }

    @Override
    public void onBackPressed() {
        hideDialog();
        Intent data = new Intent();
        setResult(RESULT_CANCELED,data);
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.devices:
                goToDevices();
                return true;
            case R.id.close:
                showDialogDecision(new MainArticlesPresenter.Dialog() {
                    @Override
                    public void aceptarDialog() {
                        presenter.logOut();
                    }
                    @Override
                    public void rechazarDialog() {
                        hideDialog();
                    }
                },"<b> " + getString(R.string.label_cerrar_sesion) + " </b><br><br> " + getString(R.string.text_cerrar_sesion),"Sí","No");
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }
}
