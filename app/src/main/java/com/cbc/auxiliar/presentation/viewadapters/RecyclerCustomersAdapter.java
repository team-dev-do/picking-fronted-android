package com.cbc.auxiliar.presentation.viewadapters;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.cbc.auxiliar.R;
import com.cbc.auxiliar.domain.helpers.PreferencesHelper;
import com.cbc.auxiliar.domain.presenters.MainCustomersPresenter;
import com.cbc.auxiliar.presentation.viewcontrollers.activitys.MainArticlesActivity;
import com.cbc.auxiliar.presentation.viewmodels.CustomerViewModel;

import java.util.List;

import static com.cbc.auxiliar.presentation.viewmodels.CustomerViewModel.ProgressStatus.PENDIENTE;
import static com.cbc.auxiliar.presentation.viewmodels.CustomerViewModel.ProgressStatus.PROCESO;
import static com.cbc.auxiliar.presentation.viewmodels.CustomerViewModel.ProgressStatus.TERMINADO;
import static com.cbc.auxiliar.presentation.viewutils.ConstantsUtils.EXTRA_CUSTOMER_IDPARTNER;
import static com.cbc.auxiliar.presentation.viewutils.ConstantsUtils.EXTRA_CUSTOMER_IDTRANSPORTATION;
import static com.cbc.auxiliar.presentation.viewutils.ConstantsUtils.EXTRA_CUSTOMER_NAMEPARTNER;
import static com.cbc.auxiliar.presentation.viewutils.ConstantsUtils.RESULT_ACTIVITY_ARTICLE;

/**
 * -----------------------------------------------------------------------------------------------------------
 * |                                               CBC AUXILIAR                                              |
 * -----------------------------------------------------------------------------------------------------------
 *
 * FECHA           AUTOR               J.PROYECTO              PROYECTO                DESCRIPCION
 * -----------------------------------------------------------------------------------------------------------
 * 04/10/2020       Joel Cauti          Alexander Olivares      CBC Picking             Creacion Adapter
 */

public class RecyclerCustomersAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final Context context;
    private final Activity activity;
    private final MainCustomersPresenter.View view;
    private final PreferencesHelper preferencesHelper;

    private List<CustomerViewModel> customers;
    private LayoutInflater inflater;
    private String idTransportation;
    private String idPartner;
    private String namePartner;

    public RecyclerCustomersAdapter(Context context, String idTransportation, String idPartner, String namePartner) {
        this.context = context;
        this.activity = (Activity) context;
        this.view = (MainCustomersPresenter.View) context;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.idTransportation = idTransportation;
        this.idPartner = idPartner;
        this.namePartner = namePartner;
        this.preferencesHelper = new PreferencesHelper(this.context);
        this.customers = preferencesHelper.getCustomersSelected(idTransportation, idPartner);
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v;
        v = inflater.inflate(R.layout.item_customer, null, false);
        return new ViewHolderOrder(v);
    }

    @SuppressLint({"ResourceAsColor", "SetTextI18n", "UseCompatLoadingForDrawables"})
    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, final int position) {
        if(holder instanceof ViewHolderOrder){
            ViewHolderOrder viewHolderOrder = (ViewHolderOrder) holder;
            final CustomerViewModel customer = customers.get(position);

            viewHolderOrder.tvNameCustomer.setText(customer.getName());
            viewHolderOrder.tvOrdenamiento.setText(String.valueOf(position + 1));
            viewHolderOrder.tvIdCustomer.setText(customer.getId());
            viewHolderOrder.tvContador.setText(customer.getArticlesComplete() + "/" + customer.getArticles().size());
            viewHolderOrder.tvStatus.setText(customer.getProgressStatus());
            viewHolderOrder.rlItem.setBackgroundColor(context.getColor(R.color.CBC_transparent));

            if(customer.getProgressStatus().equals(TERMINADO.getProgressStatus())){
                viewHolderOrder.tvStatus.setTextColor(activity.getColor(R.color.statusProgressTerminado));
                viewHolderOrder.rlItem.setBackgroundColor(context.getColor(R.color.CBC_backgroundSelected));
            }else if(customer.getProgressStatus().equals(PROCESO.getProgressStatus())){
                viewHolderOrder.tvStatus.setTextColor(activity.getColor(R.color.statusProgressProceso));
            }else if(customer.getProgressStatus().equals(PENDIENTE.getProgressStatus())){
                viewHolderOrder.tvStatus.setTextColor(activity.getColor(R.color.statusProgressEspera));
            }

            viewHolderOrder.rlItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(!customers.get(position).getProgressStatus().equals(TERMINADO.getProgressStatus())){
                        for (CustomerViewModel itemOrder : customers){
                            itemOrder.setSelected(false);
                        }
                        customers.get(position).setSelected(true);
                        preferencesHelper.updateCustomers(idTransportation, idPartner, customers);
                        Intent detail = new Intent(activity, MainArticlesActivity.class);
                        detail.putExtra(EXTRA_CUSTOMER_IDTRANSPORTATION,idTransportation);
                        detail.putExtra(EXTRA_CUSTOMER_IDPARTNER,idPartner);
                        detail.putExtra(EXTRA_CUSTOMER_NAMEPARTNER,namePartner);
                        activity.startActivityForResult(detail,RESULT_ACTIVITY_ARTICLE);
                    }else{
                        view.showAlert("Este pedido a sido finalizado, continua con otros pedidos o desliza hacia abajo para actualizar nuevos pedidos");
                    }
                }
            });
        }
    }

    @Override
    public int getItemViewType(int position) {
        return customers.get(position).getTypeView();
    }

    @Override
    public int getItemCount() {
        return customers.size();
    }

    //
    //
    // VIEW HOLDER
    //
    //

    public static class ViewHolderOrder extends RecyclerView.ViewHolder{

        private final RelativeLayout rlItem;
        private final TextView tvNameCustomer;
        private final TextView tvStatus;
        private final TextView tvContador;
        private final TextView tvIdCustomer;
        private final TextView tvOrdenamiento;

        public ViewHolderOrder(@NonNull View itemView) {
            super(itemView);
            rlItem = itemView.findViewById(R.id.rl_item);
            tvNameCustomer = itemView.findViewById(R.id.tv_namecustomer);
            tvStatus = itemView.findViewById(R.id.tv_status);
            tvContador = itemView.findViewById(R.id.tv_contador);
            tvIdCustomer = itemView.findViewById(R.id.tv_idcustomer);
            tvOrdenamiento = itemView.findViewById(R.id.correlativo);
        }
    }
}
