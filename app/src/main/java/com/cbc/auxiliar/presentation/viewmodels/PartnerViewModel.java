package com.cbc.auxiliar.presentation.viewmodels;

import java.util.List;

/**
 * -----------------------------------------------------------------------------------------------------------
 * |                                               CBC AUXILIAR                                              |
 * -----------------------------------------------------------------------------------------------------------
 *
 * FECHA           AUTOR               J.PROYECTO              PROYECTO                DESCRIPCION
 * ------------------------------------------------------------------------------------------------------------
 * 09/12/2020       Joel Cauti          Alexander Olivares      CBC Picking             Creacion View Model
 */

public class PartnerViewModel {

    private String id;
    private String name;

    private String progressStatus;
    private Integer typeView;
    private boolean isSelected;
    private Integer customersComplete;
    private List<CustomerViewModel> customers;

    public PartnerViewModel() {
    }

    public PartnerViewModel(String id, String name, List<CustomerViewModel> customers, Integer typeView, boolean isSelected, Integer customersComplete, String progressStatus) {
        this.id = id;
        this.name = name;
        this.customers = customers;
        this.typeView = typeView;
        this.isSelected = isSelected;
        this.customersComplete = customersComplete;
        this.progressStatus = progressStatus;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<CustomerViewModel> getCustomers() {
        return customers;
    }

    public void setCustomers(List<CustomerViewModel> customers) {
        this.customers = customers;
    }

    public Integer getTypeView() {
        return typeView;
    }

    public void setTypeView(Integer typeView) {
        this.typeView = typeView;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public Integer getCustomersComplete() {
        return customersComplete;
    }

    public void setCustomersComplete(Integer customersComplete) {
        this.customersComplete = customersComplete;
    }

    public String getProgressStatus() {
        return progressStatus;
    }

    public void setProgressStatus(String progressStatus) {
        this.progressStatus = progressStatus;
    }

    public enum ProgressStatus{
        PENDIENTE("Por hacer"),
        PROCESO("En proceso"),
        ASIGNADO("Asignado a otro usuario"),
        TERMINADO("Completado");

        private String progressStatus;

        ProgressStatus(String progressStatus) {
            this.progressStatus = progressStatus;
        }

        public String getProgressStatus() {
            return progressStatus;
        }
    }
}
