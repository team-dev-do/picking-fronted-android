package com.cbc.auxiliar.presentation.viewcontrollers.activitys;

import android.annotation.SuppressLint;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.cbc.auxiliar.R;
import com.cbc.auxiliar.domain.helpers.PreferencesHelper;
import com.cbc.auxiliar.domain.presenters.ConfigDevicesPresenter;
import com.cbc.auxiliar.presentation.viewadapters.RecyclerDeviceAdapter;
import com.cbc.auxiliar.presentation.viewmodels.BluetoothDeviceModel;
import com.cbc.auxiliar.presentation.viewmodels.SessionViewModel;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import static com.cbc.auxiliar.presentation.viewutils.ConstantsUtils.RESULT_ENABLED_BLUETOOH;

/**
 * -----------------------------------------------------------------------------------------------------------
 * |                                               CBC AUXILIAR                                              |
 * -----------------------------------------------------------------------------------------------------------
 *
 * FECHA           AUTOR               J.PROYECTO              PROYECTO                DESCRIPCION
 * ------------------------------------------------------------------------------------------------------------
 * 04/10/2020       Joel Cauti          Alexander Olivares      CBC Picking             Creacion Activity
 */

public class ConfigDevicesActivity extends AppCompatActivity implements ConfigDevicesPresenter.View {

    private TextView tvEstadoBluewtooth;
    private Switch swModeManual, swBluetoothStatus;
    private RecyclerView rvDevices;
    private Toolbar toolbar;

    private PreferencesHelper preferencesHelper;
    private BluetoothAdapter bluetoothAdapter;
    private RecyclerDeviceAdapter deviceAdapter;

    //
    //
    // BASE
    //
    //

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activty_bluetooth);
        getExtras();
        getSession();
        initPresenter();
        bindUI();
        initUI();
        initEvents();
    }

    @Override
    public void getExtras() {

    }

    @Override
    public void getSession() {
        preferencesHelper = new PreferencesHelper(ConfigDevicesActivity.this);
    }

    @Override
    public void initPresenter() {

    }

    @Override
    public void bindUI() {
        toolbar = findViewById(R.id.toolbar_devices);
        swModeManual = findViewById(R.id.sw_mode_manual);
        swBluetoothStatus = findViewById(R.id.sw_bluetooth_status);
        rvDevices = findViewById(R.id.rv_devices);
        tvEstadoBluewtooth = findViewById(R.id.tv_estado_bluetooth);
    }

    @Override
    public void initUI() {
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_baseline_keyboard_arrow_left_24);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(ConfigDevicesActivity.this);
        rvDevices.setLayoutManager(layoutManager);
        swModeManual.setChecked(preferencesHelper.getSessionVM().getDevice().isEnabled());
        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if(bluetoothAdapter != null){
            if(bluetoothAdapter.isEnabled()){
                swBluetoothStatus.setChecked(true);
                swBluetoothStatus.setEnabled(true);
                startManagmentBluetooth();
            }else{
                swBluetoothStatus.setChecked(false);
                swBluetoothStatus.setEnabled(true);
            }
        }else{
            swBluetoothStatus.setChecked(false);
            swBluetoothStatus.setEnabled(false);
        }
    }

    @Override
    public void initEvents() {
        swBluetoothStatus.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                    startActivityForResult(enableBtIntent, RESULT_ENABLED_BLUETOOH);
                }else{
                    if(bluetoothAdapter != null){
                        if(bluetoothAdapter.isEnabled()){
                            bluetoothAdapter.disable();
                        }
                    }
                    List<BluetoothDeviceModel> devices = new ArrayList<>();
                    deviceAdapter = new RecyclerDeviceAdapter(ConfigDevicesActivity.this,devices);
                    rvDevices.setAdapter(deviceAdapter);
                }
            }
        });
        swModeManual.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    SessionViewModel sesion = preferencesHelper.getSessionVM();
                    sesion.getDevice().setEnabled(true);
                    preferencesHelper.setSessionVM(sesion);
                }else {
                    SessionViewModel sesion = preferencesHelper.getSessionVM();
                    sesion.getDevice().setEnabled(false);
                    preferencesHelper.setSessionVM(sesion);
                }
            }
        });
        toolbar.setNavigationOnClickListener(v -> onBackPressed());
    }

    //
    //
    // RESPUESTA DE ACTIVITYS
    //
    //

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == RESULT_ENABLED_BLUETOOH){
            if(resultCode == RESULT_OK){
                swBluetoothStatus.setChecked(true);
                swBluetoothStatus.setEnabled(true);
                tvEstadoBluewtooth.setText("Activar el uso de bluetooth en el dispositivo");
                startManagmentBluetooth();
            }else if(resultCode == RESULT_CANCELED){
                swBluetoothStatus.setChecked(false);
                swBluetoothStatus.setEnabled(true);
                tvEstadoBluewtooth.setText("Activar el uso de bluetooth en el dispositivo");
            }
        }
    }

    //
    //
    // METODOS AUXILIARES
    //
    //

    @SuppressLint("NewApi")
    private void startManagmentBluetooth() {
        Set<BluetoothDevice> pairedDevices = bluetoothAdapter.getBondedDevices();
        List<BluetoothDeviceModel> bluetoothDevices = new ArrayList<>();
        if(pairedDevices.size() > 0){
            for(BluetoothDevice device: pairedDevices){
                BluetoothDeviceModel bluetoothDevice = new BluetoothDeviceModel();
                bluetoothDevice.setAddress(device.getAddress());
                bluetoothDevice.setConnected(false);
                bluetoothDevice.setName(device.getName());
                bluetoothDevice.setAlias(device.getAlias());
                bluetoothDevice.setBondState(device.getBondState());
                bluetoothDevice.setType(device.getType());
                bluetoothDevices.add(bluetoothDevice);
            }
        }
        deviceAdapter = new RecyclerDeviceAdapter(ConfigDevicesActivity.this, bluetoothDevices);
        rvDevices.setAdapter(deviceAdapter);
    }

    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        return true;
    }
}
