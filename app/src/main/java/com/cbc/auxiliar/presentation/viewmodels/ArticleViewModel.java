package com.cbc.auxiliar.presentation.viewmodels;

/**
 * -----------------------------------------------------------------------------------------------------------
 * |                                               CBC AUXILIAR                                              |
 * -----------------------------------------------------------------------------------------------------------
 *
 * FECHA           AUTOR               J.PROYECTO              PROYECTO                DESCRIPCION
 * ------------------------------------------------------------------------------------------------------------
 * 04/10/2020       Joel Cauti          Alexander Olivares      CBC Picking             Creacion View Model
 */

public class ArticleViewModel {

    private String id;
    private String idProduct;
    private Integer amountRequested;
    private String description;
    private String ean;
    private String ba;
    private String palet;
    private String partner;
    private String family;
    private String category;
    private String brand;
    private String unit;
    private String um;
    private String image;

    private Integer typeView;
    private Integer amountFound;
    private Integer statusProgress;
    private String message;

    public ArticleViewModel() {
    }

    public ArticleViewModel(String id, String idProduct, Integer amountRequested, String description, String ean, String ba, String palet, String partner, String family, String category, String brand, String unit, String um, String image, Integer typeView, Integer amountFound, Integer statusProgress, String message) {
        this.id = id;
        this.idProduct = idProduct;
        this.amountRequested = amountRequested;
        this.description = description;
        this.ean = ean;
        this.ba = ba;
        this.palet = palet;
        this.partner = partner;
        this.family = family;
        this.category = category;
        this.brand = brand;
        this.unit = unit;
        this.um = um;
        this.image = image;
        this.typeView = typeView;
        this.amountFound = amountFound;
        this.statusProgress = statusProgress;
        this.message = message;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdProduct() {
        return idProduct;
    }

    public void setIdProduct(String idProduct) {
        this.idProduct = idProduct;
    }

    public Integer getAmountRequested() {
        return amountRequested;
    }

    public void setAmountRequested(Integer amountRequested) {
        this.amountRequested = amountRequested;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getEan() {
        return ean;
    }

    public void setEan(String ean) {
        this.ean = ean;
    }

    public String getBa() {
        return ba;
    }

    public void setBa(String ba) {
        this.ba = ba;
    }

    public String getPalet() {
        return palet;
    }

    public void setPalet(String palet) {
        this.palet = palet;
    }

    public String getPartner() {
        return partner;
    }

    public void setPartner(String partner) {
        this.partner = partner;
    }

    public String getFamily() {
        return family;
    }

    public void setFamily(String family) {
        this.family = family;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getUm() {
        return um;
    }

    public void setUm(String um) {
        this.um = um;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Integer getTypeView() {
        return typeView;
    }

    public void setTypeView(Integer typeView) {
        this.typeView = typeView;
    }

    public Integer getAmountFound() {
        return amountFound;
    }

    public void setAmountFound(Integer amountFound) {
        this.amountFound = amountFound;
    }

    public Integer getStatusProgress() {
        return statusProgress;
    }

    public void setStatusProgress(Integer statusProgress) {
        this.statusProgress = statusProgress;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
