package com.cbc.auxiliar.presentation.viewadapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.cbc.auxiliar.R;
import com.cbc.auxiliar.domain.helpers.PreferencesHelper;
import com.cbc.auxiliar.presentation.viewmodels.BluetoothDeviceModel;

import java.util.List;

/**
 * -----------------------------------------------------------------------------------------------------------
 * |                                               CBC AUXILIAR                                              |
 * -----------------------------------------------------------------------------------------------------------
 *
 * FECHA           AUTOR               J.PROYECTO              PROYECTO                DESCRIPCION
 * -----------------------------------------------------------------------------------------------------------
 * 04/10/2020       Joel Cauti          Alexander Olivares      CBC Picking             Creacion Adapter
 */

public class RecyclerDeviceAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    private final Context context;
    private final PreferencesHelper preferencesHelper;

    private LayoutInflater inflater;
    private List<BluetoothDeviceModel> devices;

    public RecyclerDeviceAdapter(Context context, List<BluetoothDeviceModel> devices) {
        this.context = context;
        this.devices = devices;
        this.preferencesHelper = new PreferencesHelper(context);
        this.inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v;
        v = inflater.inflate(R.layout.item_device, null, false);
        return new ViewHolderDevice(v);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        ViewHolderDevice viewHolderOrder = (RecyclerDeviceAdapter.ViewHolderDevice) holder;
        viewHolderOrder.tvNombre.setText(devices.get(position).getName());
        viewHolderOrder.tvDescripcion.setText(devices.get(position).getAddress());
    }

    @Override
    public int getItemCount() {
        return devices.size();
    }

    //
    //
    // VIEW HOLDER
    //
    //

    public static class ViewHolderDevice extends RecyclerView.ViewHolder {

        private final RelativeLayout rlDevice;
        private final TextView tvNombre;
        private final TextView tvDescripcion;

        public ViewHolderDevice(@NonNull View itemView) {
            super(itemView);
            rlDevice = itemView.findViewById(R.id.rl_device);
            tvNombre = itemView.findViewById(R.id.tv_nombre);
            tvDescripcion = itemView.findViewById(R.id.tv_descripcion);
        }
    }
}
