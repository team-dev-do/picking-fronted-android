package com.cbc.auxiliar.presentation.viewutils;

/**
 * -----------------------------------------------------------------------------------------------------------
 * |                                               CBC AUXILIAR                                              |
 * -----------------------------------------------------------------------------------------------------------
 *
 * FECHA           AUTOR               J.PROYECTO              PROYECTO                DESCRIPCION
 * ------------------------------------------------------------------------------------------------------------
 * 04/10/2020       Joel Cauti          Alexander Olivares      CBC Picking             Creacion Constantes
 * 15/02/2020       Joel Cauti          Alexander Olivares      CBC Picking             Actualizacion de Constantes
 */

public class ConstantsUtils {

    // CONSTANTES PARA NAVEGACION
    public static final Integer RESULT_ACTIVITY_TRANSPORTATION      = 101;
    public static final Integer RESULT_ACTIVITY_PARTNER             = 102;
    public static final Integer RESULT_ACTIVITY_CUSTOMER            = 103;
    public static final Integer RESULT_ACTIVITY_ARTICLE             = 104;
    public static final Integer RESULT_ACTIVITY_ITEM                = 105;
    public static final Integer RESULT_ACTIVITY_DEVICES             = 106;
    public static final Integer RESULT_ENABLED_BLUETOOH             = 201;

    // REQUEST PERMISSIONS
    public static final Integer REQUEST_PERMISSION_ALL              = 10;

    // INTENT EXTRAS
    public static final String EXTRA_CUSTOMER_IDTRANSPORTATION      = "idTransportation";
    public static final String EXTRA_CUSTOMER_IDPARTNER             = "idPartner";
    public static final String EXTRA_CUSTOMER_NAMEPARTNER           = "namePartner";
}
