package com.cbc.auxiliar.presentation.viewadapters;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.cbc.auxiliar.R;
import com.cbc.auxiliar.domain.helpers.PreferencesHelper;
import com.cbc.auxiliar.domain.presenters.MainTransportationsPresenter;
import com.cbc.auxiliar.presentation.viewcontrollers.activitys.MainPartnersActivity;
import com.cbc.auxiliar.presentation.viewmodels.TransportationViewModel;

import java.util.List;

import static com.cbc.auxiliar.presentation.viewmodels.TransportationViewModel.ProgressStatus.PROCESO;
import static com.cbc.auxiliar.presentation.viewmodels.TransportationViewModel.ProgressStatus.TERMINADO;
import static com.cbc.auxiliar.presentation.viewmodels.TransportationViewModel.ProgressStatus.PENDIENTE;
import static com.cbc.auxiliar.presentation.viewutils.ConstantsUtils.RESULT_ACTIVITY_PARTNER;

/**
 * -----------------------------------------------------------------------------------------------------------
 * |                                               CBC AUXILIAR                                              |
 * -----------------------------------------------------------------------------------------------------------
 *
 * FECHA           AUTOR               J.PROYECTO              PROYECTO                DESCRIPCION
 * -----------------------------------------------------------------------------------------------------------
 * 12/12/2020       Joel Cauti          Alexander Olivares      CBC Picking             Creacion Adapter
 */

public class RecyclerTransportationsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final Context context;
    private final Activity activity;
    private final MainTransportationsPresenter.View view;
    private final PreferencesHelper preferencesHelper;

    private List<TransportationViewModel> transportations;
    private LayoutInflater inflater;

    public RecyclerTransportationsAdapter(Context context) {
        this.context = context;
        this.activity = (Activity) context;
        this.view = (MainTransportationsPresenter.View) context;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.preferencesHelper = new PreferencesHelper(this.context);
        this.transportations = preferencesHelper.getSessionVM().getTransportes();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v;
        v = inflater.inflate(R.layout.item_transportation, null, false);
        return new ViewHolderTransportation(v);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, final int position) {
        if(holder instanceof ViewHolderTransportation){
            ViewHolderTransportation vHolder = (ViewHolderTransportation) holder;
            final TransportationViewModel transportation = transportations.get(position);
            vHolder.tvCorrelativo.setText(String.valueOf(position + 1));
            vHolder.tvNameTransportation.setText("Transporte - " + transportation.getName());
            vHolder.tvIdTransportation.setText(transportation.getId());
            vHolder.tvContador.setText(transportation.getPartnersComplete() + "/" + transportation.getPartners().size());
            vHolder.tvDetail.setText(transportation.getProgressStatus());
            if(transportation.getProgressStatus().equals(TERMINADO.getProgressStatus())){
                vHolder.rlItem.setBackgroundColor(context.getColor(R.color.CBC_backgroundSelected));
            }else{
                vHolder.rlItem.setBackgroundColor(context.getColor(R.color.CBC_transparent));
            }

            if (PENDIENTE.getProgressStatus().equals(transportation.getProgressStatus())) {
                vHolder.tvDetail.setTextColor(activity.getColor(R.color.statusProgressEspera));
            } else if (PROCESO.getProgressStatus().equals(transportation.getProgressStatus())) {
                vHolder.tvDetail.setTextColor(activity.getColor(R.color.statusProgressProceso));
            } else if (TERMINADO.getProgressStatus().equals(transportation.getProgressStatus())) {
                vHolder.tvDetail.setTextColor(activity.getColor(R.color.statusProgressTerminado));
            }
            vHolder.rlItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(!transportations.get(position).getProgressStatus().equals(TERMINADO.getProgressStatus())){
                        for (TransportationViewModel itemTransportation : transportations){
                            itemTransportation.setSelected(false);
                        }
                        transportations.get(position).setSelected(true);
                        preferencesHelper.updateTransportations(transportations);
                        Intent partner = new Intent(activity, MainPartnersActivity.class);
                        activity.startActivityForResult(partner,RESULT_ACTIVITY_PARTNER);
                    }else{
                        view.showAlert("Los pedidos de este transporte an sido finalizados, continua con otros transportes o desliza hacia abajo para actualizar nuevos transportes");
                    }
                }
            });
        }
    }

    @Override
    public int getItemViewType(int position) {
        return transportations.get(position).getTypeView();
    }

    @Override
    public int getItemCount() {
        return transportations.size();
    }

    //
    //
    // VIEW HOLDER
    //
    //

    public static class ViewHolderTransportation extends RecyclerView.ViewHolder{

        private final RelativeLayout rlItem;
        private final TextView tvNameTransportation;
        private final TextView tvIdTransportation;
        private final TextView tvDetail;
        private final TextView tvContador;
        private final TextView tvCorrelativo;

        public ViewHolderTransportation(@NonNull View itemView) {
            super(itemView);
            rlItem = itemView.findViewById(R.id.rl_item);
            tvNameTransportation = itemView.findViewById(R.id.tv_nametransportation);
            tvDetail = itemView.findViewById(R.id.tv_detail);
            tvContador = itemView.findViewById(R.id.tv_contador);
            tvIdTransportation = itemView.findViewById(R.id.tv_idtransportation);
            tvCorrelativo = itemView.findViewById(R.id.correlativo);
        }
    }
}
