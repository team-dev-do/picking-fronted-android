package com.cbc.auxiliar.presentation.viewmodels;

import java.util.ArrayList;
import java.util.List;

/**
 * -----------------------------------------------------------------------------------------------------------
 * |                                               CBC AUXILIAR                                              |
 * -----------------------------------------------------------------------------------------------------------
 *
 * FECHA           AUTOR               J.PROYECTO              PROYECTO                DESCRIPCION
 * ------------------------------------------------------------------------------------------------------------
 * 04/10/2020       Joel Cauti          Alexander Olivares      CBC Picking             Creacion View Model
 */

public class SessionViewModel {

    private Integer version;
    private String imei = "";
    private String code = "";
    private String id = "";
    private String versionPayroll;
    private List<TransportationViewModel> transportes = new ArrayList<>();
    private BluetoothDeviceModel device = new BluetoothDeviceModel();
    private boolean statusSession = false;

    public SessionViewModel() {
    }

    public SessionViewModel(Integer version, String imei, String code, String id, String versionPayroll, List<TransportationViewModel> transportes, BluetoothDeviceModel device, boolean statusSession) {
        this.version = version;
        this.imei = imei;
        this.code = code;
        this.id = id;
        this.versionPayroll = versionPayroll;
        this.transportes = transportes;
        this.device = device;
        this.statusSession = statusSession;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getVersionPayroll() {
        return versionPayroll;
    }

    public void setVersionPayroll(String versionPayroll) {
        this.versionPayroll = versionPayroll;
    }

    public List<TransportationViewModel> getTransportes() {
        return transportes;
    }

    public void setTransportes(List<TransportationViewModel> transportes) {
        this.transportes = transportes;
    }

    public BluetoothDeviceModel getDevice() {
        return device;
    }

    public void setDevice(BluetoothDeviceModel device) {
        this.device = device;
    }

    public boolean isStatusSession() {
        return statusSession;
    }

    public void setStatusSession(boolean statusSession) {
        this.statusSession = statusSession;
    }
}
