package com.cbc.auxiliar.presentation.viewadapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.cbc.auxiliar.R;
import com.cbc.auxiliar.domain.presenters.MainItemPresenter;
import com.cbc.auxiliar.presentation.viewmodels.MessageViewModel;

import java.util.ArrayList;
import java.util.List;

/**
 * -----------------------------------------------------------------------------------------------------------
 * |                                               CBC AUXILIAR                                              |
 * -----------------------------------------------------------------------------------------------------------
 *
 * FECHA           AUTOR               J.PROYECTO              PROYECTO                DESCRIPCION
 * -----------------------------------------------------------------------------------------------------------
 * 04/10/2020       Joel Cauti          Alexander Olivares      CBC Picking             Creacion Adapter
 */

public class SpinnerMessageAdapter extends BaseAdapter {

    private final Context context;
    private final Activity activity;
    private final MainItemPresenter.View view;

    private List<MessageViewModel> messages = new ArrayList<>();
    private LayoutInflater inflater;

    public SpinnerMessageAdapter(Context context, List<MessageViewModel> messages) {
        this.messages = messages;
        this.context = context;
        this.activity = (Activity) context;
        this.view = (MainItemPresenter.View) context;
        this.inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return messages.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View v = convertView;
        if(v == null){
            v = inflater.inflate(R.layout.item_message, parent,false);
        }
        TextView message = v.findViewById(R.id.tv_message);
        message.setText(messages.get(position).getMessage());
        return v;
    }
}
