package com.cbc.auxiliar.presentation.viewmappers;

import com.cbc.auxiliar.data.datamodels.responses.ResponseCustomersBean;
import com.cbc.auxiliar.presentation.viewmodels.CustomerViewModel;

import java.util.ArrayList;
import java.util.List;

/**
 * -----------------------------------------------------------------------------------------------------------
 * |                                               CBC AUXILIAR                                              |
 * -----------------------------------------------------------------------------------------------------------
 *
 * FECHA           AUTOR               J.PROYECTO              PROYECTO                DESCRIPCION
 * ------------------------------------------------------------------------------------------------------------
 * 04/10/2020       Joel Cauti          Alexander Olivares      CBC Picking             Creacion Mapper
 */

public class CustomersViewModelMapper {

    public static CustomerViewModel toViewModel(ResponseCustomersBean customerBean){

        CustomerViewModel customer = null;
        if(customerBean != null) {
            customer = new CustomerViewModel();

            customer.setId(String.valueOf(customerBean.getId()));
            customer.setIdCustomer(String.valueOf(customerBean.getIdClient()));
            customer.setName(customerBean.getName());
            customer.setIdDeviceView(String.valueOf(customerBean.getIdDeviceView()));
            customer.setIdStatusOrder(customerBean.getIdStatusOrder());
            customer.setStatus(customerBean.getStatus());
            customer.setItems(customerBean.getItems());
            customer.setArticles(ArticlesViewModelMapper.toViewModels(customerBean.getProducts()));

            customer.setTypeView(0);
            customer.setSelected(false);
            customer.setArticlesComplete(customerBean.getCounted());
            if(customerBean.getIdStatusOrder() <= 2){
                customer.setProgressStatus(CustomerViewModel.ProgressStatus.PENDIENTE.getProgressStatus());
            }else if(customerBean.getIdStatusOrder() >= 3){
                customer.setProgressStatus(CustomerViewModel.ProgressStatus.TERMINADO.getProgressStatus());
            }

        }
        return customer;
    }

    public static List<CustomerViewModel> toViewModels(List<ResponseCustomersBean> customersBean){

        List<CustomerViewModel> customers = new ArrayList<>();
        if(customersBean != null) {
            for (ResponseCustomersBean customerBean : customersBean) {
                CustomerViewModel customer = toViewModel(customerBean);
                if(customer != null){
                    customers.add(customer);
                }
            }
        }
        return customers;
    }
}
