package com.cbc.auxiliar.presentation.viewmappers;

import com.cbc.auxiliar.data.datamodels.responses.ResponseArticlesBean;
import com.cbc.auxiliar.presentation.viewmodels.ArticleViewModel;

import java.util.ArrayList;
import java.util.List;

/**
 * -----------------------------------------------------------------------------------------------------------
 * |                                               CBC AUXILIAR                                              |
 * -----------------------------------------------------------------------------------------------------------
 *
 * FECHA           AUTOR               J.PROYECTO              PROYECTO                DESCRIPCION
 * ------------------------------------------------------------------------------------------------------------
 * 04/10/2020       Joel Cauti          Alexander Olivares      CBC Picking             Creacion Mapper
 */

public class ArticlesViewModelMapper {

    public static ArticleViewModel toViewModel(ResponseArticlesBean productBean){

        ArticleViewModel product = null;
        if(productBean != null) {
            product = new ArticleViewModel();

            product.setId(String.valueOf(productBean.getId()));
            product.setIdProduct(String.valueOf(productBean.getIdProduct()));
            product.setAmountRequested(productBean.getCount());
            product.setDescription(productBean.getDescription());
            product.setEan(productBean.getEan());
            product.setBa(productBean.getBa());
            product.setPalet(String.valueOf(productBean.getPalet()));
            product.setPartner(productBean.getSocio());
            product.setFamily(productBean.getFamilia());
            product.setCategory(productBean.getCategoria());
            product.setBrand(productBean.getMarca());
            if(productBean.getUnidad() == null){
                product.setUnit("");
            }else{
                product.setUnit(productBean.getUnidad());
            }
            if(productBean.getUm() == null){
                product.setUm("Unidad");
            }else{
                product.setUm(productBean.getUm());
            }
            if(productBean.getImage() != null){
                product.setImage(productBean.getImage());
            }else{
                product.setImage("");
            }

            product.setTypeView(0);
            product.setAmountFound(0);
            product.setStatusProgress(0);
            product.setMessage("");
        }
        return product;
    }

    public static List<ArticleViewModel> toViewModels(List<ResponseArticlesBean> productsBean){

        List<ArticleViewModel> products = new ArrayList<>();
        if(productsBean != null) {
            for (ResponseArticlesBean productBean : productsBean) {
                ArticleViewModel product = toViewModel(productBean);
                if(product != null){
                    products.add(product);
                }
            }
        }
        return products;
    }
}
