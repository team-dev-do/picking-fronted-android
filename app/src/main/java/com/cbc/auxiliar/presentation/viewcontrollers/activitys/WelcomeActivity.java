package com.cbc.auxiliar.presentation.viewcontrollers.activitys;

import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.cbc.auxiliar.R;
import com.cbc.auxiliar.domain.presenters.WelcomePresenter;
import com.cbc.auxiliar.domain.presenters.WelcomePresenterImpl;

/**
 * -----------------------------------------------------------------------------------------------------------
 * |                                               CBC AUXILIAR                                              |
 * -----------------------------------------------------------------------------------------------------------
 *
 * FECHA           AUTOR               J.PROYECTO              PROYECTO                DESCRIPCION
 * -----------------------------------------------------------------------------------------------------------
 * 04/10/2020       Joel Cauti          Alexander Olivares      CBC Picking             Creacion Activity
 */

public class WelcomeActivity extends AppCompatActivity implements WelcomePresenter.View {

    private Button btnIngresar;
    private WelcomePresenter presenter;

    //
    //
    // BASE
    //
    //

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);
        getExtras();
        getSession();
        initPresenter();
        bindUI();
        initUI();
        initEvents();
    }

    @Override
    public void getExtras() {

    }

    @Override
    public void getSession() {

    }

    @Override
    public void initPresenter() {
        presenter = new WelcomePresenterImpl(WelcomeActivity.this);
    }

    @Override
    public void bindUI() {
        btnIngresar = findViewById(R.id.btn_ingresar);
    }

    @Override
    public void initUI() {

    }

    @Override
    public void initEvents() {
        btnIngresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.iniciarSesion();
            }
        });
    }

    //
    //
    // REDIRECCIONAMIENTOS
    //
    //

    @Override
    public void goToMain(){
        Intent main = new Intent(WelcomeActivity.this, MainTransportationsActivity.class);
        main.setAction(Intent.ACTION_MAIN);
        startActivity(main);
        finish();
    }

    //
    //
    // METODOS AUXILIARES
    //
    //

    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        return true;
    }
}
