package com.cbc.auxiliar.presentation.viewmodels;

import java.util.List;

/**
 * -----------------------------------------------------------------------------------------------------------
 * |                                               CBC AUXILIAR                                              |
 * -----------------------------------------------------------------------------------------------------------
 *
 * FECHA           AUTOR               J.PROYECTO              PROYECTO                DESCRIPCION
 * ------------------------------------------------------------------------------------------------------------
 * 04/10/2020       Joel Cauti          Alexander Olivares      CBC Picking             Creacion View Model
 */

public class CustomerViewModel
{
    private String id;
    private String idCustomer;
    private String name;
    private String idDeviceView;
    private Integer idStatusOrder;
    private String progressStatus;
    private Integer items;
    private List<ArticleViewModel> articles;

    private Integer typeView;
    private Integer articlesComplete;
    private String status;
    private boolean isSelected;

    public CustomerViewModel() {
    }

    public CustomerViewModel(String id, String idCustomer, String name, String idDeviceView, Integer idStatusOrder, String progressStatus, Integer items, List<ArticleViewModel> articles, Integer typeView, Integer articlesComplete, String status, boolean isSelected) {
        this.id = id;
        this.idCustomer = idCustomer;
        this.name = name;
        this.idDeviceView = idDeviceView;
        this.idStatusOrder = idStatusOrder;
        this.progressStatus = progressStatus;
        this.items = items;
        this.articles = articles;
        this.typeView = typeView;
        this.articlesComplete = articlesComplete;
        this.status = status;
        this.isSelected = isSelected;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdCustomer() {
        return idCustomer;
    }

    public void setIdCustomer(String idCustomer) {
        this.idCustomer = idCustomer;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIdDeviceView() {
        return idDeviceView;
    }

    public void setIdDeviceView(String idDeviceView) {
        this.idDeviceView = idDeviceView;
    }

    public Integer getIdStatusOrder() {
        return idStatusOrder;
    }

    public void setIdStatusOrder(Integer idStatusOrder) {
        this.idStatusOrder = idStatusOrder;
    }

    public String getProgressStatus() {
        return progressStatus;
    }

    public void setProgressStatus(String progressStatus) {
        this.progressStatus = progressStatus;
    }

    public Integer getItems() {
        return items;
    }

    public void setItems(Integer items) {
        this.items = items;
    }

    public List<ArticleViewModel> getArticles() {
        return articles;
    }

    public void setArticles(List<ArticleViewModel> articles) {
        this.articles = articles;
    }

    public Integer getTypeView() {
        return typeView;
    }

    public void setTypeView(Integer typeView) {
        this.typeView = typeView;
    }

    public Integer getArticlesComplete() {
        return articlesComplete;
    }

    public void setArticlesComplete(Integer articlesComplete) {
        this.articlesComplete = articlesComplete;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public enum ProgressStatus{
        PENDIENTE("Por hacer"),
        PROCESO("En proceso"),
        TERMINADO("Finalizado");

        private String progressStatus;

        ProgressStatus(String progressStatus) {
            this.progressStatus = progressStatus;
        }

        public String getProgressStatus() {
            return progressStatus;
        }
    }
}
