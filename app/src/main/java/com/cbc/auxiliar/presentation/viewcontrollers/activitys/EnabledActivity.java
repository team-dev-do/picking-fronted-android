package com.cbc.auxiliar.presentation.viewcontrollers.activitys;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.cbc.auxiliar.R;
import com.cbc.auxiliar.domain.helpers.PreferencesHelper;
import com.cbc.auxiliar.domain.presenters.EnabledPresenter;
import com.cbc.auxiliar.domain.presenters.EnabledPresenterImpl;

import static com.cbc.auxiliar.domain.helpers.DeviceHelper.getVersion;

/**
 * -----------------------------------------------------------------------------------------------------------
 * |                                               CBC AUXILIAR                                              |
 * -----------------------------------------------------------------------------------------------------------
 *
 * FECHA           AUTOR               J.PROYECTO              PROYECTO                DESCRIPCION
 * ------------------------------------------------------------------------------------------------------------
 * 04/10/2020       Joel Cauti          Alexander Olivares      CBC Picking             Creacion Activity
 */

public class EnabledActivity extends AppCompatActivity implements EnabledPresenter.View {

    private EditText edtCodigo;
    private Button btnActivar, btnSolicitar;
    private LinearLayout llForm, llNotPermissions;
    private AlertDialog loadingDialog;
    private AlertDialog.Builder builderDialog;

    private EnabledPresenter presenter;
    private PreferencesHelper preferencesHelper;

    //
    //
    // BASE
    //
    //

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enabled);
        getExtras();
        getSession();
        initPresenter();
        bindUI();
        initUI();
        initEvents();
        presenter.getPermissions();
    }

    @Override
    public void getExtras() {

    }

    @Override
    public void getSession() {
        preferencesHelper = new PreferencesHelper(EnabledActivity.this);
    }

    @Override
    public void initPresenter() {
        presenter = new EnabledPresenterImpl(EnabledActivity.this);
    }

    @Override
    public void bindUI() {
        edtCodigo = findViewById(R.id.edt_codigo);
        btnActivar = findViewById(R.id.btn_activar);
        btnSolicitar = findViewById(R.id.btn_solicitar);
        llForm = findViewById(R.id.ll_form);
        llNotPermissions = findViewById(R.id.ll_notpermission);
    }

    @Override
    public void initUI() {
        llForm.setVisibility(View.GONE);
        llNotPermissions.setVisibility(View.GONE);
    }

    @Override
    public void initEvents() {
        btnActivar.setOnClickListener(view -> presenter.enabledDevice(edtCodigo.getText().toString()));
        btnSolicitar.setOnClickListener(view -> presenter.getPermissions());
        edtCodigo.setOnEditorActionListener((v, actionId, event) -> {
            if(actionId == EditorInfo.IME_ACTION_DONE) {
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(edtCodigo.getWindowToken(), 0);
                return true;
            }
            return false;
        });
    }

    //
    //
    // RESULTADO DE PERMISOS
    //
    //

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode){
            case 10:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    setPermissionsGranted(true);
                }else{
                    setPermissionsGranted(false);
                }
                return;
        }
    }

    //
    //
    // DIALOGOS
    //
    //

    @Override
    public void showLoading() {
        hideLoading();
        builderDialog = new AlertDialog.Builder(EnabledActivity.this);
        View viewLoading = getLayoutInflater().inflate(R.layout.dialog_loading,null,false);
        builderDialog.setView(viewLoading);
        loadingDialog = builderDialog.create();
        loadingDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        loadingDialog.setCancelable(false);
        loadingDialog.show();
    }

    @Override
    public void showLoading(String message) {
        hideLoading();
        builderDialog = new AlertDialog.Builder(EnabledActivity.this);
        View viewLoading = getLayoutInflater().inflate(R.layout.dialog_loading,null,false);
        TextView tvMessage = viewLoading.findViewById(R.id.tv_message);
        tvMessage.setText(message);
        builderDialog.setView(viewLoading);
        loadingDialog = builderDialog.create();
        loadingDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        loadingDialog.setCancelable(false);
        loadingDialog.show();
    }

    @Override
    public void hideLoading() {
        if(loadingDialog != null){
            loadingDialog.dismiss();
        }
    }

    //
    //
    // REDIRECCIONAMIENTOS
    //
    //

    private void goToMain() {
        Intent main = new Intent(EnabledActivity.this, MainTransportationsActivity.class);
        main.setAction(Intent.ACTION_MAIN);
        startActivity(main);
        finish();
    }

    @Override
    public void goToWelcome() {
        Intent welcome = new Intent(EnabledActivity.this, WelcomeActivity.class);
        welcome.setAction(Intent.ACTION_MAIN);
        startActivity(welcome);
        finish();
    }

    //
    //
    // METODOS AUXILIARES
    //
    //

    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        int keycode = event.getKeyCode();
        if(keycode == KeyEvent.KEYCODE_BACK ||
            keycode == KeyEvent.KEYCODE_DEL ||
            (keycode >= 7 && keycode <= 16)){
            return super.dispatchKeyEvent(event);
        }
        return true;
    }

    @Override
    public void showRegisterDevice() {
        llForm.setVisibility(View.VISIBLE);
        llNotPermissions.setVisibility(View.GONE);
    }

    @Override
    public void setPermissionsGranted(boolean b) {
        if(b){
            llForm.setVisibility(View.GONE);
            llNotPermissions.setVisibility(View.GONE);
            if(preferencesHelper.getSessionVM() != null){
                if(preferencesHelper.getSessionVM().getVersion() == null || preferencesHelper.getSessionVM().getVersion() < getVersion()){
                    preferencesHelper.removeSession();
                    presenter.checkImei();
                }else{
                    if(preferencesHelper.getSessionVM().isStatusSession()){
                        goToMain();
                    }else{
                        presenter.checkImei();
                    }
                }
            }else{
                presenter.checkImei();
            }
        }else{
            llForm.setVisibility(View.GONE);
            llNotPermissions.setVisibility(View.VISIBLE);
        }
    }
}
