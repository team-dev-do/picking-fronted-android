package com.cbc.auxiliar.presentation.viewmodels;

/**
 * -----------------------------------------------------------------------------------------------------------
 * |                                               CBC AUXILIAR                                              |
 * -----------------------------------------------------------------------------------------------------------
 *
 * FECHA           AUTOR               J.PROYECTO              PROYECTO                DESCRIPCION
 * ------------------------------------------------------------------------------------------------------------
 * 04/10/2020       Joel Cauti          Alexander Olivares      CBC Picking             Creacion View Model
 */

public class BluetoothDeviceModel {

    private boolean enabled = false;
    private boolean isConnected = false;
    private String name = "";
    private String address = "";
    private String alias = "";
    private int bondState;
    private int type;

    public BluetoothDeviceModel() {
    }

    public BluetoothDeviceModel(boolean enabled, boolean isConnected, String name, String address, String alias, int bondState, int type) {
        this.enabled = enabled;
        this.isConnected = isConnected;
        this.name = name;
        this.address = address;
        this.alias = alias;
        this.bondState = bondState;
        this.type = type;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public boolean isConnected() {
        return isConnected;
    }

    public void setConnected(boolean connected) {
        isConnected = connected;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public int getBondState() {
        return bondState;
    }

    public void setBondState(int bondState) {
        this.bondState = bondState;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }
}
