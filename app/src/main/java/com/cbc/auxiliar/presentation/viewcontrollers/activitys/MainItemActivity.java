package com.cbc.auxiliar.presentation.viewcontrollers.activitys;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.cbc.auxiliar.domain.presenters.MainItemPresenterImpl;
import com.cbc.auxiliar.presentation.viewadapters.SpinnerMessageAdapter;
import com.cbc.auxiliar.presentation.viewmodels.MessageViewModel;
import com.google.gson.Gson;
import com.cbc.auxiliar.R;
import com.cbc.auxiliar.domain.presenters.MainItemPresenter;
import com.cbc.auxiliar.presentation.viewmodels.ArticleViewModel;

import java.util.ArrayList;
import java.util.List;

import static com.cbc.auxiliar.domain.helpers.LogHelper.printLog;
import static com.cbc.auxiliar.presentation.viewutils.ConstantsUtils.RESULT_ACTIVITY_DEVICES;

/**
 * -----------------------------------------------------------------------------------------------------------
 * |                                               CBC AUXILIAR                                              |
 * -----------------------------------------------------------------------------------------------------------
 *
 * FECHA           AUTOR               J.PROYECTO              PROYECTO                DESCRIPCION
 * -----------------------------------------------------------------------------------------------------------
 * 04/10/2020       Joel Cauti          Alexander Olivares      CBC Picking             Creacion Activity
 */

public class MainItemActivity extends AppCompatActivity implements MainItemPresenter.View {

    private TextView tvTitleItem, tvUm;
    private EditText etCantidad;
    private Button btnHecho;
    private Spinner spMessage;
    private Toolbar toolbar;

    private String title;
    private ArticleViewModel product;
    private List<MessageViewModel> messages;
    private AlertDialog loadingDialog;
    private AlertDialog.Builder builderDialog;

    private MainItemPresenter presenter;

    //
    //
    // BASE
    //
    //

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_item);
        getExtras();
        getSession();
        initPresenter();
        bindUI();
        initUI();
        initEvents();
    }

    @Override
    public void getExtras() {
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            title = extras.getString("EXTRA_TITLE_PLANILLA");
            product = new Gson().fromJson(extras.getString("EXTRA_PRODUCT"), ArticleViewModel.class);

        }
    }

    @Override
    public void getSession() {

    }

    @Override
    public void initPresenter() {
        presenter = new MainItemPresenterImpl(MainItemActivity.this);
        if(product != null){
            presenter.setInicio(product.getId());
        }
    }

    @Override
    public void bindUI() {
        toolbar = findViewById(R.id.toolbar_item);
        tvTitleItem = findViewById(R.id.tv_title_item);
        tvUm = findViewById(R.id.tv_um);
        etCantidad = findViewById(R.id.et_cantidad);
        btnHecho = findViewById(R.id.btn_hecho);
        spMessage = findViewById(R.id.sp_message);
    }

    @SuppressLint({"UseCompatLoadingForDrawables", "SetTextI18n"})
    @Override
    public void initUI() {
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_baseline_keyboard_arrow_left_24);
        toolbar.setTitle(product.getDescription());
        messages = new ArrayList<>();
        messages.add(new MessageViewModel("Stock insuficiente"));
        messages.add(new MessageViewModel("Producto defectuoso"));
        messages.add(new MessageViewModel("Otros"));
        tvTitleItem.setText(String.valueOf(product.getAmountRequested()));
        tvUm.setText(" " + (product.getUm().equals("CJ")? product.getAmountRequested() > 0 ? "cajas" : "caja" : product.getAmountRequested() > 0 ? "unidades" : "unidad"));
        btnHecho.setEnabled(true);
        btnHecho.setBackground(getDrawable(R.drawable.shape_btn_main_normal));
        if (product.getAmountFound() == 0) {
            etCantidad.setText(String.valueOf(product.getAmountRequested()));
            product.setAmountFound(product.getAmountRequested());
            product.setStatusProgress(2);
            spMessage.setVisibility(View.GONE);
        } else {
            etCantidad.setText(String.valueOf(product.getAmountFound()));
            if (product.getAmountFound() < product.getAmountRequested()) {
                product.setStatusProgress(1);
                spMessage.setVisibility(View.VISIBLE);
            } else {
                product.setStatusProgress(2);
                spMessage.setVisibility(View.GONE);
            }
        }
        spMessage.setAdapter(new SpinnerMessageAdapter(MainItemActivity.this, messages));
        if(!product.getMessage().equals("")){
            switch (product.getMessage()){
                case "Stock insuficiente":
                    spMessage.setSelection(0);
                    break;
                case "Producto defectuoso":
                    spMessage.setSelection(1);
                    break;
                case "Otros":
                    spMessage.setSelection(2);
                    break;
            }
        }
    }

    @Override
    public void initEvents() {
        btnHecho.setOnClickListener(v -> {
            Intent data = new Intent();
            if (product.getAmountFound() == product.getAmountRequested()) {
                product.setMessage("");
            }
            data.putExtra("EXTRA_PRODUCT_RESULT", new Gson().toJson(product));
            setResult(RESULT_OK, data);
            finish();
        });
        etCantidad.setOnEditorActionListener((v, actionId, event) -> {
            if(actionId == EditorInfo.IME_ACTION_DONE) {
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(etCantidad.getWindowToken(), 0);
                return true;
            }
            return false;
        });
        etCantidad.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @SuppressLint("UseCompatLoadingForDrawables")
            @Override
            public void afterTextChanged(Editable s) {
                if (!etCantidad.getText().toString().equals("")) {
                    int inputCantidad = Integer.parseInt(etCantidad.getText().toString());
                    if (inputCantidad >= 0 && inputCantidad <= product.getAmountRequested()) {
                        if (inputCantidad < product.getAmountRequested()) {
                            product.setStatusProgress(1);
                        } else if (inputCantidad == product.getAmountRequested()) {
                            product.setStatusProgress(2);
                        }
                        product.setAmountFound(Integer.parseInt(etCantidad.getText().toString()));
                        btnHecho.setEnabled(true);
                        btnHecho.setBackground(getDrawable(R.drawable.shape_btn_main_normal));
                        if (inputCantidad < product.getAmountRequested()) {
                            spMessage.setVisibility(View.VISIBLE);
                        } else {
                            spMessage.setVisibility(View.GONE);
                        }
                    } else {
                        product.setStatusProgress(0);
                        product.setAmountFound(0);
                        btnHecho.setEnabled(false);
                        btnHecho.setBackground(getDrawable(R.drawable.shape_btn_main_normal_disabled));
                        spMessage.setVisibility(View.GONE);
                    }
                } else {
                    product.setStatusProgress(0);
                    product.setAmountFound(0);
                    btnHecho.setEnabled(false);
                    btnHecho.setBackground(getDrawable(R.drawable.shape_btn_main_normal_disabled));
                    spMessage.setVisibility(View.GONE);
                }
            }
        });
        spMessage.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                product.setMessage(messages.get(position).getMessage());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
        toolbar.setNavigationOnClickListener(v -> onBackPressed());
    }

    //
    //
    // DIALOGOS
    //
    //

    @Override
    public void hideDialog() {
        if(loadingDialog != null){
            loadingDialog.dismiss();
        }
    }

    @Override
    public void showDialogDecision(final MainItemPresenter.Dialog dialog, String message, String labelAceptar, String labelRechazar) {
        hideDialog();
        builderDialog = new AlertDialog.Builder(MainItemActivity.this);
        View viewLoading = getLayoutInflater().inflate(R.layout.dialog_decision,null,false);
        TextView tvMessage = viewLoading.findViewById(R.id.tv_message);
        Button aceptar = viewLoading.findViewById(R.id.btn_aceptar);
        Button rechazar = viewLoading.findViewById(R.id.btn_rechazar);
        aceptar.setText(labelAceptar);
        rechazar.setText(labelRechazar);
        tvMessage.setText(message);
        aceptar.setOnClickListener(v -> dialog.aceptarDialog());
        rechazar.setOnClickListener(v -> dialog.rechazarDialog());
        builderDialog.setView(viewLoading);
        loadingDialog = builderDialog.create();
        loadingDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        loadingDialog.setCancelable(false);
        loadingDialog.show();
    }

    @Override
    public void showDialogDecision(final MainItemPresenter.Dialog dialog, String message) {
        hideDialog();
        builderDialog = new AlertDialog.Builder(MainItemActivity.this);
        View viewLoading = getLayoutInflater().inflate(R.layout.dialog_decision,null,false);
        TextView tvMessage = viewLoading.findViewById(R.id.tv_message);
        Button aceptar = viewLoading.findViewById(R.id.btn_aceptar);
        Button rechazar = viewLoading.findViewById(R.id.btn_rechazar);
        tvMessage.setText(message);
        aceptar.setOnClickListener(v -> dialog.aceptarDialog());
        rechazar.setOnClickListener(v -> dialog.rechazarDialog());
        builderDialog.setView(viewLoading);
        loadingDialog = builderDialog.create();
        loadingDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        loadingDialog.setCancelable(false);
        loadingDialog.show();
    }

    @Override
    public void showLoading(String message) {
        hideDialog();
        builderDialog = new AlertDialog.Builder(MainItemActivity.this);
        View viewLoading = getLayoutInflater().inflate(R.layout.dialog_loading,null,false);
        TextView tvMessage = viewLoading.findViewById(R.id.tv_message);
        tvMessage.setText(Html.fromHtml(message));
        builderDialog.setView(viewLoading);
        loadingDialog = builderDialog.create();
        loadingDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        loadingDialog.setCancelable(false);
        loadingDialog.show();
    }

    //
    //
    // REDIRECCIONAMIENTOS
    //
    //

    private void goToDevices() {
        Intent devices = new Intent(MainItemActivity.this, ConfigDevicesActivity.class);
        startActivityForResult(devices, RESULT_ACTIVITY_DEVICES);
    }

    private void goToEnabled() {
        Intent enabled = new Intent(MainItemActivity.this, EnabledActivity.class);
        enabled.setAction(Intent.ACTION_MAIN);
        startActivity(enabled);
        finish();
    }

    //
    //
    // METODOS AUXILIARES
    //
    //

    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        int keycode = event.getKeyCode();
        if(keycode == KeyEvent.KEYCODE_BACK ||
            keycode == KeyEvent.KEYCODE_DEL ||
            (keycode >= 7 && keycode <= 16)){
            return super.dispatchKeyEvent(event);
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public void closeSession() {
        goToEnabled();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.devices:
                goToDevices();
                return true;
            case R.id.close:
                showDialogDecision(new MainItemPresenter.Dialog() {
                    @Override
                    public void aceptarDialog() {
                        presenter.logOut();
                    }
                    @Override
                    public void rechazarDialog() {
                        hideDialog();
                    }
                },"<b> " + getString(R.string.label_cerrar_sesion) + " </b><br><br> " + getString(R.string.text_cerrar_sesion),"Sí","No");
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }
}
