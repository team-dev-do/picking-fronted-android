package com.cbc.auxiliar.presentation.viewmappers;

import com.cbc.auxiliar.data.datamodels.responses.ResponseTransportationsBean;
import com.cbc.auxiliar.presentation.viewmodels.TransportationViewModel;

import java.util.ArrayList;
import java.util.List;

/**
 * -----------------------------------------------------------------------------------------------------------
 * |                                               CBC AUXILIAR                                              |
 * -----------------------------------------------------------------------------------------------------------
 *
 * FECHA           AUTOR               J.PROYECTO              PROYECTO                DESCRIPCION
 * ------------------------------------------------------------------------------------------------------------
 * 09/12/2020       Joel Cauti          Alexander Olivares      CBC Picking             Creacion Mapper
 */

public class TransportationsViewModelMapper {

    public static TransportationViewModel toViewModel(ResponseTransportationsBean transportationBean){

        TransportationViewModel transportation = null;
        if(transportationBean != null) {
            transportation = new TransportationViewModel();

            transportation.setId(String.valueOf(transportationBean.getId()));
            transportation.setName(transportationBean.getName());
            transportation.setPartners(PartnersViewModelMapper.toViewModels(transportationBean.getFamilies()));

            transportation.setTypeView(0);
            transportation.setSelected(false);
            transportation.setPartnersComplete(0);
            transportation.setProgressStatus(TransportationViewModel.ProgressStatus.PENDIENTE.getProgressStatus());
        }
        return transportation;
    }

    public static List<TransportationViewModel> toViewModels(List<ResponseTransportationsBean> transportationsBean){

        List<TransportationViewModel> transportations = new ArrayList<>();
        if(transportationsBean != null) {
            for (ResponseTransportationsBean transportationBean : transportationsBean) {
                TransportationViewModel transportation = toViewModel(transportationBean);
                if(transportation != null){
                    transportations.add(transportation);
                }
            }
        }
        return transportations;
    }
}
