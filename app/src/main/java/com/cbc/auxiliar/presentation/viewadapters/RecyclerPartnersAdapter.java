package com.cbc.auxiliar.presentation.viewadapters;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.cbc.auxiliar.R;
import com.cbc.auxiliar.domain.helpers.PreferencesHelper;
import com.cbc.auxiliar.domain.presenters.MainPartnersPresenter;
import com.cbc.auxiliar.presentation.viewmodels.PartnerViewModel;
import com.cbc.auxiliar.presentation.viewmodels.TransportationViewModel;

import java.util.List;

import static com.cbc.auxiliar.presentation.viewmodels.PartnerViewModel.ProgressStatus.ASIGNADO;
import static com.cbc.auxiliar.presentation.viewmodels.PartnerViewModel.ProgressStatus.PENDIENTE;
import static com.cbc.auxiliar.presentation.viewmodels.PartnerViewModel.ProgressStatus.PROCESO;
import static com.cbc.auxiliar.presentation.viewmodels.PartnerViewModel.ProgressStatus.TERMINADO;

/**
 * -----------------------------------------------------------------------------------------------------------
 * |                                               CBC AUXILIAR                                              |
 * -----------------------------------------------------------------------------------------------------------
 *
 * FECHA           AUTOR               J.PROYECTO              PROYECTO                DESCRIPCION
 * -----------------------------------------------------------------------------------------------------------
 * 12/12/2020       Joel Cauti          Alexander Olivares      CBC Picking             Creacion Adapter
 */

public class RecyclerPartnersAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final Context context;
    private final Activity activity;
    private final MainPartnersPresenter.View view;
    private final PreferencesHelper preferencesHelper;

    private List<PartnerViewModel> partners;
    private String idTransportation;
    private List<TransportationViewModel> transportations;
    private LayoutInflater inflater;

    public RecyclerPartnersAdapter(Context context) {
        this.context = context;
        this.activity = (Activity) context;
        this.view = (MainPartnersPresenter.View) context;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.preferencesHelper = new PreferencesHelper(this.context);
        this.transportations = preferencesHelper.getSessionVM().getTransportes();
        getPartners();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v;
        v = inflater.inflate(R.layout.item_partner, null, false);
        return new ViewHolderPartner(v);
    }

    @SuppressLint("UseCompatLoadingForDrawables")
    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if(holder instanceof RecyclerPartnersAdapter.ViewHolderPartner){
            RecyclerPartnersAdapter.ViewHolderPartner vHolder = (RecyclerPartnersAdapter.ViewHolderPartner) holder;
            final PartnerViewModel partner = partners.get(position);
            vHolder.tvNamePartner.setText(partner.getName());
            vHolder.btnLiberar.setVisibility(View.GONE);
            if(partner.getProgressStatus().equals(TERMINADO.getProgressStatus())) {
                vHolder.rlItem.setBackgroundColor(context.getColor(R.color.CBC_backgroundSelected));
                vHolder.ivIcon.setImageDrawable(activity.getDrawable(R.drawable.ic_baseline_check_24_green));
                vHolder.tvNamePartner.setTextColor(context.getColor(R.color.CBC_backgroundCant));
                vHolder.ivArrow.setImageDrawable(activity.getDrawable(R.drawable.ic_baseline_keyboard_arrow_right_24));
                vHolder.tvStatus.setTextColor(context.getColor(R.color.statusProgressTerminado));
                vHolder.tvStatus.setText(partner.getProgressStatus());
            }else if(partner.getProgressStatus().equals(ASIGNADO.getProgressStatus())){
                vHolder.rlItem.setBackgroundColor(context.getColor(R.color.CBC_backgroundDisabled));
                vHolder.ivIcon.setImageDrawable(activity.getDrawable(R.drawable.ic_baseline_check_24));
                vHolder.tvNamePartner.setTextColor(context.getColor(R.color.CBC_inputText));
                vHolder.ivArrow.setImageDrawable(activity.getDrawable(R.drawable.ic_baseline_keyboard_arrow_right_gray_24));
                vHolder.tvStatus.setTextColor(context.getColor(R.color.statusProgressEspera));
                vHolder.tvStatus.setText(partner.getProgressStatus());
            }else if(partner.getProgressStatus().equals(PROCESO.getProgressStatus())){
                vHolder.rlItem.setBackgroundColor(context.getColor(R.color.CBC_transparent));
                vHolder.ivIcon.setImageDrawable(activity.getDrawable(R.drawable.ic_baseline_check_24));
                vHolder.tvNamePartner.setTextColor(context.getColor(R.color.CBC_backgroundCant));
                vHolder.ivArrow.setImageDrawable(activity.getDrawable(R.drawable.ic_baseline_keyboard_arrow_right_24));
                vHolder.tvStatus.setTextColor(context.getColor(R.color.statusProgressProceso));
                vHolder.tvStatus.setText(partner.getProgressStatus());
                vHolder.btnLiberar.setVisibility(View.VISIBLE);
            }else{
                vHolder.rlItem.setBackgroundColor(context.getColor(R.color.CBC_transparent));
                vHolder.ivIcon.setImageDrawable(activity.getDrawable(R.drawable.ic_baseline_check_24));
                vHolder.tvNamePartner.setTextColor(context.getColor(R.color.CBC_backgroundCant));
                vHolder.ivArrow.setImageDrawable(activity.getDrawable(R.drawable.ic_baseline_keyboard_arrow_right_24));
                vHolder.tvStatus.setTextColor(context.getColor(R.color.statusProgressEspera));
            }

            vHolder.btnLiberar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    view.liberarFamilia(idTransportation, partner.getId());
                }
            });
            if(partner.getProgressStatus().equals(PENDIENTE.getProgressStatus()) || partner.getProgressStatus().equals(PROCESO.getProgressStatus()) || partner.getProgressStatus().equals(ASIGNADO.getProgressStatus())){
                vHolder.rlItem.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        view.getCustomers(idTransportation, partner.getId());
                    }
                });
            }
        }
    }

    @Override
    public int getItemViewType(int position) {
        return partners.get(position).getTypeView();
    }

    @Override
    public int getItemCount() {
        return partners.size();
    }

    //
    //
    // VIEW HOLDER
    //
    //

    public static class ViewHolderPartner extends RecyclerView.ViewHolder{

        private final RelativeLayout rlItem;
        private final TextView tvNamePartner;
        private final TextView tvStatus;
        private final ImageView ivIcon;
        private final ImageView ivArrow;
        private final Button btnLiberar;

        public ViewHolderPartner(@NonNull View itemView) {
            super(itemView);
            rlItem = itemView.findViewById(R.id.rl_item);
            tvNamePartner = itemView.findViewById(R.id.tv_namepartner);
            ivIcon = itemView.findViewById(R.id.iv_icon);
            tvStatus = itemView.findViewById(R.id.tv_status);
            ivArrow = itemView.findViewById(R.id.iv_arrow);
            btnLiberar = itemView.findViewById(R.id.btn_liberar);
        }
    }

    //
    //
    // METODOS AUXILIARES
    //
    //

    private void getPartners() {
        for(TransportationViewModel transportation: transportations){
            if(transportation.isSelected()){
                this.idTransportation = transportation.getId();
                this.partners = transportation.getPartners();
            }
        }
    }
}
