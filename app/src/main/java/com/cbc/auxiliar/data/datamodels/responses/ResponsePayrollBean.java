package com.cbc.auxiliar.data.datamodels.responses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ResponsePayrollBean {

    @SerializedName("trucks")
    @Expose
    private List<ResponseTransportationsBean> trucks;
    @SerializedName("idOrderFile")
    @Expose
    private int idOrderFile;

    public List<ResponseTransportationsBean> getTrucks() {
        return trucks;
    }

    public void setTrucks(List<ResponseTransportationsBean> trucks) {
        this.trucks = trucks;
    }

    public int getIdOrderFile() {
        return idOrderFile;
    }

    public void setIdOrderFile(int idOrderFile) {
        this.idOrderFile = idOrderFile;
    }
}
