package com.cbc.auxiliar.data.webservices.interfaces;

import com.cbc.auxiliar.BuildConfig;
import com.cbc.auxiliar.data.datamodels.request.RegisterDeviceBean;
import com.cbc.auxiliar.data.datamodels.responses.ResponseDeviceEnabledBean;
import com.cbc.auxiliar.data.datamodels.responses.ResponseLogoutBean;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * -----------------------------------------------------------------------------------------------------------
 * |                                               CBC AUXILIAR                                              |
 * -----------------------------------------------------------------------------------------------------------
 *
 * FECHA           AUTOR               J.PROYECTO              PROYECTO                DESCRIPCION
 * -----------------------------------------------------------------------------------------------------------
 * 04/10/2020       Joel Cauti          Alexander Olivares      CBC Picking             Creacion Service Retrofit
 */

public interface EnabledService {

    @GET(BuildConfig.VALIDAR_IMEI)
    Call<ResponseDeviceEnabledBean> checkImei(@Path("imei") String imei);

    @POST(BuildConfig.ACTIVAR_DISPOSITIVO)
    Call<ResponseDeviceEnabledBean> registerDevice(@Body RegisterDeviceBean registerDeviceBean);

    @GET(BuildConfig.CERRAR_SESION)
    Call<ResponseLogoutBean> logOut(@Header("imei") String imei);
}
