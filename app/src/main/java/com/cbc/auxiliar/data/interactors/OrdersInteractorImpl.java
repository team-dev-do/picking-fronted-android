package com.cbc.auxiliar.data.interactors;

import com.cbc.auxiliar.data.datamodels.request.PedidosFinishBean;
import com.cbc.auxiliar.data.datamodels.responses.ResponseFinishBean;
import com.cbc.auxiliar.data.datamodels.responses.ResponseInicioProducto;
import com.cbc.auxiliar.data.datamodels.responses.ResponseCustomersBean;
import com.cbc.auxiliar.data.datamodels.responses.ResponsePayrollBean;
import com.cbc.auxiliar.data.datamodels.responses.ResponseTerminarProducto;
import com.cbc.auxiliar.data.datamodels.responses.ResponseTransportationsBean;
import com.cbc.auxiliar.data.datamodels.responses.ResponseUnlockBean;
import com.cbc.auxiliar.data.webservices.manager.OrderManager;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * -----------------------------------------------------------------------------------------------------------
 * |                                               CBC AUXILIAR                                              |
 * -----------------------------------------------------------------------------------------------------------
 *
 * FECHA           AUTOR               J.PROYECTO              PROYECTO                DESCRIPCION
 * -----------------------------------------------------------------------------------------------------------
 * 04/10/2020       Joel Cauti          Alexander Olivares      CBC Picking             Creacion Implementacion Interactor
 */

public class OrdersInteractorImpl implements OrdersInteractor{

    private final Callbacks callbacks;

    public OrdersInteractorImpl(Callbacks callbacks) {
        this.callbacks = callbacks;
    }

    /**
     * Obtiene los productos V1
     * @param imei
     */
    @Override
    public void getOrders(String imei) {
        final String method = "getOrders";
        OrderManager.getApiService().getOrders(imei).enqueue(new Callback<List<ResponseCustomersBean>>() {
            @Override
            public void onResponse(Call<List<ResponseCustomersBean>> call, Response<List<ResponseCustomersBean>> response) {
                switch (response.code()){
                    case 200:
                    case 201:
                        List<ResponseCustomersBean> pedidos = new ArrayList<>();
                        if(response.body() != null){
                            pedidos.addAll(response.body());
                            callbacks.successOrders(pedidos);
                        }else{
                            callbacks.errorRequest(3,method);
                        }
                        break;
                    case 401:
                        callbacks.unauthorized(method);
                        break;
                    case 500:
                        callbacks.errorRequest(2,method);
                        break;
                    default:
                        callbacks.errorRequest(1,method);
                        break;
                }
            }

            @Override
            public void onFailure(Call<List<ResponseCustomersBean>> call, Throwable t) {
                callbacks.errorRequest(0,method);
            }
        });
    }

    /**
     * Obtiene los productos V2 agrupados por transportes
     * @param imei
     */
    @Override
    public void getTransportations(String imei) {
        final String method = "getTransportations";
        OrderManager.getApiService().getTransportations(imei).enqueue(new Callback<ResponsePayrollBean>() {
            @Override
            public void onResponse(Call<ResponsePayrollBean> call, Response<ResponsePayrollBean> response) {
                switch (response.code()){
                    case 200:
                    case 201:
                        ResponsePayrollBean res = new ResponsePayrollBean();
                        if(response.body() != null){
                            res = response.body();
                            callbacks.successTransportations(res);
                        }else{
                            callbacks.errorRequest(3,method);
                        }
                        break;
                    case 401:
                        callbacks.unauthorized(method);
                        break;
                    case 500:
                        callbacks.errorRequest(2,method);
                        break;
                    default:
                        callbacks.errorRequest(1,method);
                        break;
                }
            }

            @Override
            public void onFailure(Call<ResponsePayrollBean> call, Throwable t) {
                callbacks.errorRequest(0,method);
            }
        });
    }

    /**
     * Obtiene los clientes V2 por cada transporte y familia
     * @param imei
     * @param transportation
     * @param partner
     */
    @Override
    public void getCustomers(String imei, String transportation, String partner) {
        final String method = "getCustomers";
        OrderManager.getApiService().getCustomers(imei, transportation, partner).enqueue(new Callback<List<ResponseCustomersBean>>() {
            @Override
            public void onResponse(Call<List<ResponseCustomersBean>> call, Response<List<ResponseCustomersBean>> response) {
                switch (response.code()){
                    case 200:
                    case 201:
                        List<ResponseCustomersBean> customers = new ArrayList<>();
                        if(response.body() != null){
                            customers.addAll(response.body());
                            callbacks.successCustomers(customers);
                        }else{
                            callbacks.errorRequest(3,method);
                        }
                        break;
                    case 401:
                        callbacks.unauthorized(method);
                        break;
                    case 500:
                        callbacks.errorRequest(2,method);
                        break;
                    default:
                        callbacks.errorRequest(1,method);
                        break;
                }
            }

            @Override
            public void onFailure(Call<List<ResponseCustomersBean>> call, Throwable t) {
                callbacks.errorRequest(0,method);
            }
        });
    }

    /**
     * Libera los clientes v2
     * @param imei
     * @param transportation
     * @param partner
     */
    @Override
    public void unlockCustomers(String imei, String transportation, String partner) {
        final String method = "unlockCustomers";
        OrderManager.getApiService().unlockCustomers(imei, transportation, partner).enqueue(new Callback<ResponseUnlockBean>() {
            @Override
            public void onResponse(Call<ResponseUnlockBean> call, Response<ResponseUnlockBean> response) {
                switch (response.code()){
                    case 200:
                    case 201:
                        ResponseUnlockBean unlock;
                        if(response.body() != null){
                            unlock = response.body();
                            callbacks.successUnlock(unlock);
                        }else{
                            callbacks.errorRequest(3,method);
                        }
                        break;
                    case 401:
                        callbacks.unauthorized(method);
                        break;
                    case 500:
                        callbacks.errorRequest(2,method);
                        break;
                    default:
                        callbacks.errorRequest(1,method);
                        break;
                }
            }

            @Override
            public void onFailure(Call<ResponseUnlockBean> call, Throwable t) {
                callbacks.errorRequest(0,method);
            }
        });
    }

    /**
     * Completa un pedido V1
     * @param imei
     * @param pedidos
     */
    @Override
    public void finishOrders(String imei, PedidosFinishBean pedidos) {
        final String method = "finishOrders";
        OrderManager.getApiService().finishOrders(imei,pedidos).enqueue(new Callback<ResponseFinishBean>() {
            @Override
            public void onResponse(Call<ResponseFinishBean> call, Response<ResponseFinishBean> response) {
                switch (response.code()){
                    case 200:
                    case 201:
                        if(response.body() != null){
                            ResponseFinishBean responseFinishBean = response.body();
                            callbacks.successFinishPedidos(responseFinishBean);
                        }else{
                            callbacks.errorRequest(3,method);
                        }
                        break;
                    case 401:
                        callbacks.unauthorized(method);
                        break;
                    case 500:
                        callbacks.errorRequest(2,method);
                        break;
                    default:
                        callbacks.errorRequest(1,method);
                        break;
                }
            }

            @Override
            public void onFailure(Call<ResponseFinishBean> call, Throwable t) {
                callbacks.errorRequest(0,method);
            }
        });
    }

    /**
     * establece el inicio del picking de un producto
     * @param imei
     * @param product
     */
    @Override
    public void setInicio(String imei, String product) {
        final String method = "setInicio";
        OrderManager.getApiService().setInicio(imei, product).enqueue(new Callback<ResponseInicioProducto>() {
            @Override
            public void onResponse(Call<ResponseInicioProducto> call, Response<ResponseInicioProducto> response) {
                switch (response.code()) {
                    case 200:
                    case 201:
                        if (response.body() != null) {
                            ResponseInicioProducto responseInicioProducto = response.body();
                            callbacks.successSetInicio(responseInicioProducto);
                        } else {
                            callbacks.errorRequest(3, method);
                        }
                        break;
                    case 401:
                        callbacks.unauthorized(method);
                        break;
                    case 500:
                        callbacks.errorRequest(2, method);
                        break;
                    default:
                        callbacks.errorRequest(1, method);

                }
            }

            @Override
            public void onFailure(Call<ResponseInicioProducto> call, Throwable t) {
                callbacks.errorRequest(0,method);
            }
        });
    }

    /**
     * Establece el final del picking de un producto
     * @param imei
     * @param product
     */
    @Override
    public void setTerminar(String imei, String product) {
        final String method = "setTerminar";
        OrderManager.getApiService().setTerminar(imei, product).enqueue(new Callback<ResponseTerminarProducto>() {
            @Override
            public void onResponse(Call<ResponseTerminarProducto> call, Response<ResponseTerminarProducto> response) {
                switch (response.code()) {
                    case 200:
                    case 201:
                        if (response.body() != null) {
                            ResponseTerminarProducto responseTerminarProducto = response.body();
                            callbacks.successSetTerminar(responseTerminarProducto);
                        } else {
                            callbacks.errorRequest(3, method);
                        }
                        break;
                    case 401:
                        callbacks.unauthorized(method);
                        break;
                    case 500:
                        callbacks.errorRequest(2, method);
                        break;
                    default:
                        callbacks.errorRequest(1, method);

                }
            }

            @Override
            public void onFailure(Call<ResponseTerminarProducto> call, Throwable t) {
                callbacks.errorRequest(0,method);
            }
        });
    }
}
