package com.cbc.auxiliar.data.webservices.manager;

import com.cbc.auxiliar.BuildConfig;
import com.cbc.auxiliar.data.restutils.RestUtil;
import com.cbc.auxiliar.data.webservices.interfaces.EnabledService;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;

/**
 * -----------------------------------------------------------------------------------------------------------
 * |                                               CBC AUXILIAR                                              |
 * -----------------------------------------------------------------------------------------------------------
 *
 * FECHA           AUTOR               J.PROYECTO              PROYECTO                DESCRIPCION
 * -----------------------------------------------------------------------------------------------------------
 * 04/10/2020       Joel Cauti          Alexander Olivares      CBC Picking             Creacion Service Manager Retrofit
 */

public class EnabledManager {

    private static final String BASE_URL = BuildConfig.URL_HOST;
    private static EnabledService apiService;

    public static EnabledService getApiService(String token){
        OkHttpClient httpClient = RestUtil.getHttpClientToken(token);
        Retrofit.Builder builder = new Retrofit.Builder();
        builder.baseUrl(BASE_URL);
        builder.client(httpClient);
        builder.addConverterFactory(JacksonConverterFactory.create(RestUtil.getObjectMapperConfiguration()));
        Retrofit retrofit = builder.build();
        apiService = retrofit.create(EnabledService.class);
        return apiService;
    }

    public static EnabledService getApiService(){
        OkHttpClient httpClient = RestUtil.getHttpClient();
        Retrofit.Builder builder = new Retrofit.Builder();
        builder.baseUrl(BASE_URL);
        builder.client(httpClient);
        builder.addConverterFactory(JacksonConverterFactory.create(RestUtil.getObjectMapperConfiguration()));
        Retrofit retrofit = builder.build();
        apiService = retrofit.create(EnabledService.class);
        return apiService;
    }
}
