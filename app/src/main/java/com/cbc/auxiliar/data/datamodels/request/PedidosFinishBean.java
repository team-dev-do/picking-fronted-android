package com.cbc.auxiliar.data.datamodels.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * -----------------------------------------------------------------------------------------------------------
 * |                                               CBC AUXILIAR                                              |
 * -----------------------------------------------------------------------------------------------------------
 *
 * FECHA           AUTOR               J.PROYECTO              PROYECTO                DESCRIPCION
 * -----------------------------------------------------------------------------------------------------------
 * 04/10/2020       Joel Cauti          Alexander Olivares      CBC Picking             Creacion Data Bean
 */

public class PedidosFinishBean {

    @SerializedName("idOrder")
    @Expose
    private int idOrder;
    @SerializedName("products")
    @Expose
    private List<ProductFinishBean> products = null;

    public int getIdOrder() {
        return idOrder;
    }

    public void setIdOrder(int idOrder) {
        this.idOrder = idOrder;
    }

    public List<ProductFinishBean> getProducts() {
        return products;
    }

    public void setProducts(List<ProductFinishBean> products) {
        this.products = products;
    }

}
