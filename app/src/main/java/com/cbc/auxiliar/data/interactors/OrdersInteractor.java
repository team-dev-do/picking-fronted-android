package com.cbc.auxiliar.data.interactors;

import com.cbc.auxiliar.data.datamodels.request.PedidosFinishBean;
import com.cbc.auxiliar.data.datamodels.responses.ResponseFinishBean;
import com.cbc.auxiliar.data.datamodels.responses.ResponseInicioProducto;
import com.cbc.auxiliar.data.datamodels.responses.ResponseCustomersBean;
import com.cbc.auxiliar.data.datamodels.responses.ResponsePayrollBean;
import com.cbc.auxiliar.data.datamodels.responses.ResponseTerminarProducto;
import com.cbc.auxiliar.data.datamodels.responses.ResponseTransportationsBean;
import com.cbc.auxiliar.data.datamodels.responses.ResponseUnlockBean;

import java.util.List;

/**
 * -----------------------------------------------------------------------------------------------------------
 * |                                               CBC AUXILIAR                                              |
 * -----------------------------------------------------------------------------------------------------------
 *
 * FECHA           AUTOR               J.PROYECTO              PROYECTO                DESCRIPCION
 * -----------------------------------------------------------------------------------------------------------
 * 04/10/2020       Joel Cauti          Alexander Olivares      CBC Picking             Creacion Interactor
 */

public interface OrdersInteractor {

    void getOrders(String imei);
    void getTransportations(String imei);
    void getCustomers(String imei, String idTransportation, String idPartner);
    void unlockCustomers(String imei, String idTransportation, String idPartner);
    void finishOrders(String imei, PedidosFinishBean pedidos);
    void setInicio(String imei, String product);
    void setTerminar(String imei, String product);

    interface Callbacks{

        void errorRequest(Integer errorCode, String method);
        void unauthorized(String method);
        void successOrders(List<ResponseCustomersBean> response);
        void successFinishPedidos(ResponseFinishBean response);
        void successSetInicio(ResponseInicioProducto response);
        void successSetTerminar(ResponseTerminarProducto response);
        void successTransportations(ResponsePayrollBean transportes);
        void successCustomers(List<ResponseCustomersBean> customers);
        void successUnlock(ResponseUnlockBean unlock);
    }
}
