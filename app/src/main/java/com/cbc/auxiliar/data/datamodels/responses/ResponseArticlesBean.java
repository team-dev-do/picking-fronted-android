package com.cbc.auxiliar.data.datamodels.responses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * -----------------------------------------------------------------------------------------------------------
 * |                                               CBC AUXILIAR                                              |
 * -----------------------------------------------------------------------------------------------------------
 *
 * FECHA           AUTOR               J.PROYECTO              PROYECTO                DESCRIPCION
 * -----------------------------------------------------------------------------------------------------------
 * 04/10/2020       Joel Cauti          Alexander Olivares      CBC Picking             Creacion Data Bean
 */

public class ResponseArticlesBean {

    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("idProduct")
    @Expose
    private int idProduct;
    @SerializedName("count")
    @Expose
    private int count;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("ean")
    @Expose
    private String ean;
    @SerializedName("ba")
    @Expose
    private String ba;
    @SerializedName("palet")
    @Expose
    private int palet;
    @SerializedName("socio")
    @Expose
    private String socio;
    @SerializedName("familia")
    @Expose
    private String familia;
    @SerializedName("categoria")
    @Expose
    private String categoria;
    @SerializedName("marca")
    @Expose
    private String marca;
    @SerializedName("unidad")
    @Expose
    private String unidad;
    @SerializedName("um")
    @Expose
    private String um;
    @SerializedName("image")
    @Expose
    private String image;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdProduct() {
        return idProduct;
    }

    public void setIdProduct(int idProduct) {
        this.idProduct = idProduct;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getEan() {
        return ean;
    }

    public void setEan(String ean) {
        this.ean = ean;
    }

    public String getBa() {
        return ba;
    }

    public void setBa(String ba) {
        this.ba = ba;
    }

    public int getPalet() {
        return palet;
    }

    public void setPalet(int palet) {
        this.palet = palet;
    }

    public String getSocio() {
        return socio;
    }

    public void setSocio(String socio) {
        this.socio = socio;
    }

    public String getFamilia() {
        return familia;
    }

    public void setFamilia(String familia) {
        this.familia = familia;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getUnidad() {
        return unidad;
    }

    public void setUnidad(String unidad) {
        this.unidad = unidad;
    }

    public String getUm() {
        return um;
    }

    public void setUm(String um) {
        this.um = um;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
