package com.cbc.auxiliar.data.interactors;

import com.cbc.auxiliar.data.datamodels.responses.ResponseDeviceEnabledBean;
import com.cbc.auxiliar.data.datamodels.responses.ResponseLogoutBean;

/**
 * -----------------------------------------------------------------------------------------------------------
 * |                                               CBC AUXILIAR                                              |
 * -----------------------------------------------------------------------------------------------------------
 *
 * FECHA           AUTOR               J.PROYECTO              PROYECTO                DESCRIPCION
 * -----------------------------------------------------------------------------------------------------------
 * 04/10/2020       Joel Cauti          Alexander Olivares      CBC Picking             Creacion Interactor
 */

public interface EnabledInteractor {

    void checkImei(String imei);
    void enabledDevice(String code, String imei);
    void logOut(String imei);

    interface Callbacks {

        void errorRequest(Integer errorCode, String method);
        void unauthorized(String method);
        void successCheckImei(ResponseDeviceEnabledBean response);
        void successEnabledDevice(ResponseDeviceEnabledBean response);
        void successLogout(ResponseLogoutBean response);
    }
}
