package com.cbc.auxiliar.data.webservices.interfaces;

import com.cbc.auxiliar.BuildConfig;
import com.cbc.auxiliar.data.datamodels.request.PedidosFinishBean;
import com.cbc.auxiliar.data.datamodels.responses.ResponseFinishBean;
import com.cbc.auxiliar.data.datamodels.responses.ResponseInicioProducto;
import com.cbc.auxiliar.data.datamodels.responses.ResponseCustomersBean;
import com.cbc.auxiliar.data.datamodels.responses.ResponsePayrollBean;
import com.cbc.auxiliar.data.datamodels.responses.ResponseTerminarProducto;
import com.cbc.auxiliar.data.datamodels.responses.ResponseUnlockBean;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * -----------------------------------------------------------------------------------------------------------
 * |                                               CBC AUXILIAR                                              |
 * -----------------------------------------------------------------------------------------------------------
 *
 * FECHA           AUTOR               J.PROYECTO              PROYECTO                DESCRIPCION
 * -----------------------------------------------------------------------------------------------------------
 * 04/10/2020       Joel Cauti          Alexander Olivares      CBC Picking             Creacion Service Retrofit
 */

public interface OrdersService {

    @GET(BuildConfig.OBTENER_PEDIDO)
    Call<List<ResponseCustomersBean>> getOrders(@Header("imei") String imei);

    @GET(BuildConfig.OBTENER_TRANSPORTES)
    Call<ResponsePayrollBean> getTransportations(@Header("imei") String imei);

    @GET(BuildConfig.OBTENER_CLIENTES)
    Call<List<ResponseCustomersBean>> getCustomers(@Header("imei") String imei,
                                                   @Path("idtransporte") String transporte,
                                                   @Path("idfamilia") String familia);

    @GET(BuildConfig.LIBERAR_CLIENTES)
    Call<ResponseUnlockBean> unlockCustomers(@Header("imei") String imei,
                                                   @Path("idtransporte") String transporte,
                                                   @Path("idfamilia") String familia);

    @POST(BuildConfig.PEDIDO_COMPLETADO)
    Call<ResponseFinishBean> finishOrders(@Header("imei") String imei,
                                          @Body PedidosFinishBean pedidos);

    @GET(BuildConfig.INICIO_PRODUCTO)
    Call<ResponseInicioProducto> setInicio(@Header("imei") String imei,
                                           @Path("idproducto") String producto);

    @GET(BuildConfig.TERMINAR_PRODUCTO)
    Call<ResponseTerminarProducto> setTerminar(@Header("imei") String imei,
                                               @Path("idproducto") String producto);
}
