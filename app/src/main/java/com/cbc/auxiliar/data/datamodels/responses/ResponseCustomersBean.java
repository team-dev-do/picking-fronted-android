package com.cbc.auxiliar.data.datamodels.responses;

import  com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * -----------------------------------------------------------------------------------------------------------
 * |                                               CBC AUXILIAR                                              |
 * -----------------------------------------------------------------------------------------------------------
 *
 * FECHA           AUTOR               J.PROYECTO              PROYECTO                DESCRIPCION
 * -----------------------------------------------------------------------------------------------------------
 * 04/10/2020       Joel Cauti          Alexander Olivares      CBC Picking             Creacion Data Bean
 */

public class ResponseCustomersBean {

    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("idClient")
    @Expose
    private int idClient;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("idDeviceView")
    @Expose
    private int idDeviceView;
    @SerializedName("idStatusOrder")
    @Expose
    private int idStatusOrder;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("items")
    @Expose
    private int items;
    @SerializedName("counted")
    @Expose
    private int counted;
    @SerializedName("products")
    @Expose
    private List<ResponseArticlesBean> products;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdClient() {
        return idClient;
    }

    public void setIdClient(int idClient) {
        this.idClient = idClient;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getIdDeviceView() {
        return idDeviceView;
    }

    public void setIdDeviceView(int idDeviceView) {
        this.idDeviceView = idDeviceView;
    }

    public int getIdStatusOrder() {
        return idStatusOrder;
    }

    public void setIdStatusOrder(int idStatusOrder) {
        this.idStatusOrder = idStatusOrder;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getItems() {
        return items;
    }

    public void setItems(int items) {
        this.items = items;
    }

    public int getCounted() {
        return counted;
    }

    public void setCounted(int counted) {
        this.counted = counted;
    }

    public List<ResponseArticlesBean> getProducts() {
        return products;
    }

    public void setProducts(List<ResponseArticlesBean> products) {
        this.products = products;
    }
}
