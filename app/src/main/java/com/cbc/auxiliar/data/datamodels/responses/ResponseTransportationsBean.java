package com.cbc.auxiliar.data.datamodels.responses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * -----------------------------------------------------------------------------------------------------------
 * |                                               CBC AUXILIAR                                              |
 * -----------------------------------------------------------------------------------------------------------
 *
 * FECHA           AUTOR               J.PROYECTO              PROYECTO                DESCRIPCION
 * -----------------------------------------------------------------------------------------------------------
 * 12/12/2020       Joel Cauti          Alexander Olivares      CBC Picking             Creacion Data Bean
 */

public class ResponseTransportationsBean {

    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("families")
    @Expose
    private List<ResponsePartnersBean> families;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<ResponsePartnersBean> getFamilies() {
        return families;
    }

    public void setFamilies(List<ResponsePartnersBean> families) {
        this.families = families;
    }
}
