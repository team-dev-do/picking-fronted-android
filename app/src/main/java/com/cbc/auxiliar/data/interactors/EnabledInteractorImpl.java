package com.cbc.auxiliar.data.interactors;

import com.cbc.auxiliar.data.datamodels.request.RegisterDeviceBean;
import com.cbc.auxiliar.data.datamodels.responses.ResponseDeviceEnabledBean;
import com.cbc.auxiliar.data.datamodels.responses.ResponseLogoutBean;
import com.cbc.auxiliar.data.webservices.manager.EnabledManager;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * -----------------------------------------------------------------------------------------------------------
 * |                                               CBC AUXILIAR                                              |
 * -----------------------------------------------------------------------------------------------------------
 *
 * FECHA           AUTOR               J.PROYECTO              PROYECTO                DESCRIPCION
 * -----------------------------------------------------------------------------------------------------------
 * 04/10/2020       Joel Cauti          Alexander Olivares      CBC Picking             Creacion Implementacion Interactor
 */

public class EnabledInteractorImpl implements EnabledInteractor{

    private final Callbacks callbacks;

    public EnabledInteractorImpl(Callbacks callbacks) {
        this.callbacks = callbacks;
    }

    /**
     * Verifica que el IMEI del dispositivo este registrado en base de datos
     * @param imei
     */
    @Override
    public void checkImei(String imei) {
        final String method = "checkImei";
        EnabledManager.getApiService().checkImei(imei).enqueue(new Callback<ResponseDeviceEnabledBean>() {
            @Override
            public void onResponse(Call<ResponseDeviceEnabledBean> call, Response<ResponseDeviceEnabledBean> response) {
                switch (response.code()){
                    case 200:
                    case 201:
                        if(response.body() != null){
                            ResponseDeviceEnabledBean responseCheckImeiBean = response.body();
                            callbacks.successCheckImei(responseCheckImeiBean);
                        }else{
                            callbacks.errorRequest(3,method);
                        }
                        break;
                    case 401:
                        callbacks.unauthorized(method);
                        break;
                    case 500:
                        callbacks.errorRequest(2,method);
                        break;
                    default:
                        callbacks.errorRequest(1,method);
                        break;
                }
            }

            @Override
            public void onFailure(Call<ResponseDeviceEnabledBean> call, Throwable t) {
                callbacks.errorRequest(0,method);
            }
        });
    }

    /**
     * Activa un dispositivo para su uso
     * @param code
     * @param imei
     */
    @Override
    public void enabledDevice(String code, String imei) {
        final String method = "enabledDevice";
        EnabledManager.getApiService().registerDevice(new RegisterDeviceBean(code, imei)).enqueue(new Callback<ResponseDeviceEnabledBean>() {
            @Override
            public void onResponse(Call<ResponseDeviceEnabledBean> call, Response<ResponseDeviceEnabledBean> response) {
                switch (response.code()){
                    case 200:
                    case 201:
                        if(response.body() != null){
                            ResponseDeviceEnabledBean responseCheckImeiBean = response.body();
                            callbacks.successEnabledDevice(responseCheckImeiBean);
                        }else{
                            callbacks.errorRequest(3,method);
                        }
                        break;
                    case 401:
                        callbacks.unauthorized(method);
                        break;
                    case 500:
                        callbacks.errorRequest(2,method);
                        break;
                    default:
                        callbacks.errorRequest(1,method);
                        break;
                }
            }

            @Override
            public void onFailure(Call<ResponseDeviceEnabledBean> call, Throwable t) {
                callbacks.errorRequest(0,method);
            }
        });
    }

    /**
     * Cierra sesion y desvincula los pedidos asignados al presente dispositivo
     * @param imei
     */
    @Override
    public void logOut(String imei) {
        final String method = "logOut";
        EnabledManager.getApiService().logOut(imei).enqueue(new Callback<ResponseLogoutBean>() {
            @Override
            public void onResponse(Call<ResponseLogoutBean> call, Response<ResponseLogoutBean> response) {
                switch (response.code()){
                    case 200:
                    case 201:
                        ResponseLogoutBean responseLogoutBean = response.body();
                        callbacks.successLogout(responseLogoutBean);
                        break;
                    case 401:
                        callbacks.unauthorized(method);
                        break;
                    case 500:
                        callbacks.errorRequest(2,method);
                        break;
                    default:
                        callbacks.errorRequest(1,method);
                        break;
                }
            }

            @Override
            public void onFailure(Call<ResponseLogoutBean> call, Throwable t) {
                callbacks.errorRequest(0,method);
            }
        });
    }
}
